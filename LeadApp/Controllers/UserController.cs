﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Controllers
{
	[Restricted]
	public class UserController: Controller {
		private UserService _serv = new UserService();
		public SecurityService _servSec = new SecurityService();
		private string error = string.Empty;
		private bool isEmailSent = false;


		[NotRestricted]
		public ActionResult Captcha(string prefix)
		{
			ImageCaptcha cap = new ImageCaptcha();
			var result = cap.GetCaptcha();
			Session.Add("Captcha" + prefix, result.Text);

			return new FileContentResult(result.Image, result.ContentType);
		}







		[NotRestricted]
		public ActionResult Login()
		{
			//Check if user sesssion is alive then send it to HomePage
			if (Auth.isLogin)
				return RedirectToAction("Index");

			return View(new LoginViewModel());
		}

		[NotRestricted]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Login(LoginViewModel logindata, string url)
		{

			try
			{
				//Check if user sesssion is alive then send it to HomePage
				if (Auth.isLogin)
					return RedirectToAction("Index");


				if (Session["Captcha"] != null)
				{
					string GetCaptcha = Session["Captcha"].ToString();

					if (string.IsNullOrEmpty(logindata.Captcha))
						ModelState.AddModelError("_FORM", "Please verify word shown in the image.");
					else if (!logindata.Captcha.Equals(GetCaptcha))
						ModelState.AddModelError("_FORM", "Provided text does not match Captcha text.");

					logindata.ShowCaptcha = true;
					logindata.Captcha = "";
				}


				if (ModelState.IsValid)
				{
					string error = "";
					bool VerifyUser = _serv.CreateSession(logindata, out error);


					if (VerifyUser)
					{
						Session.Remove("LoginCount");
						Session.Remove("Captcha");

						string RedirectURL = string.Empty;
						if (!string.IsNullOrEmpty(url))
							RedirectURL = EncryptDecryptHelper.Decrypt(url);

						if (!string.IsNullOrEmpty(RedirectURL) && RedirectURL.Contains("http://"))
							return new RedirectResult(RedirectURL);
						else
							return RedirectToAction("Index", "Home");//Sucesfully Login
					}
					else
						ModelState.AddModelError("_FORM", error);
				}

				logindata.Captcha = "";
				return View(logindata);

			}
			catch (System.Data.SqlClient.SqlException ex)
			{
				ModelState.AddModelError("_FORM", "Something went wrong (" + ex + ")");

				return View(logindata);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("_FORM", "Error: " + ex);
				return View(logindata);

			}
			finally
			{
				if (!ModelState.IsValid)
				{
					if (Session["LoginCount"] != null)
					{
						Session["LoginCount"] = HelperMethods.ConvertToInt(Session["LoginCount"]) + 1;
						if (HelperMethods.ConvertToInt(Session["LoginCount"]) >= 3)
							logindata.ShowCaptcha = true;//place captcha on page
					}
					else
						Session["LoginCount"] = 1;
				}
			}

		}

		[NotRestricted]
		public void Logout()
		{
			_serv.SignOut();

			Session["isLogin"] = "false";
			Session.Clear();
			Session.RemoveAll();
			Session.Abandon();

			HttpCookie LoginAuthKey = new HttpCookie("AuthKey", " ");
			LoginAuthKey.Expires = DateTime.Now.AddDays(-1);
			Response.Cookies.Add(LoginAuthKey);
			Response.Cookies.Remove("AuthKey");


			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.Cache.SetNoStore();



			ModelState.AddModelError("_FORM", "You have been successfully logged out.");
			TempData["OutputMessage"] = "You have been successfully logged out.";
			TempData["AlertCSS"] = "alert success";

			//return new RedirectResult();
			Response.Redirect("/User/login", true);

			//return RedirectToAction("Login");
		}








		[NotRestricted]
		public ActionResult Forgot()
		{
			return View(new AppUserPwdRequest());
		}

		[NotRestricted]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Forgot(AppUserPwdRequest _data)
		{
			try
			{
				//Check if user sesssion is alive then send it to HomePage
				if (Auth.isLogin)
					return RedirectToAction("Index");


				if (Session["Captcha"] != null)
				{
					string GetCaptcha = Session["Captcha"].ToString();

					if (string.IsNullOrEmpty(_data.Captcha))
						ModelState.AddModelError("_FORM", "Please verify word shown in the image.");
					else if (!_data.Captcha.Equals(GetCaptcha))
						ModelState.AddModelError("_FORM", "Provided text does not match Captcha text.");

					_data.ShowCaptcha = true;
					_data.Captcha = "";
				}


				if (string.IsNullOrEmpty(_data.UserName) && string.IsNullOrEmpty(_data.Email))
					ModelState.AddModelError("_FORM", "Please provide user name or email address.");

				if (string.IsNullOrEmpty(_data.UserName) && !HelperMethods.isEmail(_data.Email))
					ModelState.AddModelError("Email", "Invalid email address.");


				if (ModelState.IsValid)
				{					
					bool isValid = _serv.Forgot(_data, out error);


					if (isValid)
					{
						Session.Remove("LoginCount");
						Session.Remove("Captcha");


						TempData["OutputMessage"] = "An email is sent containing the link to reset your password.";
						TempData["AlertCSS"] = "alert success";

						return View(new AppUserPwdRequest());
					}
					else
						ModelState.AddModelError("_FORM", error);		
				}

				_data.Captcha = "";
				return View(_data);

			}
			catch (System.Data.SqlClient.SqlException ex)
			{
				ModelState.AddModelError("_FORM", "Something went wrong (" + ex + ")");

				return View(_data);
			}
			catch (Exception ex)
			{
				ModelState.AddModelError("_FORM", "Error: " + ex);
				return View(_data);

			}
			finally
			{
				if (!ModelState.IsValid)
				{
					if (Session["LoginCount"] != null)
					{
						Session["LoginCount"] = HelperMethods.ConvertToInt(Session["LoginCount"]) + 1;
						if (HelperMethods.ConvertToInt(Session["LoginCount"]) >= 3)
							_data.ShowCaptcha = true;//place captcha on page
					}
					else
						Session["LoginCount"] = 1;
				}
			}
		}





		[NotRestricted]
		public ActionResult ResetPassword(AppUserPwdRequest _data, string r)
		{
			try
			{
				string key = EncryptDecryptHelper.Decrypt(r);
				if (key.Split('&').Length == 2)
				{
					var randNo = key.Split('&')[0];
					var RecID = key.Split('&')[1];


					var RandomNo = randNo.Substring(randNo.IndexOf("=") + 1);
					var ResetRecID = RecID.Substring(RecID.IndexOf("=") + 1);


					if (Request.HttpMethod == "POST")
					{
						if (string.IsNullOrEmpty(_data.Password))
							ModelState.AddModelError("Password", "Please enter a new password.");

						if (string.IsNullOrEmpty(_data.Confirmpwd))
							ModelState.AddModelError("Confirmpwd", "Please repeat the password for verification.");

						if ((_data.Password != _data.Confirmpwd))
							ModelState.AddModelError("Password", "Both Password Field does not match.");



						if (ModelState.IsValid)
						{

							bool isValid = _serv.ResetPassword(ResetRecID.ConvertToInt(), _data.Password);

							if (isValid)
							{
								TempData["OutputMessage"] = "Your Password has been reset sucessfully.";
								TempData["AlertCSS"] = "alert success";
							}
							else
							{
								TempData["OutputMessage"] = "Password Reset fail.";
								TempData["AlertCSS"] = "alert warning";
							}


							return RedirectToAction("login");
						}
					}

				}
				else
				{
					TempData["OutputMessage"] = "Reference Key is not valid.";
					TempData["AlertCSS"] = "alert error";
				}

				return View();
			}
			catch
			{
				ModelState.AddModelError("_FORM", "An error occured.");
				return View();
			}
		}






		public JsonResult isUserExist() {
			string Username = HelperMethods.CleanUserName(HelperMethods.ConvertToString(Request.QueryString["AppUser.UserName"]));
			int UserId = HelperMethods.ConvertToInt(Request.QueryString["UserId"]);

			UserId = HelperMethods.ConvertToInt(Request.QueryString["AppUser.UserId"]);

			return Json(_serv.isUserExist(Username, UserId), JsonRequestBehavior.AllowGet);
		}

		public JsonResult isUserEmailExist()
		{
			string Email = Request.QueryString["Email"].ConvertToString();
			int UserId = HelperMethods.ConvertToInt(Request.QueryString["UserId"]);

			return Json(_serv.isUserEmailExist(Email, UserId), JsonRequestBehavior.AllowGet);
		}




		public ActionResult Index(string q) {
			if (!string.IsNullOrEmpty(q))
				return View(_serv.GetEmployees().Where(e => e.AppUser.UserName.Contains(q) || e.Email.Contains(q) || e.ContactName.Contains(q)));
			else
				return View(_serv.GetEmployees());
		}





		public ActionResult Create() {
			ViewBag.GroupID = new SelectList(_servSec.GetAllGroups(), "GroupId", "GroupName");
			//ViewBag.PartyDepartments = new MultiSelectList(CommonService.GetPartyDepartments(), "PartyDepartmentId", "PartyDepartmentName");
			return View();
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(AppEmployee employeeData, List<int> PartyDepartments, int GroupId, bool? isEmailSend)
		{
			//Clean Username with spaces
			employeeData.AppUser.UserName = HelperMethods.CleanUserName(employeeData.AppUser.UserName);

			if (!_serv.isUserExist(employeeData.AppUser.UserName, 0))
				ModelState.AddModelError("AppUser.UserName", "User Name already exist");

			//if (PartyDepartments == null)
			//	ModelState.AddModelError("PartyDepartments", "Must Specify Client Department Access");

			if (ModelState.IsValid) {
				bool isSuccess = _serv.CreateEmployee(employeeData, PartyDepartments, GroupId, isEmailSend.ConvertToBool(), out error, out isEmailSent);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";


				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
				{
					if (isEmailSend.ConvertToBool()) 
						error += (isEmailSent) ? "" : " (Sending account information email failed!)";
					TempData["OutputMessage"] = "Record Successfully Created" + error;
				}

				if (ModelState.IsValid)
					return RedirectToAction("Index");
			}


			ViewBag.GroupID = new SelectList(_servSec.GetAllGroups(), "GroupId", "GroupName", GroupId);
			//ViewBag.PartyDepartments = new MultiSelectList(CommonService.GetPartyDepartments(), "PartyDepartmentId", "PartyDepartmentName", PartyDepartments);
			return View(employeeData);
		}










		public ActionResult Edit(string id) {
			if (!HelperMethods.IsGUID(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			AppEmployee employeeData = _serv.GetEmployee(HelperMethods.ConvertToGUID(id));
			if (employeeData == null)
				return HttpNotFound();

			employeeData.AppUser.Password = "";
			ViewBag.GroupID = new SelectList(_servSec.GetAllGroups(), "GroupId", "GroupName", employeeData.AppUser.GroupID);
			//ViewBag.PartyDepartments = new MultiSelectList(CommonService.GetPartyDepartments(), "PartyDepartmentId", "PartyDepartmentName", employeeData.AppEmployeeDepartments.Select(e => e.PartyDepartmentId));

			return View(employeeData);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(string id, AppEmployee employeeData, List<int> PartyDepartments, int GroupId, bool? isEmailSend)
		{
			if (!HelperMethods.IsGUID(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			//if (PartyDepartments == null)
			//	ModelState.AddModelError("PartyDepartments", "Must Specify Client Department Access");


			if (ModelState.IsValid) {
				
				bool isSuccess = _serv.EditEmployee(HelperMethods.ConvertToGUID(id), employeeData, PartyDepartments, GroupId, isEmailSend.ConvertToBool(), out error, out isEmailSent);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
				{
					if (isEmailSend.ConvertToBool())
						error += (isEmailSent) ? "" : " (Sending account information email failed!)";
					TempData["OutputMessage"] = "Record Successfully Created" + error;
				}

				if (ModelState.IsValid)
					return RedirectToAction("Index");
			}

			ViewBag.GroupID = new SelectList(_servSec.GetAllGroups(), "GroupId", "GroupName", employeeData.AppUser.GroupID);
			//ViewBag.PartyDepartments = new MultiSelectList(CommonService.GetPartyDepartments(), "PartyDepartmentId", "PartyDepartmentName", employeeData.AppEmployeeDepartments.Select(e => e.PartyDepartmentId));
			return View(employeeData);
		}













		public ActionResult ViewProfile(string id) {
			int UserID = HelperMethods.IsNumeric(id) ? HelperMethods.ConvertToInt(id) : Auth.UserID;
			AppEmployee employeeData = _serv.GetUser(UserID);
			if (employeeData == null){
				TempData["AlertCSS"] = "alert error";
				TempData["OutputMessage"] = "ID is not valid";
				return RedirectToAction("Index");
			}
			employeeData.Notes = (employeeData.Notes != null) ? employeeData.Notes.Replace(System.Environment.NewLine, "<br/>") : employeeData.Notes;
			employeeData.Signature = (employeeData.Signature != null) ? employeeData.Signature.Replace(System.Environment.NewLine, "<br/>") : employeeData.Signature;


			var ThumbImg = employeeData.AppUser.AppUserMetas.Where(e => e.MetaKey == "Thumbnail-Original").Select(e => e.MetaValue).FirstOrDefault();
			string UserProfilePic =  WebConstants.UserProfileThumbURL + "thb128-" + ThumbImg;
			ViewBag.Thumb = (ThumbImg != null) ? UserProfilePic : WebConstants.Default_UserThumbURL;

			return View(employeeData);
		}

		public ActionResult EditProfile() {
			AppEmployee employeeData = _serv.GetUser(Auth.UserID);
			if (employeeData == null) {
				TempData["AlertCSS"] = "alert error";
				TempData["OutputMessage"] = "ID is not valid";
				return RedirectToAction("ViewProfile");
			}

			employeeData.AppUser.Password = "";
			return View(employeeData);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditProfile(AppEmployee employeeData) {

			if (ModelState.IsValid) {
				bool isSuccess = _serv.EditProfile(employeeData, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Profile Successfully Updated";

				if (ModelState.IsValid)
					return RedirectToAction("ViewProfile");
			}

			//employeeData.AppUser.Password = "";
			return View(employeeData);
		}


		[ValidateInput(false)]
		public ActionResult UploadPicture(FormCollection FrmInput) {
			try {

				if (Request.HttpMethod == "POST") {

					#region File Upload
					string FileSavePath = WebConstants.PhysicalPath + WebConstants.UserProfileThumbPath;
					string SaveFileName = string.Empty;
					string SuccessMsg = string.Empty;
					string ErrorMsg = string.Empty;

					string FileField = "Profile-Pic";

					var UploadFileField = Request.Files[FileField];

					if (UploadFileField != null) {
						if (UploadFileField.ContentLength > 0) {
							FileSystemHelper FileUpl = new FileSystemHelper();
							bool UPloadFile = FileUpl.UploadFile(FileField, FileSavePath,
								WebConstants.UserProfileExtensions,
								WebConstants.UserProfileImageFileSize, out SaveFileName, out ErrorMsg);

							if (UPloadFile) {
								SuccessMsg = "File Uploaded Successfully";

								bool SaveOriginal = _serv.AddThumbRec(SaveFileName, WebConstants.UserThumbOriginalKey);
								bool SaveAllThumb = _serv.UserThumbnails(FileSavePath + SaveFileName);


								if (!SaveOriginal || !SaveAllThumb)
									ErrorMsg += "Error in Creating Thumbnail ";
							}

						} else
							ErrorMsg += "No File has been selected";
					} else
						ErrorMsg += "No File has been selected";

					#endregion

					Session["Thumbnail"] = SaveFileName;
					TempData["AlertCSS"] = string.IsNullOrEmpty(ErrorMsg) ? "alert success" : "alert error";
					TempData["OutputMessage"] = string.IsNullOrEmpty(ErrorMsg) ? SuccessMsg : ErrorMsg;
					return RedirectToAction("ViewProfile");
				}

				return View();

			} catch (Exception) {
				ModelState.AddModelError("_FORM", "An error occured.");
				return View();
			}
		}














		[HttpPost]
		public ActionResult Delete(string id) {
			if (!HelperMethods.IsGUID(id)) {
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}


			bool isSuccess = _serv.DeletEmployee(HelperMethods.ConvertToGUID(id), out error);
			TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

			if (!isSuccess)
				TempData["OutputMessage"] = "Error Occured: " + error;
			else
				TempData["OutputMessage"] = "Record Successfully Deleted";

			return RedirectToAction("Index");
		}
	}
}
