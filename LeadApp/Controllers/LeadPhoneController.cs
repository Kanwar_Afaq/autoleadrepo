﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System;
using System.Linq.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Controllers
{
    [Restricted]
    public class LeadPhoneController : Controller
    {
        public ManufacturerService _servMan = new ManufacturerService();
        public LeadPhoneServices _serv = new LeadPhoneServices();
        private LeadAppEntities _context = new LeadAppEntities();
        // GET: LeadPhone
        public ActionResult Detail(int id)
        {

            LeadPhone _data = _serv.Get().Where(e => e.LeadPhoneId == id).FirstOrDefault();
            ViewBag.PendingLeads = _serv.getPendingLeads();
            ViewBag.SubmittedBy = "Submitted by: " + _data.AppUser.UserName;
            ViewBag.LeadPhoneId = id;
            var Lastfollowup = _data.LeadPhoneFollowUps;
            ViewBag.LastFollowUp = Lastfollowup.OrderByDescending(q => q.LeadPhoneFollowUpId);
            var isVarifyButtonVisible = _context.LeadPhones.Where(q => q.isVarified == true && q.LeadPhoneId==id);
            if (isVarifyButtonVisible.Count() > 0)
            {
                ViewBag.IsVarifyButtonVisible = false;
            }
            else
            {
                ViewBag.IsVarifyButtonVisible = true;
            }
            var IsApproveButtonVisible = _context.LeadPhones.Where(q => q.LeadPhoneId == id && q.StatusId == 4);
            if (IsApproveButtonVisible.Count() > 0)
            {
                ViewBag.IsApproveButtonVisible = false;
            }
            else
            {
                ViewBag.IsApproveButtonVisible = true;
            }
            //if(Auth.GroupID==7)
            //{
            //    return RedirectToAction("VarifyLead", new { id = id });
            //}
            if (Auth.GroupID == 5)
            {
                var isSalePriceButtonVisible = _serv.isVisiblePriceButton(id);

                if (isSalePriceButtonVisible == true)
                {
                    ViewBag.isSalePriceButtonVisible = true;
                }
                else
                {
                    ViewBag.isSalePriceButtonVisible = false;
                }
                var IsLeadDisqualifyButtonVisible = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == id);
                if (IsLeadDisqualifyButtonVisible.Count() == 2)
                {
                    ViewBag.IsLeadDisqualifyButtonVisible = true;
                }
                else
                {
                    ViewBag.IsLeadDisqualifyButtonVisible = false;
                }
            }
            else
            {
                ViewBag.IsLeadDisqualifyButtonVisible = false;
            }

            // var _datalastaskingprice = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == id && q.LatestAskingPrice != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
            var _datalastofferedamount = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == id && q.LatestOfferedAmount != null).OrderByDescending(q => q.LeadPhoneFollowUpId);


            // ViewBag.LatestAskingPrice = _datalastaskingprice.Any() ? _datalastaskingprice.FirstOrDefault().LatestAskingPrice : 0;
            ViewBag.LatestOfferedPrice = _datalastofferedamount.Any() ? _datalastofferedamount.FirstOrDefault().LatestOfferedAmount : 0;
            ViewBag.LastFollowUpPrice = Lastfollowup.Any() ? Lastfollowup.OrderByDescending(q => q.LeadPhoneFollowUpId).FirstOrDefault().LatestOfferedAmount : 0;
            if (_data == null)
            {
                TempData["OutputMessage"] = "Record Not Found";
                return RedirectToAction("LeadDetail", new { id = id });
            }
            return View(_data);
        }


        [HttpPost]
        public JsonResult VarifyLead(int id)
        {

            var getVarified = _serv.getVarified(id);
            return Json(getVarified, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult VarifiedLead(int id, string PlaceofPickup, string City, string Phone, string Email, int VehicleYear, string VinNo, string ZipCode)
        {

            var varifiedLead = _serv.VarifiedLead(id, PlaceofPickup, City, Phone, Email, VehicleYear, VinNo, ZipCode);
            return Json(varifiedLead, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult Create(LeadPhone _data)
        {
            string error = null;
            int LeadPhoneId = 0;
            ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name");
            if (ModelState.IsValid)
            {
                bool isSuccess = _serv.Add(_data, out error, out LeadPhoneId);
                TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

                if (!isSuccess)
                    ModelState.AddModelError("", error);

                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(int? id)
        {
            ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult Edit(int? id, LeadPhone _data)
        {
            string error = string.Empty;
            ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name");
            if (ModelState.IsValid)
            {
                bool isSuccess = _serv.Edit(id, _data, out error);
                TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

                if (!isSuccess)
                    ModelState.AddModelError("", error);
                else
                    TempData["OutputMessage"] = "Record Successfully Modified!";

                if (ModelState.IsValid)
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("Edit", new { id = id });

            }
            return View();
        }

        public ActionResult Index(string q, string isRejected, int? statusid)
        {
            ViewBag.StatusId = statusid;
            ViewBag.PendingLeads = _serv.getPendingLeads();
            //var _data = _serv.GetByUserId(statusid);
            // return Json(new { data = _data }, JsonRequestBehavior.AllowGet);
            return View();
        }
        public ActionResult loadData(int? statusId)
        {
            var _data = _serv.GetByUserId(statusId);
            return Json(new { data = _data }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult Index(int? statusId)
        {
            int start = Convert.ToInt32(Request["start"]);
            int length = Convert.ToInt32(Request["length"]);
           string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            var result = _serv.GetByUserId(statusId);
           
                int totalRows = result.Count();
                if (!String.IsNullOrEmpty(searchValue))
            {
                result = result.Where(x => ((x.Name.ToLower()?? "N/A").Contains(searchValue.ToLower())) ||
                ((x.Status.ToLower()?? "N/A").Contains(searchValue.ToLower())) ||
                ((x.VehicleYear.ToString().ToLower()?? "N/A").Contains(searchValue.ToLower())) ||
               
                ((x.Manufacturer.ToLower()?? "N/A").Contains(searchValue.ToLower())) ||
                x.LeadPhoneId.ToString().ToLower().Contains(searchValue.ToLower()) ||
                ((x.Email.ToLower()?? "N/A").Contains(searchValue.ToLower()))
              ).ToList();
            }
            int totalRowsAfterFilter = result.Count();
            result = result.OrderBy(sortColumnName + " " + sortDirection).ToList();
            result = result.Skip(start).Take(length).ToList();
            return Json(new { data = result, draw = Request["draw"], recordsTotal = totalRows, recordsFiltered = totalRowsAfterFilter }, JsonRequestBehavior.AllowGet);

        }

        

        public ActionResult AddNote(int id, string Note, string Mode, decimal? Amount)
        {
            if (Auth.GroupID == 5)
            {
                var _datalastaskingprice = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == id && q.LatestAskingPrice != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
                if (_datalastaskingprice.Any())
                {
                    var lastasking = _datalastaskingprice.Any() ? _datalastaskingprice.FirstOrDefault().LatestAskingPrice : 0;
                    if (Amount > lastasking)
                    {
                        return Json(4, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            var AddNote = _serv.AddNote(id, Note, Mode, Amount);
            return Json(2, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddNoteOnly(int id, string Note)
        {
            var AddNoteOnly = _serv.AddNoteOnly(id, Note);
            return Json(2, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveLead(int id, string Note, decimal AmountAccepted, string PlaceofPickup, decimal LatestOfferedPrice)
        {
            if (AmountAccepted > LatestOfferedPrice)
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var ApproveLead = _serv.ApproveLead(id, Note, AmountAccepted, PlaceofPickup);
                return Json(1, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Profit_Loss()
        {
            ViewBag.PendingLeads = _serv.getPendingLeads();
            return View();
        }
        [HttpPost]
        public JsonResult loadProfitLossData(string startdate, string EndDate)
        {
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();

            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            var Status = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;
            if (!(string.IsNullOrEmpty(startdate)) && !(string.IsNullOrEmpty(EndDate)))
            {
                var result2 = _serv.getProfitLossStats().AsQueryable().Where(q => q.createdDate >= Convert.ToDateTime(startdate) && q.createdDate < Convert.ToDateTime(EndDate));
                totalRecords = result2.Count();
                var data = result2.Skip(skip).Take(pageSize).ToList();
                return Json(new { recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                var result = _serv.getProfitLossStats();
                totalRecords = result.Count();
                var data = result.Skip(skip).Take(pageSize).ToList();
                return Json(new { recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);
            }



        }

        public ActionResult RejectLead(int id, string Note)
        {
            var RejectLead = _serv.RejectLead(id, Note);
            return Json(3, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisqualifyLead(int id, string Note)
        {
            var DisqualifyLead = _serv.DisqualifyLead(id, Note);
            return Json(3, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddSalePrice(int id, double SalePrice)
        {
            var getSalePrice = _serv.EditSalePrice(id, SalePrice);
            return Json(1, JsonRequestBehavior.AllowGet);
        }

    }
}