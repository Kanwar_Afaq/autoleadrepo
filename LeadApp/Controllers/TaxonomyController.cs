﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LeadApp.Controllers
{
	[Restricted]
	public class TaxonomyController : Controller
    {
		public TaxonomyService _serv = new TaxonomyService();
		private string error = string.Empty;


        public ActionResult Index(string q)
        {
			if(!string.IsNullOrEmpty(q))
				return View(_serv.Get().Where(e=>e.Name.Contains(q)));
			else
				return View(_serv.Get());
        }





		public ActionResult Create() {
			ViewBag.TaxonomyCatId = new SelectList(_serv.GetCategory(), "TaxonomyCatId", "Name");
			
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(Taxonomy _data) {

			if (ModelState.IsValid) {

				bool isSuccess = _serv.Add(_data, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Created";

				if (ModelState.IsValid)
					return RedirectToAction("Index");
			}


			ViewBag.TaxonomyCatId = new SelectList(_serv.GetCategory(), "TaxonomyCatId", "Name", _data.TaxonomyCatId);

			return View(_data);
		}










		public ActionResult Edit(int? id) {
			if (!id.IsNumeric()) {
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}

			Taxonomy _data = _serv.Get().Where(e=>e.TaxonomyId == id.ConvertToInt()).FirstOrDefault();
			if (_data == null) {
				TempData["OutputMessage"] = "Record Not Found";
				return RedirectToAction("Index");
			}
			ViewBag.TaxonomyCatId = new SelectList(_serv.GetCategory(), "TaxonomyCatId", "Name", _data.TaxonomyCatId);
			return View(_data);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int? id, Taxonomy _data)
		{
			if (!id.IsNumeric())
			{
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}


			if (ModelState.IsValid) {

				bool isSuccess = _serv.Edit(_data, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Modified!";

				if (ModelState.IsValid)
					return RedirectToAction("Index");
				else
					return RedirectToAction("Edit", new { id = id });

			}


			ViewBag.TaxonomyCatId = new SelectList(_serv.GetCategory(), "TaxonomyCatId", "Name", _data.TaxonomyCatId);
			return View(_data);
		}









		[HttpPost]
		public ActionResult Delete(int? id) {
			if (!id.IsNumeric())
			{
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}


			bool isSuccess = _serv.Delete(id.ConvertToInt(), out error);
			TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

			if (!isSuccess)
				TempData["OutputMessage"] = "Error Occured: " + error;
			else
				TempData["OutputMessage"] = "Record Successfully Deleted";

			return RedirectToAction("Index");
		}


		public JsonResult isExist(string Name, int TaxonomyCatId, int? TaxonomyId)
		{
			return _serv.isExist(Name, TaxonomyCatId, TaxonomyId) == false ? Json(true, JsonRequestBehavior.AllowGet) : Json(string.Format("{0} is already exist.", Name), JsonRequestBehavior.AllowGet);
		}


    }
}
