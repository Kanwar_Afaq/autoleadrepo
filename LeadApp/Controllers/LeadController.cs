﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Collections.Specialized;
using System.Web.Mvc;
using System.Text;
using System;

namespace LeadApp.Controllers
{
	[Restricted]
	public class LeadController : Controller
    {
		public LeadService _serv = new LeadService();
		public WebsiteService _servOrg = new WebsiteService();
		public ManufacturerService _servMan = new ManufacturerService();
		public TaxonomyService _servTax = new TaxonomyService();
		private string error = string.Empty;


		public ActionResult Index(string q, string isRejected)
        {
			var _data = _serv.Get();
			if (isRejected == "true")
				_data = _data.Where(e => e.isPeddlePosted == false);

			if (!string.IsNullOrEmpty(q))
				return View(_data.Where(e => e.Name.Contains(q)));
			else
				return View(_data);
        }


		public ActionResult All(string q, string isRejected)
		{
			var _data = _serv.Get();
			if (isRejected == "true")
				_data = _data.Where(e=>e.isPeddlePosted == false);

			if (!string.IsNullOrEmpty(q))
				return View(_data.Where(e => e.Name.Contains(q)));
			else
				return View(_data);
		}


		public ActionResult Rejected(string q)
		{
			var _data = _serv.Get().Where(e => e.isPeddlePosted == false);

			if (!string.IsNullOrEmpty(q))
				return View(_data.Where(e => e.Name.Contains(q)));
			else
				return View(_data);
		}

		public ActionResult PeddlePosted(int? id)
		{
			if (!id.IsNumeric())
			{
				TempData["OutputMessage"] = "Record ID Not Valid!";				
			}
			else
			{
				bool isSuccess = _serv.PeddlePosted(id.ConvertToInt(), out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Modified!";


				return RedirectToAction("Rejected");
			}

			return RedirectToAction("Rejected");
		}

		public ActionResult Detail(int? id)
		{
			if (!id.IsNumeric())
			{
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}

			Lead _data = _serv.Get().Where(e => e.LeadId == id.ConvertToInt()).FirstOrDefault();
			if (_data == null)
			{
				TempData["OutputMessage"] = "Record Not Found";
				return RedirectToAction("Index");
			}

			return View(_data);
		}


		public ActionResult Create() {
			ViewBag.WebId = new SelectList(_servOrg.Get(), "WebId", "Name");
			ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name");
			ViewBag.ConditionId = new SelectList(_servTax.GetVehicleConditions(), "TaxonomyId", "Name");
			ViewBag.TitleId = new SelectList(_servTax.GetVehicleTitle(), "TaxonomyId", "Name");

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(Lead _data)
		{
			int LeadId = 0;
			string PeddleMessage = "";
			var WebGUID = _servOrg.Get().Where(e => e.WebId == _data.WebId).FirstOrDefault().WebGUID;


			if (ModelState.IsValid) {

				bool isSuccess = _serv.Add(_data, out error, out LeadId);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
				{				
					string URI = WebConfig.AppURL + "web/peddle-action-reverse.php";
					string myParameters = "Name=" + _data.Name 
						+ "&Phone=" + _data.Phone 
						+ "&Email=" + _data.Email 
						+ "&VehicleYear=" + _data.VehicleYear 
						+ "&ManufactureId=" + _data.ManufactureId
						+ "&VehicleModel=" + _data.VehicleModel
						+ "&ZipCode=" + _data.ZipCode
						+ "&WebGUID=" + WebGUID
						+ "&LeadId=" + LeadId;

					try
					{
						using (WebClient wc = new WebClient())
						{
							wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
							string HtmlResult = wc.UploadString(URI, myParameters);
							PeddleMessage = " Submission to Peddle Succeed.";
						}
					}
					catch (Exception e)
					{
						PeddleMessage = " Peddle Submission Failed (" + e.Message + ")";
					}

					TempData["OutputMessage"] = "Record Successfully Created. " + PeddleMessage;

				}
				if (ModelState.IsValid)
					return RedirectToAction("Create");
			}

			ViewBag.WebId = new SelectList(_servOrg.Get(), "WebId", "Name", _data.WebId);
			ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name", _data.ManufactureId);
			ViewBag.ConditionId = new SelectList(_servTax.GetVehicleConditions(), "TaxonomyId", "Name", _data.ConditionId);
			ViewBag.TitleId = new SelectList(_servTax.GetVehicleTitle(), "TaxonomyId", "Name", _data.TitleId);

			return View(_data);
		}

		public ActionResult Edit(int? id) {
			if (!id.IsNumeric()) {
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}

			Lead _data = _serv.Get().Where(e => e.LeadId == id.ConvertToInt()).FirstOrDefault();
			if (_data == null) {
				TempData["OutputMessage"] = "Record Not Found";
				return RedirectToAction("Index");
			}

			ViewBag.WebId = new SelectList(_servOrg.Get(), "WebId", "Name", _data.WebId);
			ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name", _data.ManufactureId);
			ViewBag.ConditionId = new SelectList(_servTax.GetVehicleConditions(), "TaxonomyId", "Name", _data.ConditionId);
			ViewBag.TitleId = new SelectList(_servTax.GetVehicleTitle(), "TaxonomyId", "Name", _data.TitleId);


			return View(_data);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int? id, Lead _data)
		{
			if (!id.IsNumeric())
			{
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}


			if (ModelState.IsValid) {

				bool isSuccess = _serv.Edit(_data, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Modified!";

				if (ModelState.IsValid)
					return RedirectToAction("Index");
				else
					return RedirectToAction("Edit", new { id = id });

			}

			ViewBag.WebId = new SelectList(_servOrg.Get(), "WebId", "Name", _data.WebId);
			ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name", _data.ManufactureId);
			ViewBag.ConditionId = new SelectList(_servTax.GetVehicleConditions(), "TaxonomyId", "Name", _data.ConditionId);
			ViewBag.TitleId = new SelectList(_servTax.GetVehicleTitle(), "TaxonomyId", "Name", _data.TitleId);


			return View(_data);
		}

		[HttpPost]
		public ActionResult Delete(int? id) {
			if (!id.IsNumeric())
			{
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}


			bool isSuccess = _serv.Delete(id.ConvertToInt(), out error);
			TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

			if (!isSuccess)
				TempData["OutputMessage"] = "Error Occured: " + error;
			else
				TempData["OutputMessage"] = "Record Successfully Deleted";

			return RedirectToAction("Index");
		}


		public JsonResult isExist(string Name, int? LeadId)
		{
			return _serv.isExist(Name, LeadId) == false ? Json(true, JsonRequestBehavior.AllowGet) : Json(string.Format("{0} is already exist.", Name), JsonRequestBehavior.AllowGet);
		}


    }
}
