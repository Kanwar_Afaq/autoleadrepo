﻿using LeadApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Controllers
{
	[Restricted]
	public class ReportsController : Controller
    {
		public LeadService _serv = new LeadService();
		public WebsiteService _servOrg = new WebsiteService();
		public ManufacturerService _servMan = new ManufacturerService();
		public TaxonomyService _servTax = new TaxonomyService();
		private string error = string.Empty;
        public DashBoardServices _dashboardservices = new DashBoardServices();


        // GET: Reports
        //public ActionResult Index()
        //{
        //    ViewBag.WebId = new SelectList(_servOrg.Get(), "WebId", "Name");
        //    ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name");

        //    return View();
        //}

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(DateTime? StartDate, DateTime? EndDate)
        {

            var d = _dashboardservices.ReportVM(StartDate, EndDate);
            return View(d);
        }
    }
}