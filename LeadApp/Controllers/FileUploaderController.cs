﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Controllers
{
	//[Restricted]
	public class FileUploaderController : Controller
    {
        //
        // GET: /FileUploader/
        SecurityService securityServices = new SecurityService();

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();

            }
        }

      
        public ActionResult UploadFileEdit(string RecordId,string Permission,string Title,string Source)
        {
            if (!string.IsNullOrEmpty(Permission))
            {
                List<string> strpermission = Permission.Trim().Split(',').ToList();
                var Groups = securityServices.GetAllGroups().ToList();          
                ViewBag.GroupDd = new MultiSelectList(Groups, "GroupName", "GroupName", strpermission);
                ViewBag.FileTitle = Title;
                ViewBag.Source = Source;
                ViewBag.RecordId = RecordId;
                ViewBag.ButtonTitle = "Update";
            }
            return PartialView("FileUploaderEdit");
        }

        public ActionResult UploadFilesJv(long serviceid)
        {
            ViewBag.ServiceId = serviceid;
            return PartialView("FileUploaderJV");
        }


        public ActionResult UploadFiles()
        {
            var Groups = securityServices.GetAllGroups();
            ViewBag.GroupDd = new MultiSelectList(Groups, "GroupName", "GroupName");
            ViewBag.ButtonTitle = "Save";
            return PartialView("FileUploader");
        }



        [HttpPost]
        public ActionResult UploadFiles(string FileTitle, HttpPostedFileBase FileSource, string Source, List<string> FilePermission, string PageName, string RecordId, string bntfileupload)
        {
          
            string FileName = string.Empty;
            string FileLink = string.Empty;
            string message = string.Empty;
            FileViewModel fileviewmodel = new FileViewModel();
            try
            {
                FileSystemHelper filesystemhelper = new FileSystemHelper();     
                int filesize = WebConstants.FileSize;
                string FileExtension = WebConstants.FileExtension;
                string TempFileFolder = WebConstants.TempFileUploadFolder;
              
                if (FileSource == null)
                {
                      message = "- No File Selected<br/>";
                }
               

                if (string.IsNullOrEmpty(FileTitle))
                {
					message += "- File Title cannot be empty<br/>";
                }

                if (FilePermission == null)
                {
					message += "- Please Select the group in which the file will exist<br/>";
                }             
                    if (FileSource != null)
                    {
                        if (FileSource.FileName != null)
                        {
                            if ((FileSource.ContentLength / 1024) > filesize)
                            {
								message += "- The size of the file should not exceed " + filesize + " kb <br/>";
                            }

                            string availableextensions = !string.IsNullOrEmpty(FileExtension) ? FileExtension : "jpg,jpeg,png,gif,doc,docx,xls,pdf,txt";
                            string fileExt = System.IO.Path.GetExtension(FileSource.FileName).Substring(1).ToLower();
                            if (!availableextensions.Contains(fileExt))
                            {
								message += "- Invalid type. Only the following types (" + availableextensions + ") are supported.<br/>";
                            }

                            if (message == string.Empty)
                            { 
                              
                                FileName = filesystemhelper.GenUniqueFileName(FileSource.FileName);
                                FileLink = WebConstants.PhysicalPath + TempFileFolder + FileName;
                                FileSource.SaveAs(FileLink);
                            }
                        }
                    }


                    if (message == string.Empty)
                    {                       
                        fileviewmodel.FileTitle = FileTitle;
                    
                        fileviewmodel.RecordId = Guid.NewGuid().ToString();
                        fileviewmodel.FileSource = FileName;

						string FileLinkFull = WebConstants.LogicalPath + TempFileFolder + fileviewmodel.FileSource;
                
                        fileviewmodel.FilePermission = string.Join(",", FilePermission);
                        LeadAppEntities _context = new LeadAppEntities();
                        var filepermission = _context.AppGroups.Where(m => FilePermission.Contains(m.GroupName)).Select(m=>m.GroupId);
                        fileviewmodel.FilePermissionIds = string.Join(",", filepermission.Select(s => s.ToString()).ToArray());
						fileviewmodel.FileLink = filepermission.Contains(Auth.GroupID) ? FileLinkFull : "#";


                        TempData["val"] = true;
                        TempData["RecordId"] = RecordId;                     

                        return new JsonResult { Data = new {  message = "File successfully uploaded", val = true, RecordId = RecordId, url = RenderRazorViewToString("FileUploadView", fileviewmodel) } };
                    }
                    else
                    {
                        
                        return new JsonResult { Data = new { message = message, val = false } };
                    }
           
            }
            catch { throw; }          
        }


        [HttpPost]
        public ActionResult UploadFilesEdit(string FileTitle, HttpPostedFileBase FileSource, string Source, List<string> FilePermission, string PageName, string RecordId)
        {
            string FileName = string.Empty;
            string FileLink = string.Empty;
            string message = string.Empty;
            FileViewModel fileviewmodel = new FileViewModel();
            try
            {
                FileSystemHelper filesystemhelper = new FileSystemHelper();
                int filesize = WebConstants.FileSize;
                string FileExtension = WebConstants.FileExtension;
                string TempFileFolder = WebConstants.TempFileUploadFolder;

                if (string.IsNullOrEmpty(FileTitle))
                {
                    message += "- File Title cannot be empty<br/>";
                }

                if (FilePermission == null)
                {
                    message += "- Please Select the group in which the file will exist<br/>";
                }
                if (FileSource != null)
                {
                    if (FileSource.FileName != null)
                    {
                        if ((FileSource.ContentLength / 1024) > filesize)
                        {
                            message += "- The size of the file should not exceed " + filesize + " kb <br/>";
                        }

                        string availableextensions = !string.IsNullOrEmpty(FileExtension) ? FileExtension : "jpg,jpeg,png,gif,doc,docx,xls,pdf,txt";
                        string fileExt = System.IO.Path.GetExtension(FileSource.FileName).Substring(1).ToLower();
                        if (!availableextensions.Contains(fileExt))
                        {
                            message += "- Invalid type. Only the following types (" + availableextensions + ") are supported.<br/>";
                        }

                        if (message == string.Empty)
                        {

                            FileName = filesystemhelper.GenUniqueFileName(FileSource.FileName);
                            FileLink = WebConstants.PhysicalPath + TempFileFolder + FileName;
                            FileSource.SaveAs(FileLink);
                        }
                    }
                }

                if (message == string.Empty)
                {
                    fileviewmodel.FileTitle = FileTitle;               
                    fileviewmodel.RecordId = RecordId;
                    if (!string.IsNullOrEmpty(FileName))
                    {
                         fileviewmodel.FileSource = FileName;
                    }
                    else
                    {
                         fileviewmodel.FileSource = Source;
                    }

					string FileLinkFull = WebConstants.LogicalPath + TempFileFolder + fileviewmodel.FileSource;
                
                    
                    fileviewmodel.FilePermission = string.Join(",", FilePermission);
                    LeadAppEntities _context = new LeadAppEntities();
                    var filepermission = _context.AppGroups.Where(m => FilePermission.Contains(m.GroupName)).Select(m => m.GroupId);
                    fileviewmodel.FilePermissionIds = string.Join(",", filepermission.Select(s => s.ToString()).ToArray());
					fileviewmodel.FileLink = filepermission.Contains(Auth.GroupID) ? FileLinkFull : "#";

                    return new JsonResult { Data = new { message = "File successfully uploaded", val = true, RecordId = RecordId, url = RenderRazorViewToString("FileUploadViewEdit", fileviewmodel) } };
                }
                else
                {

                    return new JsonResult { Data = new { message = message, val = false } };
                }

            }
            catch { throw; }
        }
    }

}