﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Controllers
{

    public class DashBoardController : Controller
    {
        public DashBoardServices _dashboardservices = new DashBoardServices();

        // GET: DashBoard
        public ActionResult Index()
        {
            var d = _dashboardservices.DashBoardReport(Auth.GroupID);       
            return View(d);
        }

        public ActionResult Report()
        {        
            return View();
        }
        [HttpPost]
        public ActionResult Report(DateTime? StartDate,DateTime? EndDate)
        {

            var d = _dashboardservices.ReportVM(StartDate,EndDate);            
            return View(d);
        }
    }
}