﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LeadApp.Controllers {
	[Restricted]
	public class ModuleController : Controller
	{
		public SecurityService _serv = new SecurityService();
		private string error = string.Empty;

		public JsonResult isModuleExist(string ModuleName, int ModuleID = 0) {
			return Json(_serv.isModuleExist(ModuleName, ModuleID), JsonRequestBehavior.AllowGet);
		}


		public ActionResult Index() {
			return View(_serv.GetAllModules());
		}

		public ActionResult _attributes()
		{
			return View();
		}





		public ActionResult Create() {
			return View();
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(AppModule getData) {
			if (!_serv.isModuleExist(getData.ModuleName, 0))
				ModelState.AddModelError(getData.ModuleName, "Module Name already exist");


			if (ModelState.IsValid) {
				bool isSuccess = _serv.CreateModule(getData, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Created";

				if (ModelState.IsValid)
					return RedirectToAction("Index");
			}

			return View();
		}








		public ActionResult Edit(int? id) {
			if (!HelperMethods.IsNumeric(id)) {
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}

			AppModule getData = _serv.GetModule(HelperMethods.ConvertToInt(id));
			if (getData == null) {
				TempData["OutputMessage"] = "Record Not Found";
				return RedirectToAction("Index");
			}

			return View(getData);
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int? id, AppModule getData) {
			if (ModelState.IsValid) {
				bool isSuccess = _serv.EditModule(HelperMethods.ConvertToInt(id), getData, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Modified!";

				if (ModelState.IsValid)
					return RedirectToAction("Index");
			}

			return View(getData);
		}





		[HttpPost]
		public ActionResult Delete(int? id)
		{
			if (!HelperMethods.IsNumeric(id))
			{
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}

			bool isSuccess = _serv.DeleteModule(HelperMethods.ConvertToInt(id), out error);
			TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

			if (!isSuccess)
				TempData["OutputMessage"] = "Error Occured: " + error;
			else
				TempData["OutputMessage"] = "Record Successfully Deleted";

			return RedirectToAction("Index");
		}









		public ActionResult Config(int? ModuleId)
		{
			List<AppModuleConfig> _data = _serv.getAppModuleConfig();
			if (ModuleId.IsNumeric())
				_data = _data.Where(e => e.ModuleID.Equals(ModuleId)).ToList();

			ViewBag.ModuleId = ModuleId;
			return View(_data);
		}



		public ActionResult ConfigEdit(int? id, int? ModuleId)
		{
			if (!HelperMethods.IsNumeric(id))
			{
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Config");
			}

			AppModuleConfig getData = _serv.getAppModuleConfig(HelperMethods.ConvertToInt(id));
			if (getData == null)
			{
				TempData["OutputMessage"] = "Record Not Found";
				return RedirectToAction("Config", new { ModuleId = ModuleId });
			}

			ViewBag.ModuleId = ModuleId;
			return View(getData);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult ConfigEdit(int? id, int? ModuleId, AppModuleConfig getData)
		{
			if (ModelState.IsValid)
			{
				bool isSuccess = _serv.ConfigEdit(HelperMethods.ConvertToInt(id), getData, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Modified!";

				if (ModelState.IsValid)
					return RedirectToAction("Config", new { ModuleId = ModuleId });
			}

			ViewBag.ModuleId = ModuleId; 
			return View(getData);
		}






	}
}