﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace LeadApp.Controllers {
	[Restricted]
	public class TaskController: Controller {
		TaskServices _serv = new TaskServices();
		UserService _servUser = new UserService();
		private string error = string.Empty;


        public ActionResult Index(string q)
        {
			IEnumerable<Task> _data = _serv.GetAllTasks();

            if (!string.IsNullOrEmpty(q))
				return View(_data.Where(e => e.Title.Contains(q) || e.Description.Contains(q)));
            else
				return View(_data);			
		}






		public ActionResult Create() {
			ViewBag.AssigneeUserID = new MultiSelectList(_servUser.GetEmployees(), "UserId", "ContactName");
			ViewBag.StatusID = new SelectList(_serv.GetStatus(), "StatusID", "StatusName");
			return View();
		}

		[HttpPost]
		public ActionResult Create(Task Data, List<int> AssigneeUserID) {
			string error = "";

            if (AssigneeUserID.Count == 0)
                ModelState.AddModelError("AssigneeUserID", "You must assign atleast one user");


			if (ModelState.IsValid) {
					bool isCreate = _serv.AddTask(Data, AssigneeUserID, out error);
					TempData["AlertCSS"] = isCreate ? "alert success" : "alert error";

					if (!isCreate)
						ModelState.AddModelError("", error);
					else
						TempData["OutputMessage"] = "Task Successfully Created";

					if (ModelState.IsValid)
						return RedirectToAction("Index");
			}

			ViewBag.AssigneeUserID = new MultiSelectList(_servUser.GetEmployees(), "UserId", "ContactName", AssigneeUserID);
			ViewBag.StatusID = new SelectList(_serv.GetStatus(), "StatusID", "StatusName", Data.StatusID);

			return View(Data);
		}







		public ActionResult Edit(Guid? id)
		{
			if (!HelperMethods.IsGUID(id))
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

			Task _data = _serv.GetTask(id.ConvertToGUID());
			if (_data == null)
				return HttpNotFound();

			if (_data.CreatedBy != Auth.UserID)
			{
				TempData["AlertCSS"] = "alert error";
				TempData["OutputMessage"] = "You can edit your own task only";
				return RedirectToAction("Index");
			}

			ViewBag.AssigneeUserID = new MultiSelectList(_servUser.GetEmployees(), "UserId", "ContactName", _data.TaskAssignees.Select(e => e.AssigneeUserID));
			ViewBag.StatusID = new SelectList(_serv.GetStatus(), "StatusID", "StatusName", _data.StatusID);


			return View(_data);
		}

		[HttpPost]
		public ActionResult Edit(Task Data, List<int> AssigneeUserID, string Mode)
		{
			

			if (AssigneeUserID.Count == 0)
				ModelState.AddModelError("AssigneeUserID", "You must assign atleast one user");


			if (ModelState.IsValid)
			{
				bool isSuccess = _serv.Edit(Data, AssigneeUserID, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Task Successfully Edited";

				if (ModelState.IsValid)
					return RedirectToAction("Index");
			}

			ViewBag.AssigneeUserID = new MultiSelectList(_servUser.GetEmployees(), "UserId", "ContactName", AssigneeUserID);
			ViewBag.StatusID = new SelectList(_serv.GetStatus(), "StatusID", "StatusName", Data.StatusID);

			return View(Data);
		}








		public ActionResult Detail(Guid id) {
			Task Data = _serv.GetTask(id);
			ViewBag.TaskStatus = new SelectList(_serv.GetStatus(), "StatusID", "StatusName", Data.StatusID);
			ViewBag.StatusID = new SelectList(_serv.GetStatus(), "StatusID", "StatusName");
			return View(Data);
		}

		[HttpPost]
        public ActionResult Detail(TaskRespons Data)
        {
			ViewBag.TaskStatus = new SelectList(_serv.GetStatus(), "StatusID", "StatusName", Data.StatusID);
			ViewBag.StatusID = new SelectList(_serv.GetStatus(), "StatusID", "StatusName");


			if (ModelState.IsValid) {

				bool isCreate = _serv.AddReply(Data, out error);
				TempData["AlertCSS"] = isCreate ? "alert success" : "alert error";

				if (!isCreate)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Task Reply Successfully Submitted";

				if (ModelState.IsValid)
					return RedirectToAction("Detail", new { id = Data.TaskID });
			}


			return View(Data);
		}



		public ActionResult _TaskSummary(Guid id) {
			return View(_serv.GetAllTaskReply(id));
		}


		public ActionResult Delete(Guid id) {
			//ViewData["ID"] = id;
			if (ModelState.IsValid) {

				bool isCreate = _serv.DeleteTask(id, out error);
				TempData["AlertCSS"] = isCreate ? "alert success" : "alert error";

				if (!isCreate)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Task  Successfully deleted";

				if (ModelState.IsValid) {
					return RedirectToAction("Index");
				}

			}
			return RedirectToAction("Index");

		}



		public ActionResult Closed(Guid id) {

			bool isSuccess = _serv.CloseTask(id, out error);
			TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

			if (!isSuccess)
				ModelState.AddModelError("", error);
			else
				TempData["OutputMessage"] = "Task Closed Successfully!";

			return RedirectToAction("Detail", new { id = id });

		}

		[HttpPost]
		public ActionResult ChangeStatus(Guid TaskID, string status) {
			bool isSuccess = false;
			if (status == "Completed & Close")
				isSuccess = _serv.CloseTask(TaskID, out error);
			else
				isSuccess = _serv.ChangeStatus(TaskID, status, out error);

			TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";	
			if (!isSuccess)
				ModelState.AddModelError("", error);
			else
				TempData["OutputMessage"] = "Task Status Updated Successfully";

			return RedirectToAction("Detail", new { id = TaskID });

		}


	}
}