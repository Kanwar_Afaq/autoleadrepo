﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LeadApp.Controllers
{
	[Restricted]
	public class GroupController : Controller
    {
		public SecurityService _serv = new SecurityService();
		private string error = string.Empty;


        public ActionResult Index(string q)
        {
			if(!string.IsNullOrEmpty(q))
				return View(_serv.GetAllGroups().Where(e=>e.GroupName.Contains(q)));
			else
				return View(_serv.GetAllGroups());
        }





		public ActionResult Create() {
			ViewBag.ApppModule = _serv.GetAllModules();
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(AppGroup appgroup, FormCollection fm) {
			List<AppModule> setAllmodules = new List<AppModule>();

			if (ModelState.IsValid) {

				AppGroup processData = _serv.ProcessGroupData(appgroup, fm, out setAllmodules);

				bool isSuccess = _serv.CreateGroup(processData, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Created";

				if (ModelState.IsValid)
					return RedirectToAction("Index");
			}


			ViewBag.ApppModule = setAllmodules;

			return View(appgroup);
		}










		public ActionResult Edit(int? id) {
			if (!HelperMethods.IsNumeric(id)) {
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}

			AppGroup getData = _serv.getAppGroup(HelperMethods.ConvertToInt(id));
			if (getData == null) {
				TempData["OutputMessage"] = "Record Not Found";
				return RedirectToAction("Index");
			}

			ViewBag.ApppModule = _serv.EditGroupData(getData);	
			return View(getData);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int? id, AppGroup appgroup, FormCollection fm) {
			if (!HelperMethods.IsNumeric(id)) {
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}

			List<AppModule> setAllmodules = new List<AppModule>();

			if (ModelState.IsValid) {

				AppGroup processData = _serv.ProcessGroupData(appgroup, fm, out setAllmodules);
				bool isSuccess = _serv.EditGroup(HelperMethods.ConvertToInt(id), processData, out error);
				TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Record Successfully Modified!";

				if (ModelState.IsValid)
					return RedirectToAction("Index");
				else
					return RedirectToAction("Edit", new { id = id });

			} else {

				return RedirectToAction("Edit", new { id = id });
			}

			return View(appgroup);
		}









		[HttpPost]
		public ActionResult Delete(int? id) {
			if (!HelperMethods.IsNumeric(id)) {
				TempData["OutputMessage"] = "Record ID Not Valid!";
				return RedirectToAction("Index");
			}


			bool isSuccess = _serv.DeleteGroup(HelperMethods.ConvertToInt(id), out error);
			TempData["AlertCSS"] = isSuccess ? "alert success" : "alert error";

			if (!isSuccess)
				TempData["OutputMessage"] = "Error Occured: " + error;
			else
				TempData["OutputMessage"] = "Record Successfully Deleted";

			return RedirectToAction("Index");
		}


    }
}
