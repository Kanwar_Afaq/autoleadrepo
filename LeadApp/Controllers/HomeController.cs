﻿using LeadApp.Helpers;
using LeadApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Controllers
{
	public class HomeController : Controller
	{

        public DashBoardServices _dashboardservices = new DashBoardServices();
        public LeadPhoneServices _serv = new LeadPhoneServices();


        [Restricted]
        public ActionResult Index()
        {         
            if (Auth.GroupID==5)
            {
                var d = _dashboardservices.DashBoardReport(Auth.GroupID);
                ViewBag.PendingLeads = _serv.getPendingLeads();
                return View("Manager",d);
            }
            else if(Auth.GroupID==6)
            {
                var d = _dashboardservices.DashBoardReport(Auth.GroupID);
                return View("Manager", d);
            }
            else
            {                
                return View("Dashboard");                
            }
        }

     
        [Restricted]
        public ActionResult Dashboard()
        {
           return View();
        }



		public ActionResult Restricted()
		{
			return View();
		}
	}
}