﻿using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LeadApp.Controllers
{
	public class FormController : Controller
	{
		public LeadService _serv = new LeadService();
		public WebsiteService _servOrg = new WebsiteService();
		public ManufacturerService _servMan = new ManufacturerService();
		public TaxonomyService _servTax = new TaxonomyService();
		private string error = string.Empty;


		public ActionResult Index(Lead _getdata, int? WebId, Guid? WebGUID)
		{
			if (!WebGUID.IsGUID())
				WebGUID = _servOrg.Get().FirstOrDefault().WebGUID;

			Website _data = _servOrg.Get().Where(e => e.WebGUID == WebGUID.ConvertToGUID()).FirstOrDefault();

			if (_data != null)
			{
				ViewBag.WebId = _data.WebId;
				ViewBag.WebGUID = _data.WebGUID;
				ViewBag.Name = _data.Name;
				ViewBag.Domain = _data.Domain;

				ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name");
				ViewBag.ConditionId = new SelectList(_servTax.GetVehicleConditions(), "TaxonomyId", "Name");
				ViewBag.TitleId = new SelectList(_servTax.GetVehicleTitle(), "TaxonomyId", "Name");

				return View();
			}
			else
				return RedirectToAction("Web1", _getdata);
		}

		[HttpPost]
		//[ValidateAntiForgeryToken]
		public ActionResult Submit(Lead _data, Guid? WebGUID, string r = "")
		{
			int LeadId = 0;

			if (ModelState.IsValid)
			{
				_data.WebId = _servOrg.Get().Where(e => e.WebGUID == WebGUID.ConvertToGUID()).FirstOrDefault().WebId;

				bool isSuccess = _serv.Add(_data, out error, out LeadId);
				TempData["AlertCSS"] = isSuccess ? "alert success bg-success" : "alert error bg-danger";

				if (!isSuccess)
					ModelState.AddModelError("", error);
				else
					TempData["OutputMessage"] = "Thank you for providing us the details. We will contact you shortly!";

				if (isSuccess)
					return RedirectToAction("Web", new { WebId = _data.WebId });
			}

			ViewBag.WebId = _data.WebId;
			ViewBag.WebGUID = WebGUID.ConvertToGUID();

			//ViewBag.WebId = new SelectList(_servOrg.Get(), "WebId", "Name", _data.WebId);
			ViewBag.ManufactureId = new SelectList(_servMan.Get(), "ManufactureId", "Name", _data.ManufactureId);
			ViewBag.ConditionId = new SelectList(_servTax.GetVehicleConditions(), "TaxonomyId", "Name", _data.ConditionId);
			ViewBag.TitleId = new SelectList(_servTax.GetVehicleTitle(), "TaxonomyId", "Name", _data.TitleId);

			return View("Index", _data);
		}



		public JsonResult Submit(Lead _data, Guid? WebGUID, string key = "", string r = "")
		{

			Dictionary<string, string> dic = new Dictionary<string, string>();
			string ConvDivMsg = "";
			string DivCSSClass = "alert error bg-danger";
			string getReturnURL = "";
			int LeadId = 0;


			if (!WebGUID.IsGUID())
			{
				ConvDivMsg = "Website ID not Valid!";
			}
			else if (!string.IsNullOrEmpty(r))
			{
				ConvDivMsg = "Request is not Valid. Please try again!";
			}
			else if (ModelState.IsValid)
			{
				_data.WebId = _servOrg.Get().Where(e => e.WebGUID == WebGUID.ConvertToGUID()).FirstOrDefault().WebId;

				bool isSuccess = _serv.Add(_data, out error, out LeadId);
				DivCSSClass = isSuccess ? "alert success bg-success" : "alert error bg-danger";

				if (!isSuccess)
					ConvDivMsg = "Request is not Valid (" + error + ")";
				else
					ConvDivMsg = "Thank you for providing us the details. We will contact you shortly!";
			}
			else
			{
				string validationErrors = string.Join(",",
					ModelState.Values.Where(E => E.Errors.Count > 0)
					.SelectMany(E => E.Errors)
					.Select(E => E.ErrorMessage)
					.ToArray());

				ConvDivMsg = "Request is not Valid! " + validationErrors;
			}
			dic.Add("Message", ConvDivMsg);
			dic.Add("divClass", DivCSSClass);
			dic.Add("LeadId", LeadId.ConvertToString());
			return Json(dic, JsonRequestBehavior.AllowGet);
		}


		public JsonResult Ack(Guid? WebGUID, string LeadId, string Message, bool isPosted = false, string r = "")
		{

			Dictionary<string, string> dic = new Dictionary<string, string>();
			string ConvDivMsg = "";
			string DivCSSClass = "";
			string getReturnURL = "";
			string error = "";


			if (!WebGUID.IsGUID())
			{
				ConvDivMsg = "Website ID not Valid!";
			}
			else if (!string.IsNullOrEmpty(r))
			{
				ConvDivMsg = "Request is not Valid. Please try again!";
			}
			else if (!LeadId.IsNumeric())
			{
				ConvDivMsg = "Lead ID is not Valid. Please try again!";
			}
			else if (ModelState.IsValid)
			{
				bool isSuccess = _serv.Ack(LeadId.ConvertToInt(), Message, out error);
				DivCSSClass = isSuccess ? "alert success bg-success" : "alert error bg-danger";

				if (!isSuccess)
					ConvDivMsg = "Request is not Valid (" + error + ")";
				else
					ConvDivMsg = "";
			}
			else
			{
				string validationErrors = string.Join(",",
					ModelState.Values.Where(E => E.Errors.Count > 0)
					.SelectMany(E => E.Errors)
					.Select(E => E.ErrorMessage)
					.ToArray());

				ConvDivMsg = "Request is not Valid! " + validationErrors;
			}
			dic.Add("Message", ConvDivMsg);
			dic.Add("divClass", DivCSSClass);
			return Json(dic, JsonRequestBehavior.AllowGet);
		}


		public ActionResult Web(int WebId)
		{
			Website _data = _servOrg.Get().Where(e => e.WebId == WebId).FirstOrDefault();
			ViewBag.WebId = _data.WebId;
			ViewBag.OrgGUID = _data.WebGUID;
			ViewBag.Name = _data.Name;
			ViewBag.Domain = _data.Domain;
			ViewBag.isShort = true;

			return View();
		}





		public ActionResult Web1()
		{
			int WebId = 1;
			Website _data = _servOrg.Get().Where(e => e.WebId == WebId).FirstOrDefault();
			ViewBag.WebId = _data.WebId;
			ViewBag.OrgGUID = _data.WebGUID;
			ViewBag.Name = _data.Name;
			ViewBag.Domain = _data.Domain;
			ViewBag.isShort = true;

			return View();
		}

		public ActionResult Web2()
		{
			int WebId = 2;
			Website _data = _servOrg.Get().Where(e => e.WebId == WebId).FirstOrDefault();
			ViewBag.WebId = _data.WebId;
			ViewBag.OrgGUID = _data.WebGUID;
			ViewBag.Name = _data.Name;
			ViewBag.Domain = _data.Domain;
			ViewBag.isShort = true;

			return View();
		}

	}
}