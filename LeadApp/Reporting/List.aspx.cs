﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LeadApp.Helpers;
using LeadApp.Services;
using LeadApp.Models;

namespace LeadApp.Reporting
{
	public partial class List : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
					ReportViewer getReportViewer = (ReportViewer)Master.FindControl("ShowReportViewer");
					if (getReportViewer != null)
					{
						LeadService _serv = new LeadService();

						DateTime StartDate = DateTime.Now.AddYears(-10);
						DateTime EndDate = DateTime.Now.AddYears(10);


						var start = Request.QueryString["start"];
						if (start.IsDateTime())
							StartDate = start.ConvertToDate();

						var end = Request.QueryString["end"];
						if (end.IsDateTime())
							EndDate = end.ConvertToDate().AddDays(1).AddSeconds(-1);

						var WebId = Request.QueryString["WebId"];
						var ManufactureId = Request.QueryString["ManufactureId"];
						var Yrno = Request.QueryString["Yrno"];


						IEnumerable<LeadViewModel> _data = _serv.GetList().Where(x=>x.EntryDate >= StartDate && x.EntryDate <= EndDate);
						if (WebId.IsNumeric())
							_data = _data.Where(x => x.WebId == WebId.ConvertToInt());

						if(ManufactureId.IsNumeric())
							_data = _data.Where(x => x.ManufactureId == ManufactureId.ConvertToInt());

						if (Yrno.IsNumeric())
							_data = _data.Where(x => x.VehicleYear == Yrno.ConvertToInt());


						string ShowReport = "List";
						string getPath = Server.MapPath(".");
						getReportViewer.LocalReport.ReportPath = getPath + "/" + ShowReport + ".rdlc";
						getReportViewer.ProcessingMode = ProcessingMode.Local;
						ReportDataSource DataSource1 = new ReportDataSource("DataSet1", _data);


						getReportViewer.LocalReport.DataSources.Clear();
						getReportViewer.Width = 950;
						getReportViewer.ShowToolBar = true;
						getReportViewer.SizeToReportContent = true;
						getReportViewer.AsyncRendering = true;
						getReportViewer.LocalReport.DataSources.Add(DataSource1);

						getReportViewer.LocalReport.Refresh();
					}
			}
		}
	}
}