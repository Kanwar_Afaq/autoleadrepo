﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LeadApp.Services;
using LeadApp.Models;

namespace LeadApp.Helpers {
	/// <summary>
	/// Authorization Key of Login User
	/// </summary>
	[Serializable]
	public class Auth {

		public static string GetNavLink()
		{
			SecurityService _rep = new SecurityService();

			string _output = string.Empty;
			var protocol = HttpContext.Current.Request.Url.Scheme;

			string isActiveCSS = string.Empty;
			var FullURL = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
			var AbsolutePath = FullURL.AbsolutePath;
			string ControllerName = (FullURL.Segments.Count() > 1) ? FullURL.Segments[1].ConvertToString().ToString().Replace("/", "") : "";
			


			List<AppModule> modules = _rep.getUserModules().Where(e => e.ShowInMenu == true).OrderBy(e => e.Sort).ToList();
			_output += "<ul>\n";
			foreach (var x in modules.Where(e => e.isPrimary == true))
			{
				string ModuleIndex = (!string.IsNullOrEmpty(x.ModuleIndex) ? "/" + x.ModuleIndex : "");
				string ModuleParam = (!string.IsNullOrEmpty(x.ModuleParam) ? "/?" + x.ModuleParam : "");
				var Link = "/" + x.ModuleLink + ModuleIndex + ModuleParam;
				if (ControllerName == x.ModuleLink)
					isActiveCSS = " active";

				_output += "\t<li class=\"" + x.CssClass + isActiveCSS + "\" title=\"" + x.ModuleName + "\"><a href=\"" + Link + "\"><span>" + x.ModuleName + "</span></a></li>\n";
				isActiveCSS = "";
			}
			_output += "</ul>\n\n";




			_output += "<div class=\"expand-ctrl-box hz\"><span class=\"expand-ctrl\"></span></div>\n";
			_output += "<ul>\n";
			foreach (var x in modules.Where(e => e.isPrimary == false))
			{
				//u.Action("Index", x.ModuleLink, null, protocol)
				string ModuleIndex = (!string.IsNullOrEmpty(x.ModuleIndex) ? "/" + x.ModuleIndex : "");
				string ModuleParam = (!string.IsNullOrEmpty(x.ModuleParam) ? "/?" + x.ModuleParam : "");
				var Link = "/" + x.ModuleLink + ModuleIndex + ModuleParam;
				if (ControllerName == x.ModuleLink)
					isActiveCSS = " active";
				
				_output += "\t<li class=\"" + x.CssClass + isActiveCSS + "\" title=\"" + x.ModuleName + "\"><a href=\"" + Link + "\"><span>" + x.ModuleName + "</span></a></li>\n";
				isActiveCSS = "";
			}
			_output += "</ul>\n";




			return _output;
		}


		public static bool isAuthorize {
			get {
				if (IsActive && isLogin)
					return true;
				else
					return false;
			}
		}
		public static bool isLogin {
			get {
				return HttpContext.Current.Session["isLogin"].ConvertToBool();
			}
		}
		public static long UserLogID {
			get {
				return HttpContext.Current.Session["UserLogID"].ConvertToInt();
			}
		}
		public static int UserID {
			get {
				return HttpContext.Current.Session["UserID"].ConvertToInt();
			}
		}
		public static Guid UserGUID {
			get {
				return HttpContext.Current.Session["UserGUID"].ConvertToGUID();
			}
		}
		public static string UserName {
			get {
				return HttpContext.Current.Session["UserName"].ConvertToString();
			}
		}
		public static string Name
		{
			get
			{
				return HttpContext.Current.Session["Name"].ConvertToString();
			}
		}
		public static string Email
		{
			get {
				return HttpContext.Current.Session["Email"].ConvertToString();
			}
		}
		public static string Signature
		{
			get
			{
				return HttpContext.Current.Session["Signature"].ConvertToString();
			}
		}
		public static int GroupID {
			get {
				return HttpContext.Current.Session["GroupID"].ConvertToInt();
			}
		}

		public static bool IsActive {
			get {
				return HttpContext.Current.Session["IsActive"].ConvertToBool();
			}
		}
		public static string Thumbnail {
			get {
				return HttpContext.Current.Session["Thumbnail"].ConvertToString();
			}
		}
		public static string ThumbImg{
		 get{
			 string UserThumb = WebConstants.Default_UserThumbURL;
			 if (!string.IsNullOrEmpty(Thumbnail))
				 UserThumb = WebConstants.UserProfileThumbURL + "thb32-" + Thumbnail;

			 return UserThumb;
		 }
		}
		public static int ActiveTasks {
			get {
				try {
					TaskServices TS = new TaskServices();
					return TS.GetAllTasks().Where(e => e.isClosed != true).Count();
				} catch {
					return 0;
				}
			}
		}
		public static string AlertMessage {
			get {
				string Msg = string.Empty;
				//if (!UserAuthorization.IsActive)
				//	Msg += ("You need to activate your account before start using website services<br/>");
				//if (UserAuthorization.PostStatusId == WebConstants.ModerationPostStatusID)
				//	Msg += ("Your account is held for moderation. You can use website services once moderator activated your account.<br/>");

				return Msg;
			}
		}


	}
}