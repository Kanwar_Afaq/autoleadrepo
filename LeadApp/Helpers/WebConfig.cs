﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using LeadApp.Services;
using LeadApp.Models;

namespace LeadApp.Helpers {


	public static class WebConfig {
		private static SecurityService _Serv = new SecurityService();
		private static List<AppModuleConfig> _sysConfig = new List<AppModuleConfig>();

		public static List<AppModuleConfig> GetSysConfig()
		{
			_sysConfig = _Serv.getAppModuleConfig();
			return _sysConfig;
		}

		static WebConfig()
		{
			_sysConfig = GetSysConfig();
		}

		//Implement to Refresh the Cache Variables
		public static string RefreshCache(this List<AppModuleConfig> configs)
		{
			return _sysConfig.Exists(e => e.ConfigKey == "AppURL") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "AppURL").ConfigValue : "";
		}


		public static string GetMeta(string MetaField)
		{
			return _sysConfig.Exists(e => e.ConfigKey == MetaField) ? _sysConfig.FirstOrDefault(e => e.ConfigKey == MetaField).ConfigValue : "";
		}


		public static string Title
		{
			get
			{
				string _title = GetMeta("Title");
				return (string.IsNullOrEmpty(_title)) ? WebConstants.AppTitle : _title;
			}

		}

		public static string PhysicalPath
		{
			get
			{
				return GetMeta("PhysicalPath");
			}

		}

		public static string AppURL
		{
			get
			{
				return GetMeta("AppURL"); 
			}

		}


		#region SMTP Info
		public static bool EmailTestmodeEnable
		{
			get
			{
				string _data = GetMeta("EmailTestmodeEnable");
				return (_data.ToLower() == "false") ? false : true;
			}

		}
		public static string TestModeEmail
		{
			get
			{
				string _data = GetMeta("TestModeEmail");
				return (_data);
			}

		}

		public static string SMTPHost
		{
			get
			{
				return _sysConfig.Exists(e => e.ConfigKey == "SMTPHost") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "SMTPHost").ConfigValue : "localhost";
			}
		}

		public static string SMTPPort
		{
			get
			{
				return _sysConfig.Exists(e => e.ConfigKey == "SMTPPort") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "SMTPPort").ConfigValue : "25";
			}
		}

		public static string SMTPUsername
		{
			get
			{
				return _sysConfig.Exists(e => e.ConfigKey == "SMTPUsername") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "SMTPUsername").ConfigValue : "";
			}
		}

		public static string SMTPPassword
		{
			get
			{
				return _sysConfig.Exists(e => e.ConfigKey == "SMTPPassword") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "SMTPPassword").ConfigValue : "";
			}
		}

		public static string FromEmailID
		{
			get
			{
				return _sysConfig.Exists(e => e.ConfigKey == "FromEmailID") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "FromEmailID").ConfigValue : "test@sepiasolutions.com";
			}
		}

		public static string EmailFromName
		{
			get
			{
				return _sysConfig.Exists(e => e.ConfigKey == "EmailFromName") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "EmailFromName").ConfigValue : "Sepia System";
			}
		}

		public static string BCCAddress
		{
			get
			{
				return _sysConfig.Exists(e => e.ConfigKey == "BCCAddress") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "BCCAddress").ConfigValue : "";
			}
		}

		public static string EmailSignature
		{
			get
			{
				return _sysConfig.Exists(e => e.ConfigKey == "EmailSignature") ? _sysConfig.FirstOrDefault(e => e.ConfigKey == "EmailSignature").ConfigValue : "";
			}
		}
		#endregion





























		///Add Cookie value to use in User Login and other places
		public static void setWebsiteCookie() {
			if (HttpContext.Current.Request.Cookies["CookieVerify"] == null) {
				HttpCookie CookieVerify = new HttpCookie("CookieVerify", "true");
				CookieVerify.Expires = DateTime.Now.AddDays(365d);
				CookieVerify.Path = "/";
				HttpContext.Current.Response.Cookies.Add(CookieVerify);
			}
		}

		public static bool isCookieEnable {
			get {
				if (HttpContext.Current.Request.Cookies["CookieVerify"] != null)
					return true;
				else
					return false;
			}
		}       

		public static string CaptchaTextType {
			get {
				string val = ConfigurationManager.AppSettings["CaptchaTextType"];
				return !String.IsNullOrEmpty(val) ? (!(val.ToLower().Equals("default")) ? val : WebConstants.CaptchaTextType) : WebConstants.CaptchaTextType;
			}
		}
		public static int CaptchaLength {
			get {
				string val = ConfigurationManager.AppSettings["CaptchaLength"];
				return String.IsNullOrEmpty(val) ? WebConstants.CaptchaLength : (HelperMethods.IsNumeric(val) ? HelperMethods.ConvertToInt(val) : WebConstants.CaptchaLength);
			}
		}

		public static string EncryptedAbsoluteURL {
			get {
				return EncryptDecryptHelper.Encrypt(HttpContext.Current.Request.Url.ToString());
			}
		}



	}
}