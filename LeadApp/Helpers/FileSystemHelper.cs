﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Permissions;
using System.IO;
using System.Security;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.Mvc;
using System.Text;
using System.Web.UI;
using LeadApp.Models;
using LeadApp.Services;

namespace LeadApp.Helpers {
	public class FileSystemHelper {

        public static void UplMultiFile(Guid RefGUID, IEnumerable<FileViewModel> filelists)
        {
            FileUploaderServices fileuploadservices = new FileUploaderServices();

            if (filelists != null)
            {
                foreach (var item in filelists)
                {
                    item.ReferenceGUID = RefGUID;
                
                    fileuploadservices.AddFileAttacments(item);
                    string temparoryfile = WebConstants.PhysicalPath + WebConstants.TempFileUploadFolder + item.FileSource;
                    String finalfile = WebConstants.PhysicalPath + WebConstants.FileFinalUploadFolder + item.FileSource;
                    if (!System.IO.File.Exists(finalfile))
                    {
                        if (System.IO.File.Exists(temparoryfile))
                        {
                            System.IO.File.Copy(temparoryfile, finalfile);
                        }
                    }
                }
            }
        }


		public static string GetXMLFile(string FileName) {
			try {
				if (HelperMethods.isFileExist("App_Data\\" + FileName))
					return ("App_Data\\" + FileName);
				else
					return "";
			} catch {
				return "";
			}
		}


		/// <summary>
		/// Upload File to Server
		/// </summary>
		/// <param name="FileFieldName"></param>
		/// <param name="FileSavePath"></param>
		/// <param name="UplFileExtensions"></param>
		/// <param name="UplFileSize"></param>
		/// <param name="SaveFileName"></param>
		/// <param name="Error"></param>
		/// <returns></returns>
		public bool UploadFile(string FileFieldName, string FileSavePath, string UplFileExtensions, int UplFileSize, out string SaveFileName, out string Error) {
			Error = "";
			SaveFileName = "";
			int FileSizePermit = UplFileSize * 1024;
			var UploadedFile = HttpContext.Current.Request.Files[FileFieldName];


			if (!Directory.Exists(FileSavePath))
				Error += "File directory do not exist<br/>\n";

			var permission = new FileIOPermission(FileIOPermissionAccess.Write, FileSavePath);
			var permissionSet = new PermissionSet(PermissionState.None);
			permissionSet.AddPermission(permission);
			if (!permissionSet.IsSubsetOf(AppDomain.CurrentDomain.PermissionSet))
				Error += "File Permission does not permit to upload<br/>\n";


			if (UploadedFile.ContentLength > FileSizePermit)
				Error += "File Size eceed the allocated Limit of (" + UplFileSize + "KB)<br/>\n";


			string newFileNameOnServer = GenUniqueFileName(UploadedFile.FileName);
			string FileUploadedtypecheckthendeleteifprob = "@" + FileSavePath + newFileNameOnServer;
			string extension = Path.GetExtension(FileUploadedtypecheckthendeleteifprob);
			if (!UplFileExtensions.Contains(extension.ToLower()))
				Error = "Invalid File Uploaded. Only [" + UplFileExtensions + "] Files allowed.<br/>";


			if (string.IsNullOrEmpty(Error)) {
				UploadedFile.SaveAs(FileSavePath + newFileNameOnServer);
				SaveFileName = newFileNameOnServer;
				return true;
			}


			return false;
		}


		public string GenUniqueFileName(dynamic Filename) {
			string renamestr = DateTime.Today.Day.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
			return Path.GetFileNameWithoutExtension(Filename.Replace(" ", "").Replace("&", "")) + renamestr + Path.GetExtension(Filename);
		}





		/// <summary>
		/// Generate File Thumnail Image
		/// </summary>
		/// <param name="InputFile"></param>
		/// <param name="ThumbDimension"></param>
		/// <returns></returns>
		public string GenThumb(string InputFile, int ThumbDimension) {
			// Load image.
			Image image = Image.FromFile(InputFile);

			// Compute thumbnail size.
			Size thumbnailSize = GetThumbnailSize(image, ThumbDimension);

			// Get thumbnail.
			Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero);

			// Save thumbnail.
			string OutPutFile = Path.GetFullPath(InputFile).Replace(Path.GetFileName(InputFile), "") + "thb" + ThumbDimension + "-" + Path.GetFileName(InputFile);
			thumbnail.Save(OutPutFile);

			return Path.GetFileName(OutPutFile);
		}

		private static Size GetThumbnailSize(Image original, int ThumbDimension) {
			// Maximum size of any dimension.
			//const int ThumbDimension = 40;

			// Width and height.
			int originalWidth = original.Width;
			int originalHeight = original.Height;

			// Compute best factor to scale entire image based on larger dimension.
			double factor;
			if (originalWidth > originalHeight) {
				factor = (double)ThumbDimension / originalWidth;
			} else {
				factor = (double)ThumbDimension / originalHeight;
			}

			// Return thumbnail size.
			return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
		}


		public static bool Thumbnail(string fileName, string prefix, int width, string folderPth = "") {
			try {
				Image image = Image.FromFile(folderPth + fileName);
				float AspectRatio = (float)image.Size.Width / (float)image.Size.Height;
				int NewWidth = width;
				int newHeight = Convert.ToInt32(NewWidth / AspectRatio);
				Bitmap thumbnailBitmap = new Bitmap(NewWidth, newHeight);
				Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
				thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
				thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
				thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
				var imageRectangle = new Rectangle(0, 0, NewWidth, newHeight);
				thumbnailGraph.DrawImage(image, imageRectangle);
				thumbnailBitmap.Save(folderPth + prefix + fileName, ImageFormat.Jpeg);
				thumbnailGraph.Dispose();
				thumbnailBitmap.Dispose();
				image.Dispose();
				FileInfo LogoDelete = new FileInfo(folderPth + fileName);
				if (LogoDelete.Exists)
					LogoDelete.Delete();

				return true;
			} catch {
				return false;
			}

		}

	}
}