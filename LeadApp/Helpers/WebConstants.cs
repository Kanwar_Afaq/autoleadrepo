﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Helpers {
	public static class WebConstants {//SystemConstants
		public const string AppTitle = "LeadApp - Form Collection";
		public const string SystemName = "Lead Form Collections";

		public static string SystemFooter = "&copy; " + DateTime.Now.Year + " Sepia Solutions. All rights reserved.<a class=\"sepiaLogo\" target=\"_blank\" href=\"http://sepiasolutions.com\"><img title=\"Sepia Solutions\" alt=\"Sepia Solutions\" src=\"/Content/images/sepiasolutions-logo.png\"><span style=\"display: none;\">Sepia Solutions</span></a>";

		public const string TimeZone = "GMT Standard Time";

		public const int PagingSize = 20;
		public const int PagingSizeBig = 50;

        public const int CustomerUserGroupID = 3;


		// File Uploading Parameters
		public const string TempFileUploadFolder = "Content/Upload/TempFolder/";
		public const string FileFinalUploadFolder = "Content/Upload/FinalUploadFolder/";
		public const string QuotationVendorUploaded = "Content/Upload/QuotationsVendorUpload/";
        public const string ProcurmentQuotationUpload = "Content/Upload/ProcurementQuotationUpload/";
		public const string UserProfileThumbPath = "Content/Upload/User/";			 //Upload location of User Images
        public const string QuotationsUploads = "Content/Upload/QuotationsUploads/";
        public const string JvUploads = "Content/Upload/JvUploads/";
        public const string MDFPath = "Content/Upload/JobIGM/";

		
		public const int FileSize = 2048;
		public const string FileExtension = "pdf,doc,docx,xls,xlsx,txt,jpg,gif";


		//e.g: "http://localhost:56954/"
		public static string LogicalPath = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port) + "/";

		//e.g.: "D:\\MazharWorkSpace\\Alpine-App\\Alpine-Dev\\Alpine-Dev\\"
		public static string PhysicalPath = HttpRuntime.AppDomainAppPath;


		//Cache Parameters
		public const bool DataCacheEnable = true;
		public const int DataCacheTime = 2;
		public const int DataCacheMaxTime = 30;




		/// <summary>
		/// Post Display Order	[PublishDate | LIFO | FIFO | SortID]
		/// </summary>
		public const string DisplayOrder = "PublishDate";
        public const string DateTimeFormat = "d M Y h:i A";


		public static DateTime baseDate = DateTime.Today;

		public static DateTime today = baseDate;
		public static DateTime yesterday = baseDate.AddDays(-1);
		public static DateTime thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
		public static DateTime thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1);
		public static DateTime lastWeekStart = thisWeekStart.AddDays(-7);
		public static DateTime lastWeekEnd = thisWeekStart.AddSeconds(-1);
		public static DateTime thisMonthStart = baseDate.AddDays(1 - baseDate.Day);
		public static DateTime thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);
		public static DateTime lastMonthStart = thisMonthStart.AddMonths(-1);
		public static DateTime lastMonthEnd = thisMonthStart.AddSeconds(-1);

		public static DateTime thisYearStart = new DateTime(baseDate.Year, 1, 1);
		public static DateTime thisYearEnd = thisYearStart.AddYears(1).AddSeconds(-1);



		/// <summary>
		/// Excerpt Characters [Default = 250]
		/// </summary>
		public const int ExcerptCharacter = 250;



		public const string CaptchaTextType = "AlphaNumeric";	//Type of Captcha [AlphaNumeric | Alpha | Numeric]
		public const int CaptchaLength = 4;	//number of character for Captcha


		//User Registration Parameter
		public const string UserProfileExtensions = ".jpg,.gif,.png,.bmp";	// User Profile Image File Extensions
		public const int UserProfileImageFileSize = 500;	// Image File Size Allowed
		public const int UserProfileThumbSmall = 32;	// User Image Thumb Small Size
		public const int UserProfileThumbMedium = 128;	// User Image Thumb Medium Size	
		public const int UserProfileThumbLarge = 512;	// User Image Thumb Large Size

		public const int UserGroupForRegistration = 3;	// Front End User Registration Group ID in System
		public const int UserGroupdIDForAdmin = 2;      // Website Admin Group ID
		public const string UserThumbOriginalKey = "Thumbnail-Original";



		// System Paths
		public static string UserProfileThumbURL = "/Content/Upload/User/";

		public static string Default_ThumbURL = "/content/images/Default/thumb-user.jpg";
		public static string Default_DocumentThumbURL = "/content/images/Default/thumb-document.gif";
		public static string Default_UserThumbURL = Default_ThumbURL;

		public static string FavIcon = "content/images/favicon.ico";
		public static string FavIconGif = "content/images/favicon.gif";

		public static string UserProfilePage = "/user/profile/";

		

	}


}