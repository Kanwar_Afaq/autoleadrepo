﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;


namespace LeadApp.Helpers {
	public static class HelperMethods {

		public static IQueryable<TSource> OrderByNullsLast<TSource, TKey>(this IQueryable<TSource> query, Expression<Func<TSource, TKey>> keySelector) {
			return query.OrderBy(keySelector).NullsLast(keySelector);
		}


		public static IQueryable<TSource> NullsLast<TSource, TKey>(this IQueryable<TSource> query, Expression<Func<TSource, TKey>> keySelector) {
			var nullExpr = Expression.Constant(null, typeof(TKey));
			var equalExpr = Expression.Equal(keySelector.Body, nullExpr);
			var lambda = Expression.Lambda<Func<TSource, bool>>(equalExpr, keySelector.Parameters);
			return query.OrderBy(lambda);
		}




		public static string ReplaceKeyword(this string WordToReplace) {
			string AppName = "Alpine";
			WordToReplace = WordToReplace.Replace("[LANG]", AppName);

			return WordToReplace;
		}


        public static bool IsGUID(this object expression)
        {
			try {
                if (expression == null)
                    return false;

				Guid output = Guid.Parse(expression.ToString());
				return true;
			} catch {
				return false;
			}
		}


        public static bool IsNumeric(this object expression)
        {
			try {
			if(expression == null)
				return false;

				double result2 = Convert.ToDouble(expression);
				return true;
			} catch {
				return false;
			}
		}
        public static bool IsDateTime(this object expression)
        {
			try {
				DateTime result2 = Convert.ToDateTime(expression.ToString());
				return true;
			} catch {
				return false;
			}
		}
        public static bool IsBoolean(this object expression)
        {
			try {
				bool result2 = Convert.ToBoolean(expression);
				return true;
			} catch {
				return false;
			}
		}
        public static bool isUrl(this string address)
        {
			string strRegex = @"(https:[/][/]|http:[/][/]|www.)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$";
			Regex re = new Regex(strRegex);
			if (re.IsMatch(address))
				return (true);
			else
				return (false);
		}
        public static bool isEmail(this string Email)
        {
			string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
				  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
				  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
			Regex re = new Regex(strRegex, RegexOptions.Compiled);

			//Regex EmailRegEx = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled);

			if (re.IsMatch(Email))
				return (true);
			else
				return (false);
		}
        public static bool isFileExist(this string FileLocation)
        {
			try {
				if (System.IO.File.Exists(FileLocation))
					return true;
				else
					return false;
			} catch {
				return false;
			}
		}







        public static string ConvertToString(this object expression)
        {
			try {
				if (expression != "")
					return Convert.ToString(expression);
				else
					return string.Empty;
			} catch {
				return string.Empty;
			}
		}
        public static Guid ConvertToGUID(this object expression)
        {
			try {
				if (expression != null)
					return Guid.Parse(expression.ToString());
				else
					return Guid.NewGuid();
			} catch {
				return Guid.NewGuid();
			}
		}
        public static bool ConvertToBool(this object expression)
        {
			try {
				if (expression != "")
					return Convert.ToBoolean(expression);
				else
					return false;
			} catch {
				return false;
			}
		}
        public static int ConvertToInt(this object expression)
        {
			try {
				if (expression != "")
					return Convert.ToInt32(expression);
				else
					return 0;
			} catch {
				return 0;
			}
		}
        public static long ConvertToLong(this object expression)
        {
			try {
				if (expression != "")
					return Convert.ToInt64(expression);
				else
					return 0;
			} catch {
				return 0;
			}
		}
        public static decimal ConvertToDecimal(this object expression)
        {
			try {
				if (expression != "")
					return Convert.ToDecimal(expression);
				else
					return 0;
			} catch {
				return 0;
			}
		}
        public static DateTime ConvertToUTC(this DateTime theDate)
        {
			var ItemDateTime = TimeZoneInfo.ConvertTimeToUtc(theDate);

			try {
				if (!string.IsNullOrEmpty(WebConstants.TimeZone)) {
					TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(WebConstants.TimeZone);
					DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(ItemDateTime, cstZone);
					//return ("The date and time are {0} {1}.", cstTime, cstZone.IsDaylightSavingTime(cstTime) ? cstZone.DaylightName : cstZone.StandardName);
					return cstTime;
				} else {
					return ItemDateTime;
				}
			} catch (TimeZoneNotFoundException) {
				return ItemDateTime;
				//return ("The registry does not define the Central Standard Time zone.");
			} catch (InvalidTimeZoneException) {
				return ItemDateTime;
				//return ("Registry data on the Central STandard Time zone has been corrupted.");
			}
		}
        public static DateTime ConvertToDateTime(this object expression)
        {
            try
            {
                if (expression != "")
                    return Convert.ToDateTime(expression);
                else
                    return DateTime.Now;
            }
            catch
            {
                return DateTime.Now;
            }
        }

        public static DateTime ConvertToDate(this object expression)
        {
            try
            {
                if (expression != "")
                    return Convert.ToDateTime(expression).Date;
                else
                    return DateTime.Now.Date;
            }
            catch
            {
                return DateTime.Now.Date;
            }
        }


		public static DateTime MaxDate(DateTime d1, DateTime d2) {
			if (d1 > d2)
				return d1;
			else
				return d2;
		}

		public static string FormatDateTime(this DateTime dateTime) {
			if (dateTime.TimeOfDay == TimeSpan.Zero)
                return dateTime.ToString("dd MMM yyyy");      //return dateTime.ToString("d");
			else
                return dateTime.ToString("dd MMM yyyy hh:mm tt");              //return dateTime.ToString("g");
		}
		public static string FormatDateTime(this DateTime? dateTime) {
			if (dateTime.HasValue)
				return dateTime.Value.FormatDateTime();
			else
				return "";
		}


        public static DateTime FormatDateTimeForEdit(this DateTime dateTime)
        {
            if (dateTime.TimeOfDay == TimeSpan.Zero)
                return dateTime.ToString("dd MMM yyyy").ConvertToDateTime();
            else
                return dateTime.ToString("dd MMM yyyy hh:mm tt").ConvertToDateTime();
        }
        public static DateTime FormatDateTimeForEdit(this DateTime? dateTime)
        {
            if (dateTime.HasValue)
                return dateTime.Value.FormatDateTimeForEdit();
            else
                return DateTime.Now;
        }




		/// <summary>
		/// Generate Random Text
		/// </summary>
		/// <param name="textLength"></param>
		/// <param name="type">AlphaNumeric | Alpha | Numeric</param>
		/// <returns></returns>
		public static string GenerateRandomText(int textLength = 8, string type = "AlphaNumeric") {
			const string Alphabats = "ABCDEFGHIJKLMNPOQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
			const string Numbers = "0123456789";
			string Chars = Alphabats + Numbers;
			if (type.ToLower() == "numeric")
				Chars = Numbers;
			else if (type.ToLower() == "alpha")
				Chars = Alphabats;

			var random = new Random();
			var result = new string(Enumerable.Repeat(Chars, textLength).Select(s => s[random.Next(s.Length)]).ToArray());
			return result;
		}

		//Function to get random number
		public static int GetRandomNumber(int min = 5, int max = 8) {
			Random random = new Random();
			return random.Next(min, max);
		}












		public static string UppercaseFirstLetter(string str) {
			var firstChar = str[0];
			var UpperCaseFirstCharacter = char.ToUpper(firstChar);
			var convertedFirstCharToUpper = UpperCaseFirstCharacter + str.Substring(1);
			return convertedFirstCharToUpper;
		}





		public static string CleanUserName(string username) {
			username.Trim().ToLower();
			username.Replace(" ", string.Empty);
			username = Regex.Replace(username, @"\s+", "");
			string[] outUser = username.Split('@');

			if (outUser[0].Length > 8)
				return outUser[0].Substring(0, 8);
			else
				return outUser[0];
		}



		public static string CleanHtml(string HtmlText, int BreakString = 0, bool InclDots = true) {
			if (String.IsNullOrEmpty(HtmlText))
				return string.Empty;

			HtmlText = HtmlText.Trim();

			HtmlText = HtmlText.Replace("<br>", "\n");
			HtmlText = HtmlText.Replace("<br >", "\n");
			HtmlText = HtmlText.Replace("<br />", "\n");
			HtmlText = HtmlText.Replace("&nbsp;&nbsp;", "\t");
			HtmlText = HtmlText.Replace("&nbsp;&nbsp;", "  ");
			HtmlText = HtmlText.Replace("&nbsp;", " ");
			//HtmlText = HtmlText.Replace("& ", "&amp; ");

			HtmlText = Regex.Replace(HtmlText, "<.*?>", string.Empty);

			if (BreakString != 0)
				HtmlText = BreakWord(HtmlText, BreakString, InclDots);

			//HtmlText = HttpUtility.HtmlEncode(HtmlText);
			return HtmlText.Trim();
		}

		public static string ReplaceHtmlChar(this string HtmlText) {
			if (String.IsNullOrEmpty(HtmlText))
				return string.Empty;

			HtmlText = HtmlText.Trim();
			HtmlText = HtmlText.Replace("& ", "&amp; ");
			//HtmlText = HttpUtility.HtmlEncode(HtmlText);

			return HtmlText;
		}


		public static string BreakWord(string InputTxt, int ChartoDisplay, bool InclDots = true) {
			if (ChartoDisplay > 0) {
				if (InputTxt.Length > ChartoDisplay) {
					var words = InputTxt.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
					var sbText = new StringBuilder();

					foreach (var word in words) {
						if ((sbText.ToString().Length + word.Length) <= ChartoDisplay) {
							sbText.Append(word + " ");
						} else {
							break;
						}
					}
					InputTxt = sbText.ToString();

					if (InclDots)
						InputTxt = InputTxt.TrimEnd(' ') + "...";
				}
			}
			return InputTxt;
		}

		public static string ShortLength(this string InputTxt, int ChartoDisplay = 255, bool InclDots = true) {
			return BreakWord(InputTxt, ChartoDisplay, InclDots);
		}




	}

}