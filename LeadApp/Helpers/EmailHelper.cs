﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace LeadApp.Helpers
{
	public class EmailHelper
	{

		public static bool SystemEmail(string EmailTemplate, ListDictionary ReplaceData, string TO, string Subject, string Body = "", string realFROM = "", string CCAdd = "", string BCCAdd = "")
		{
			string error = string.Empty;

			try
			{		
				MailDefinition EMailTemplate = new MailDefinition
				{
					BodyFileName = "~/content/Email-Templates/" + EmailTemplate,
					From = WebConfig.FromEmailID,
					Subject = Subject
				};

				MailMessage mailmsg = EMailTemplate.CreateMailMessage(TO, ReplaceData, new LiteralControl());

				return SendEmail(out error, "", TO, CCAdd, BCCAdd, "", Subject, mailmsg.Body, true, realFROM);
			}
			catch
			{
				return false;
			}
		}




		public static bool SendEmail(out string error, string From, string TO, string CC, string BCC, string Attachments, string Subject, string Body = "", bool isHtml = true, string realFROM = "")
		{
			error = string.Empty;
			List<string> TOList = new List<string>();
			List<string> CCList = new List<string>();
			List<string> BCCList = new List<string>();
			List<string> AttachmentList = new List<string>();
			
			TOList.Add(TO);
			CCList.Add(CC);
			BCCList.Add(BCC);
			AttachmentList.Add(Attachments);

			return SendEmail(out error, From, TOList, CCList, BCCList, AttachmentList, Subject, Body, isHtml, realFROM);
		}




		public static bool SendEmail(out string error, string From, List<string> TO, List<string> CC = null, List<string> BCC = null, List<string> Attachments = null, string Subject = "", string Body = "", bool isHtml = true, string realFROM = "")
		{
			error = string.Empty;
			try
			{
				MailMessage mailmsg = new MailMessage();

				//Get SystemConfig Variables
				string DefaultSMTPHost = WebConfig.SMTPHost;
				string DefaultSMTPPort = WebConfig.SMTPPort;
				string DefaultSMTPUser = WebConfig.SMTPUsername;
				string DefaultSMTPPassword = WebConfig.SMTPPassword;
				string DefaultEmailFromName = WebConfig.EmailFromName;
				string DefaultFromEmailID = WebConfig.FromEmailID;
				string DefaultBCCAddress = WebConfig.BCCAddress;
				string DefaultEmailSignature = WebConfig.EmailSignature;
				int SMTPPort = HelperMethods.ConvertToInt(DefaultSMTPPort);
				

				MailAddress DefaultFrom = new MailAddress(DefaultFromEmailID, DefaultEmailFromName);
				MailAddress FromAddress = (string.IsNullOrEmpty(From) && !From.isEmail()) ? DefaultFrom : new MailAddress(From);

				string TestModeData = string.Empty;

				
				if (TO.Count > 0)
				{
					TestModeData += "<br>TO: ";
					foreach (var t in TO)
					{
						if (!string.IsNullOrEmpty(t))
						{
							TestModeData += t + ", ";
							mailmsg.To.Add(t);
						}
					}
				}
				if (CC != null)
				{
					if (CC.Count > 0)
					{
						TestModeData += "<br>CC: ";
						foreach (var t in CC)
						{
							if (!string.IsNullOrEmpty(t))
							{
								TestModeData += t + ", ";
								mailmsg.CC.Add(t);
							}
						}
					}
				}

				if (BCC != null)
				{
					if (BCC.Count > 0)
					{
						TestModeData += "<br>BCC: ";
						foreach (var t in BCC)
						{
							if (!string.IsNullOrEmpty(t))
							{
								TestModeData += t + ", ";
								mailmsg.Bcc.Add(t);
							}
						}
					}
				}

				if (Attachments != null)
				{
					if (Attachments.Count > 0)
					{
						foreach (var t in Attachments)
						{
							if (!string.IsNullOrEmpty(t))
							{
								string FilePath = HttpContext.Current.Server.MapPath(t);
								Attachment attachment = new Attachment(FilePath, MediaTypeNames.Application.Octet);
								ContentDisposition disposition = attachment.ContentDisposition;
								disposition.CreationDate = File.GetCreationTime(FilePath);
								disposition.ModificationDate = File.GetLastWriteTime(FilePath);
								disposition.ReadDate = File.GetLastAccessTime(FilePath);
								disposition.FileName = Path.GetFileName(FilePath);
								disposition.Size = new FileInfo(FilePath).Length;
								disposition.DispositionType = DispositionTypeNames.Attachment;

								mailmsg.Attachments.Add(attachment);
							}
						}
					}
				}



				if (WebConfig.EmailTestmodeEnable)
				{					
					mailmsg.To.Clear();
					mailmsg.CC.Clear();
					mailmsg.Bcc.Clear();

					string testMsg = "<font colo=\"#ff0000\">!!~-~-~-~-~-~-~-~-~-~-~- start test header ~-~-~-~-~-~-~-~-~-~-~-!!<br><br><b>Test Mode Data</b></font><br> Original Recipients are listed as follows:<br>";
					testMsg +=  "<br>From: " + FromAddress.Address.ToString();
					testMsg += TestModeData;
					testMsg += "<br><br><font colo=\"#ff0000\">!!~-~-~-~-~-~-~-~-~-~-~- end test header ~-~-~-~-~-~-~-~-~-~-~-!!</font><br><br><br>";
					testMsg += Body;


					Body = testMsg;
					FromAddress = DefaultFrom;
					mailmsg.To.Add(WebConfig.TestModeEmail);
					if(!string.IsNullOrEmpty(From))
						mailmsg.To.Add(From);
				}


				mailmsg.Body = Body;
				mailmsg.From = FromAddress;
				mailmsg.Subject = Subject;
				mailmsg.IsBodyHtml = isHtml;

				if (DefaultBCCAddress != "")
					mailmsg.Bcc.Add(DefaultBCCAddress);

				if (!string.IsNullOrEmpty(realFROM))
					mailmsg.ReplyToList.Add(realFROM);

				



				mailmsg.BodyEncoding = System.Text.Encoding.UTF8;
				mailmsg.SubjectEncoding = System.Text.Encoding.UTF8;

				SmtpClient smtpClient = new SmtpClient(DefaultSMTPHost, SMTPPort);
				System.Net.NetworkCredential credential = new System.Net.NetworkCredential(DefaultSMTPUser, DefaultSMTPPassword);
				smtpClient.Credentials = credential;
				smtpClient.Send(mailmsg);

				return true;
			}
			catch (Exception e)
			{
				error = e.Message;
				return (false);
			}
		}

	}
}