﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Drawing.Text;

namespace LeadApp.Helpers {
	public class CaptchaViewModel {
		public string Text {
			get;
			set;
		}
		public byte[] Image {
			get;
			set;
		}
		public string ContentType {
			get;
			set;
		}
	}

	public class ImageCaptcha {
		public CaptchaViewModel GetCaptcha(bool noisy = true) {
			CaptchaViewModel output = new CaptchaViewModel();
			string CaptchaText = HelperMethods.GenerateRandomText(WebConfig.CaptchaLength, WebConfig.CaptchaTextType);
			var oRandom = new Random((int)DateTime.Now.Ticks);


			int ImgWidth = (WebConfig.CaptchaLength * 20) > 80 ? (WebConfig.CaptchaLength * 20) : 80;
			int ImgHeight = (WebConfig.CaptchaLength * 6) > 30 ? (WebConfig.CaptchaLength * 6) : 30;

			var FontFamily = "Tahoma";
			Single FontSize = ImgHeight / 2;
			Font FontName = new Font(FontFamily, FontSize);

			var strFormat = new StringFormat();
			strFormat.Alignment = StringAlignment.Center;
			strFormat.LineAlignment = StringAlignment.Center;


			int[] aBackgroundNoiseColor = new int[] { 150, 150, 150 };
			int[] aTextColor = new int[] { 0, 0, 0 };
			int[] aFontEmSizes = new int[] { 18, 20, 22 };


			string[] aFontNames = new string[] { "Comic Sans MS", "Arial", "Times New Roman", "Verdana", "Geneva" };
			FontStyle[] aFontStyles = new FontStyle[] { FontStyle.Bold, FontStyle.Italic, FontStyle.Regular, FontStyle.Strikeout };
			HatchStyle[] aHatchStyles = new HatchStyle[]
            {HatchStyle.BackwardDiagonal, HatchStyle.Cross, HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal,
             HatchStyle.DashedUpwardDiagonal, HatchStyle.DashedVertical, HatchStyle.DiagonalCross,HatchStyle.DottedDiamond, HatchStyle.DottedGrid, 
	            HatchStyle.ForwardDiagonal, HatchStyle.Horizontal, HatchStyle.LargeCheckerBoard, 
	            HatchStyle.LargeGrid, HatchStyle.LightDownwardDiagonal, HatchStyle.LightHorizontal, 
	            HatchStyle.LightUpwardDiagonal, HatchStyle.LightVertical,
             HatchStyle.Max, HatchStyle.Min, HatchStyle.NarrowHorizontal, 
	            HatchStyle.NarrowVertical, HatchStyle.Sphere, HatchStyle.Vertical, HatchStyle.Wave, HatchStyle.Weave,
             HatchStyle.WideDownwardDiagonal, HatchStyle.WideUpwardDiagonal, HatchStyle.ZigZag
            };








			//Creates an output Bitmap
			using (var oBMP = new Bitmap(ImgWidth, ImgHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))




			using (var gfx = Graphics.FromImage((Image)oBMP)) {
				gfx.TextRenderingHint = TextRenderingHint.AntiAlias;
				gfx.SmoothingMode = SmoothingMode.AntiAlias;


				//Create a Drawing area
				RectangleF oRectangleF = new RectangleF(0, 0, oBMP.Width, oBMP.Height);
				Brush oBrush = default(Brush);

				//Draw background (Lighter colors RGB 100 to 255)
				oBrush = new HatchBrush(aHatchStyles[oRandom.Next(aHatchStyles.Length - 1)], Color.FromArgb((oRandom.Next(180, 255)), (oRandom.Next(180, 255)), (oRandom.Next(180, 255))), Color.White);
				gfx.FillRectangle(oBrush, oRectangleF);


				//HatchBrush bgFill = new HatchBrush(HatchStyle.Wave, System.Drawing.Color.LightGray, System.Drawing.Color.White);
				//gfx.FillRectangle(bgFill, oRectangleF);





				//add noise in the image
				if (noisy) {
					int a1, r, x, y;
					var pen = new Pen(Color.Yellow);
					for (a1 = 1; a1 < HelperMethods.GetRandomNumber(1, WebConfig.CaptchaLength); a1++) {
						pen.Color = Color.FromArgb(
						(oRandom.Next(0, 255)),
						(oRandom.Next(0, 255)),
						(oRandom.Next(0, 255)));

						r = oRandom.Next(0, (oBMP.Width / 3));
						x = oRandom.Next(0, oBMP.Width);
						y = oRandom.Next(0, oBMP.Height);

						gfx.DrawEllipse(pen, x - r, y - r, r, r);


						// calculate line start and end point here using the Random class:
						int x0 = oRandom.Next(0, oBMP.Width);
						int y0 = oRandom.Next(0, oBMP.Height);
						int x1 = oRandom.Next(0, oBMP.Width);
						int y1 = oRandom.Next(0, oBMP.Height);

						gfx.DrawLine(pen, x0, y0, x1, x1);
					}
				}




				//Add Text in Image with Random Fonts
				Matrix oMatrix = new Matrix();
				int i = 0;
				for (i = 0; i <= CaptchaText.Length - 1; i++) {
					oMatrix.Reset();
					int iChars = CaptchaText.Length;
					int x = ImgWidth / (iChars + 1) * i;
					int y = ImgHeight / 2;

					//Rotate text Random
					oMatrix.RotateAt(oRandom.Next(-20, 20), new PointF(x, y));
					//gfx.Transform = oMatrix;

					//Draw the letters with Random Font Type, Size and Color
					gfx.DrawString
					(CaptchaText.Substring(i, 1),
						//Random Font Name and Style
					new Font(aFontNames[oRandom.Next(aFontNames.Length - 1)],
					   aFontEmSizes[oRandom.Next(aFontEmSizes.Length - 1)],
					   aFontStyles[oRandom.Next(aFontStyles.Length - 1)]),
						//Random Color (Darker colors RGB 0 to 100)
					new SolidBrush(Color.FromArgb(oRandom.Next(0, 100),
					   oRandom.Next(0, 100), oRandom.Next(0, 100))),
					x, ImgHeight / 6);

					//gfx.ResetTransform();
				}


				//add text
				//gfx.DrawString(CaptchaText, FontName, Brushes.Black, oRectangleF, strFormat);




				//render as Jpeg
				MemoryStream oMemoryStream = new MemoryStream();
				oBMP.Save(oMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);





				output.Image = oMemoryStream.GetBuffer();
				output.ContentType = "image/Jpeg";
			}

			//Save the Captcha Hash in the Session
			//output.Text = EncryptDecryptHelper.EncryptCaptha(CaptchaText);//Encrypted Output
			output.Text = CaptchaText;

			return output;
		}
	}
}