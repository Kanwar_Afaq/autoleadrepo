﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using LeadApp.Helpers;
using LeadApp.Services;
using System.Collections.Specialized;

namespace LeadApp
{
	public class NotRestrictedAttribute : FilterAttribute
	{
		// Does nothing, just used for decoration
	}

	public class RestrictedAttribute: ActionFilterAttribute, IActionFilter {
		[ValidateInput(false)]
		void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
		{

			string RefreshSysMeta = WebConfig.GetSysConfig().RefreshCache();


			//Add Cookie value to use in User Login and other places
			WebConfig.setWebsiteCookie();



			// Check if this action has NotRestricted....this will skip specific method for restricting
			object[] attributes = filterContext.ActionDescriptor.GetCustomAttributes(true);
			if (attributes.Any(a => a is NotRestrictedAttribute)) return;



			//Verify User Login Cookkie Value is valid & Re-activate user session
			UserService _UserService = new UserService();
			_UserService.CreateSessionByCookie();


			if (!Auth.isLogin)
			{
				var values = new RouteValueDictionary(new {action = "Login",controller = "User"});
				values.Add("url", WebConfig.EncryptedAbsoluteURL);
				filterContext.Result = new RedirectToRouteResult(values);
				base.OnActionExecuting(filterContext);
			}
			else
			{
				bool isSkipAuth = false;
				string param = string.Empty;

				var req = filterContext.HttpContext.Request;

				var descriptor = filterContext.ActionDescriptor;
				var actionName = descriptor.ActionName;
				var controllerName = descriptor.ControllerDescriptor.ControllerName;

				var type = filterContext.Controller.GetType();
				//var method = type.GetMethod(actionName);
				//var returnType = method.ReturnType;

				



				if (req.AcceptTypes.Contains("text/html")) { }					
				else if (req.AcceptTypes.Contains("application/json"))
					isSkipAuth = true;
				else if (req.AcceptTypes.Contains("application/xml") || req.AcceptTypes.Contains("text/xml"))
					isSkipAuth = true;

				
				if (req.IsAjaxRequest())
					isSkipAuth = true;


				if (actionName == "Restricted" && controllerName == "Home")
					isSkipAuth = true;

				//Allow all Partial Request to pass through Authentication
				if (actionName.StartsWith("_"))
					isSkipAuth = true;


				//Enable Full Access to SuperAdmin Group
				if (Auth.GroupID == 1)
					isSkipAuth = true;





				if (isSkipAuth)
					return;
				else
				{

					///Get List of All QueryString to Append in the URL
					var queryStringParams = new NameValueCollection(req.QueryString);					

					foreach (string x in queryStringParams)
					{
						if (!string.IsNullOrEmpty(param))
							param = "&" + param;
						param = x + "=" + queryStringParams[x];
					}
					if (!string.IsNullOrEmpty(param))
						param = "?" + param.ToLower();


					SecurityService _serv = new SecurityService();
					var getAccessDetail = _serv.VerifyUserAccess(controllerName);
					var AccessRights = _serv.VerifyPermission(getAccessDetail);
					bool isFullAccess = AccessRights.Where(e => e.Item1.ToLower() == "fullaccess" && e.Item2.ToLower() == "true").Any() ? true : false;
					bool isAllow = AccessRights.Where(e => e.Item1.ToLower() == actionName.ToLower() + param && e.Item2.ToLower() == "true").Any() ? true : false;

					if (isFullAccess)
						isAllow = true;
					
					if (!isAllow)
					{
						var values = new RouteValueDictionary(new { action = "Restricted", controller = "Home" });
						values.Add("url", controllerName + "/" + actionName + param);

						filterContext.Result = new RedirectToRouteResult(values);
						base.OnActionExecuting(filterContext);
					}
				}
			}

		}

	}

}