(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

require('./jquery');

require('./menu');

var _utilities = require('./utilities');

var _utilities2 = _interopRequireDefault(_utilities);

require('./jquery.plugins');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

// Calling Global jquery

// Menu stuf
// import './babel-polyfill';
// import './core-js-ie8';
//

jQuery(document).ready(function ($) {

	/*
  @todo , Implement own scroll to element with help of even mouse wheel
 mouse scroll even on section class
 $(window).on('wheel', function(e) {
 	var delta = e.originalEvent.deltaY;
 
 	if (delta > 0) $('body').text('down');
 	else $('body').text('up');
 
 	return false; // this line is only added so the whole page won't scroll in the demo
 });
 */

	// Implemented Full page
	if ($('.full-page').length) {

		var $widget_fullpage = $('.widget-home, .full-page'),
		    scrollMode = true,
		    _elements = "";

		/*
    	// Apply Class of last to last element
  // Need to turn off autoscrolling if Element is last in viewport.
  // $.fn.fullpage.setAutoScrolling(boolean); controls autoScrolling Property on runtime
  */
		$('.section:last', $widget_fullpage).addClass('last-section');

		if ($('.phone-menu-container').is(':visible')) {
			scrollMode = false;
			$('.section').addClass('fp-auto-height');
			_elements = ".site-content";
		}

		$widget_fullpage.fullpage({

			//verticalCentered: true,
			responsiveHeight: true,
			scrollBar: true,
			autoScrolling: scrollMode,

			// to avoid problems with css3 transforms and fixed elements in Chrome,
			// as detailed here: https://github.com/alvarotrigo/fullPage.js/issues/208
			css3: true,
			scrollingSpeed: 1200,
			normalScrollElements: _elements,
			scrollOverflow: true,

			// This is being so pain in beep
			fitToSection: false,

			// // Mouse Leave
			// afterLoad : function (anchor, index ) {
			//     if (index == 1) {
			//         $('.animate', this).addClass('animated');
			//         $('.menu-wrapper').removeClass('sticky')
			//     } else {
			//         $('.menu-wrapper').addClass('sticky')
			//     }
			//
			//
			// },
			onLeave: function onLeave(index, nextIndex, direction) {

				console.log(index, nextIndex, direction);

				// index = current div
				var reverse = false;
				var $_currentDiv = $('.section:eq(' + (index - 1) + ')', $widget_fullpage);
				if ($_currentDiv.hasClass('rev')) {
					reverse = true;
				}

				var n = nextIndex - 1;
				// var $_this = $(this).parent().find('.section:eq(' + index + ')');

				// check
				var _target_item = $('.section:eq(' + n + ')', $widget_fullpage);

				/**
     *
     * If current section is last section. and direction is down then,
     * turn off auto scrolling.
     *
     **/
				// if ( scrollMode ) {
				// 	if ( $('.fp-section.last-section').hasClass('current') && direction == 'up' ) {
				// 		 $.fn.fullpage.setAutoScrolling( true );
				// 	} else {
				// 		// utils.scroll_to_element( _target_item, 1500, function () {
				// 		$.fn.fullpage.setAutoScrolling(false);
				// 		// } );
				//
				// 	}
				// }


				// console.log( _target_item );

				//@todo Need to work on reverse
				// if( reverse ) {
				// 	utils.animate( $_currentDiv , '.do-animate', reverse);
				// }
				_utilities2['default'].animate(_target_item, '.do-animate');
			}
		});
	}

	console.log('..:: Welcome ::..');

	// Animation for Archive Pages
	if ($('body').hasClass('archive')) {

		$(window).scroll(function () {

			_utilities2['default'].scroll_event_trigger(function () {

				$('li.product').each(function (i, v) {

					if ($(this).isInViewgport(100)) {
						$(this).wait(1500 * Math.floor(i / 2), function () {
							$(this).addClass('testClass');
						});
					}
				});
			}(), 250);
		});
	}

	// Check object to be animated first,
	// so custom speed can be applied
	// instead of default
	if ($('.do-animate[data-speed]').length) {
		$('.do-animate[data-speed]').each(function () {
			var $_this = $(this);
			$_this.css({
				'-webkit-transition-duration': _utilities2['default'].getSpeed($_this) + "s!Important;",
				'-moz-transition-duration': _utilities2['default'].getSpeed($_this) + "s!Important;",
				'transition-duration': _utilities2['default'].getSpeed($_this) + "s!Important;",
				'-webkit-animation-duration': _utilities2['default'].getSpeed($_this) + "s!Important;",
				'-moz-animation-duration': _utilities2['default'].getSpeed($_this) + "s!Important;",
				'animation-duration': _utilities2['default'].getSpeed($_this) + "s!Important;"
			});
		});
	}

	// Slick Slider---
	// Apply Class on widget currently.
	if ($('.slick-container').length) {

		$('.slick-container .ss-banner-cont > ul').each(function (i, v) {

			// If more than 1 Li are here.
			// More than 1 means need to slide
			if ($('li', $(this)).length > 1) {
				$(this).slick({
					speed: 600
					// adaptiveHeight: true
				});
			}
		});
	};
}); // jQuery scope


},{"./jquery":2,"./jquery.plugins":3,"./menu":4,"./utilities":5}],2:[function(require,module,exports){
'use strict';

/**
 * Created by xafar
 */
$ = window.jQuery;
// Cookies = window.Cookies();
if (xash_utility.xashDebug) console.log('jquery.js Loaded');


},{}],3:[function(require,module,exports){
'use strict';

/**
 * Created by xafaR.
 * Copyright XASHLabs
 * Set of Custom Plugins or maybe copy/paste vendor plugins here as well...
 */

// add wait as $.wait() standalone and $(elem).wait() for animation chaining
(function ($) {
	if (xash_utility.xashDebug) console.log('jQuery.plugins Loaded');

	// Jquery Wait plugin, better than setTimeout native.
	$.wait = function (duration, completeCallback, target) {
		var $target = $(target || '<queue />');
		return $target.delay(duration).queue(function (next) {
			completeCallback.call($target);
			next();
		});
	};

	$.fn.wait = function (duration, completeCallback) {
		return $.wait.call(this, duration, completeCallback, this);
	};

	// Checks & returns item in viewport.
	$.fn.isInViewport = function () {
		var offset = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

		var elementTop = $(this).offset().top;
		var elementBottom = elementTop + $(this).outerHeight();

		var viewportTop = $(window).scrollTop() + offset;
		var viewportBottom = viewportTop + $(window).height();

		return elementBottom > viewportTop && elementTop < viewportBottom;
	};
})(jQuery);


},{}],4:[function(require,module,exports){
'use strict';

require('./jquery');

var _utilities = require('./utilities');

var _utilities2 = _interopRequireDefault(_utilities);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

/**
 * Created by xafaR.
 * Copyright XASHLabs
 * All Menu related functions
 */

var _target_menu = '.navbar-toggle',
    _menu_box = '.menu-curtain';

jQuery(document).ready(function ($) {

	if (xash_utility.xashDebug) console.log('Menu.js Loaded');

	var is_home = $('body').hasClass('home');

	/*  Search Button
  //--------------------------------------------------*/

	$('.super-menu li.search-btn').on('click', function (e) {
		e.preventDefault();

		// alert('Mini Cart clicked.')
		$('#header-search').modal('show');
	});

	// Get element height with its offset position
	// so actuall location is discovered
	function get_menu_btn_pos(elem) {
		return $(elem).offset().top + $(elem).height();
	}

	// Drop Downs for Bootstrap
	//---------------------------------------------------------------

	/*
 $('.___dropdown').on('show.bs.dropdown', function() {
 	$(this).find('.dropdown-menu').first().stop(true, true).slideDown();
 });
 
 // Add slideUp animation to Bootstrap dropdown when collapsing.
 $('.___dropdown').on('hide.bs.dropdown', function() {
 	$(this).find('.dropdown-menu').first().stop(true, true).slideUp();
 });
 */

	$('.main-navigation .dropdown').hover(function () {
		$(this).find('.dropdown-menu').first().stop(true, true).addClass('show-menu');
	}, function () {
		$(this).find('.dropdown-menu').first().stop(true, true).removeClass('show-menu');
	});

	//----------------------------------------------------------------------------------*/
	//  DROPDOWN STUFF             -----------------------------------------------------*/
	//----------------------------------------------------------------------------------*/

	$('.dropdown-toggle').each(function (i, v) {
		$(this).attr('data-toggle', 'dropdown');
	});
});


},{"./jquery":2,"./utilities":5}],5:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _utils; /**
             * Created by xafaR
             * Copyright XASHLabs
             * Utility Module is based on static functions
             */

require('./jquery');

if (xash_utility.xashDebug) console.log('Utitlies.js Loaded');
var utils = (_utils = {

	// Using object define globally, xash_utility via localized_script
	themeURL: xash_utility.themeUri,

	self: undefined,

	// Load CSS Element, instead of queue
	loadCss: function loadCss(path, location) {
		var link = document.createElement("link");
		location = location || "head";
		link.type = "text/css";
		link.rel = "stylesheet";
		link.href = path;
		$(link).appendTo($(location));
	},

	// Console Logs in debug mode
	debugPrint: function debugPrint() {
		var $string = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

		console.log($string);
	},

	menuIsVisible: function menuIsVisible() {
		var $menu = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

		return $($menu).is(":visible");
	},

	getSpeed: function getSpeed($ele) {
		return parseInt(this.getAttr($ele, 'speed'));
	},

	getDelay: function getDelay($ele) {
		return parseInt(this.getAttr($ele, 'delay'));
	},

	getAnimation: function getAnimation($ele) {
		return $.trim(this.getAttr($ele, 'anim'));
	}

}, _utils['getSpeed'] = function getSpeed($ele) {
	return $.trim(this.getAttr($ele, 'speed'));
}, _utils.getAttr = function getAttr($ele, attr) {
	return $ele.data(attr);
}, _utils.animate = function animate($parent_element) {
	var $target_element = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '.do-animate';
	var $reverse = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;


	//	Need to add class who got delay paramer late.
	//	Check Delay first.
	// var cur_action = action;

	// repeation of animation
	// $('.current').find('.repeat').each(function () {
	// 	$(this).addClass('animate');
	// });

	$('.current').removeClass('current');
	$parent_element.addClass('current');

	console.log(this);

	$($parent_element).find($target_element).each(function (k, v) {

		var _this = $(this);

		var _delay = utils.getDelay(_this); // this. scope issue, so refering parent
		var _anim = utils.getAnimation(_this);

		// Pull Back all animation if reverse mode is on.
		// @note need to reverse delay as well, for better effect
		if ($reverse) {

			if (_delay) {

				// timer
				setTimeout(function () {
					_this.removeClass(_anim).delay(200);
					if (_this.hasClass('_invis')) {
						_this.addClass('invisible').removeClass('_invis');
					}
				}, _delay);

				// exit.
				return false;
			}
		}

		// Normal Animation Procedure
		if (_delay) {

			// timer
			setTimeout(function () {
				_this.addClass(_anim).delay(200);
				if (_this.hasClass('invisible')) {
					_this.addClass('_invis').removeClass('invisible');
				}
			}, _delay);
		} else {
			_this.removeClass(_anim);
		}
	});
}, _utils.scroll_event_trigger = function scroll_event_trigger(fn, timeout) {
	var waiting = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;


	// making it local
	var _waiting = waiting;

	if (_waiting) {
		return;
	} else {

		var _timer = setTimeout(function () {

			fn;
			_waiting = true;
		}, timeout);
	}
}, _utils.scroll_to_element = function scroll_to_element(elem) {
	var speed = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1000;
	var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	var easing = arguments[3];

	$('html, body').animate({
		scrollTop: $(elem).offset().top
	}, {
		duration: speed,
		complete: function complete() {
			if ($.isFunction(callback)) {
				callback();
			}
		}
	});
}, _utils.sticky_test_add = function sticky_test_add(className, limit) {
	var $_body = $('body');

	if ($(window).scrollTop() > limit) {
		$_body.addClass(className);
	} else {
		$_body.removeClass(className);
	}
}, _utils.get_cart_item_number = function get_cart_item_number(elem) {
	return parseInt($(elem).text().split(' ')[0]);
}, _utils.generateModal = function generateModal(placementId, heading, formContent) {
	var timer = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
	var strSubmitFunc = arguments[4];
	var btnText = arguments[5];


	strSubmitFunc = strSubmitFunc || null;
	heading = heading || null;

	var html = '<div id="modalWindow" class="modal fade " style="display:none;" tabindex="-1" role="dialog">';
	html += "<div class='modal-dialog'><div class='modal-content'>";
	html += '<div class="modal-header">';
	html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';

	if (heading != null) {
		html += '<h3>' + heading + '</h3>';
	}
	html += '</div>';

	//html += '<p>';
	html += '<div class="modal-body">';
	html += formContent;
	html += '</div>';

	if (btnText != '' && strSubmitFunc != null) {
		html += '<div class="modal-footer">';
		html += '<span class="btn btn-success"';
		html += ' onClick="' + strSubmitFunc + '">' + btnText;
		html += '</span>';
		html += '<span class="btn" data-dismiss="modal">';
		html += 'Close';
		html += '</span>'; // close button
		html += '</div>'; // footer
	}
	html += '</div></div>'; // modalContent
	html += '</div>'; // modalWindow

	$(html).appendTo(placementId);
	// $( placementId ).html(html);
	$("#modalWindow").modal('show');

	var _timer_dialog = null;

	// if Timer is On
	if (timer) {
		_timer_dialog = setTimeout(function () {

			$('#modalWindow').modal('hide');
		}, timer);
	}

	$('#modalWindow').on('hidden.bs.modal', function () {
		$(this).remove();
		clearTimeout(_timer_dialog);
	});
}, _utils); // utils


// Exports Module
exports['default'] = utils;
// });


},{"./jquery":2}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhc3NldHMvanMvRVM2L2NvbXBpbGVkL2FwcC5qcyIsImFzc2V0cy9qcy9FUzYvY29tcGlsZWQvanF1ZXJ5LmpzIiwiYXNzZXRzL2pzL0VTNi9jb21waWxlZC9qcXVlcnkucGx1Z2lucy5qcyIsImFzc2V0cy9qcy9FUzYvY29tcGlsZWQvbWVudS5qcyIsImFzc2V0cy9qcy9FUzYvY29tcGlsZWQvdXRpbGl0aWVzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUxBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3ZDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIndXNlIHN0cmljdCc7XG5cbnJlcXVpcmUoJy4vanF1ZXJ5Jyk7XG5cbnJlcXVpcmUoJy4vbWVudScpO1xuXG52YXIgX3V0aWxpdGllcyA9IHJlcXVpcmUoJy4vdXRpbGl0aWVzJyk7XG5cbnZhciBfdXRpbGl0aWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3V0aWxpdGllcyk7XG5cbnJlcXVpcmUoJy4vanF1ZXJ5LnBsdWdpbnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgJ2RlZmF1bHQnOiBvYmogfTsgfVxuXG4vLyBDYWxsaW5nIEdsb2JhbCBqcXVlcnlcblxuLy8gTWVudSBzdHVmXG4vLyBpbXBvcnQgJy4vYmFiZWwtcG9seWZpbGwnO1xuLy8gaW1wb3J0ICcuL2NvcmUtanMtaWU4Jztcbi8vXG5cbmpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCQpIHtcblxuXHQvKlxyXG4gIEB0b2RvICwgSW1wbGVtZW50IG93biBzY3JvbGwgdG8gZWxlbWVudCB3aXRoIGhlbHAgb2YgZXZlbiBtb3VzZSB3aGVlbFxyXG4gbW91c2Ugc2Nyb2xsIGV2ZW4gb24gc2VjdGlvbiBjbGFzc1xyXG4gJCh3aW5kb3cpLm9uKCd3aGVlbCcsIGZ1bmN0aW9uKGUpIHtcclxuIFx0dmFyIGRlbHRhID0gZS5vcmlnaW5hbEV2ZW50LmRlbHRhWTtcclxuIFxuIFx0aWYgKGRlbHRhID4gMCkgJCgnYm9keScpLnRleHQoJ2Rvd24nKTtcclxuIFx0ZWxzZSAkKCdib2R5JykudGV4dCgndXAnKTtcclxuIFxuIFx0cmV0dXJuIGZhbHNlOyAvLyB0aGlzIGxpbmUgaXMgb25seSBhZGRlZCBzbyB0aGUgd2hvbGUgcGFnZSB3b24ndCBzY3JvbGwgaW4gdGhlIGRlbW9cclxuIH0pO1xyXG4gKi9cblxuXHQvLyBJbXBsZW1lbnRlZCBGdWxsIHBhZ2Vcblx0aWYgKCQoJy5mdWxsLXBhZ2UnKS5sZW5ndGgpIHtcblxuXHRcdHZhciAkd2lkZ2V0X2Z1bGxwYWdlID0gJCgnLndpZGdldC1ob21lLCAuZnVsbC1wYWdlJyksXG5cdFx0ICAgIHNjcm9sbE1vZGUgPSB0cnVlLFxuXHRcdCAgICBfZWxlbWVudHMgPSBcIlwiO1xuXG5cdFx0LypcclxuICAgIFx0Ly8gQXBwbHkgQ2xhc3Mgb2YgbGFzdCB0byBsYXN0IGVsZW1lbnRcclxuICAvLyBOZWVkIHRvIHR1cm4gb2ZmIGF1dG9zY3JvbGxpbmcgaWYgRWxlbWVudCBpcyBsYXN0IGluIHZpZXdwb3J0LlxyXG4gIC8vICQuZm4uZnVsbHBhZ2Uuc2V0QXV0b1Njcm9sbGluZyhib29sZWFuKTsgY29udHJvbHMgYXV0b1Njcm9sbGluZyBQcm9wZXJ0eSBvbiBydW50aW1lXHJcbiAgKi9cblx0XHQkKCcuc2VjdGlvbjpsYXN0JywgJHdpZGdldF9mdWxscGFnZSkuYWRkQ2xhc3MoJ2xhc3Qtc2VjdGlvbicpO1xuXG5cdFx0aWYgKCQoJy5waG9uZS1tZW51LWNvbnRhaW5lcicpLmlzKCc6dmlzaWJsZScpKSB7XG5cdFx0XHRzY3JvbGxNb2RlID0gZmFsc2U7XG5cdFx0XHQkKCcuc2VjdGlvbicpLmFkZENsYXNzKCdmcC1hdXRvLWhlaWdodCcpO1xuXHRcdFx0X2VsZW1lbnRzID0gXCIuc2l0ZS1jb250ZW50XCI7XG5cdFx0fVxuXG5cdFx0JHdpZGdldF9mdWxscGFnZS5mdWxscGFnZSh7XG5cblx0XHRcdC8vdmVydGljYWxDZW50ZXJlZDogdHJ1ZSxcblx0XHRcdHJlc3BvbnNpdmVIZWlnaHQ6IHRydWUsXG5cdFx0XHRzY3JvbGxCYXI6IHRydWUsXG5cdFx0XHRhdXRvU2Nyb2xsaW5nOiBzY3JvbGxNb2RlLFxuXG5cdFx0XHQvLyB0byBhdm9pZCBwcm9ibGVtcyB3aXRoIGNzczMgdHJhbnNmb3JtcyBhbmQgZml4ZWQgZWxlbWVudHMgaW4gQ2hyb21lLFxuXHRcdFx0Ly8gYXMgZGV0YWlsZWQgaGVyZTogaHR0cHM6Ly9naXRodWIuY29tL2FsdmFyb3RyaWdvL2Z1bGxQYWdlLmpzL2lzc3Vlcy8yMDhcblx0XHRcdGNzczM6IHRydWUsXG5cdFx0XHRzY3JvbGxpbmdTcGVlZDogMTIwMCxcblx0XHRcdG5vcm1hbFNjcm9sbEVsZW1lbnRzOiBfZWxlbWVudHMsXG5cdFx0XHRzY3JvbGxPdmVyZmxvdzogdHJ1ZSxcblxuXHRcdFx0Ly8gVGhpcyBpcyBiZWluZyBzbyBwYWluIGluIGJlZXBcblx0XHRcdGZpdFRvU2VjdGlvbjogZmFsc2UsXG5cblx0XHRcdC8vIC8vIE1vdXNlIExlYXZlXG5cdFx0XHQvLyBhZnRlckxvYWQgOiBmdW5jdGlvbiAoYW5jaG9yLCBpbmRleCApIHtcblx0XHRcdC8vICAgICBpZiAoaW5kZXggPT0gMSkge1xuXHRcdFx0Ly8gICAgICAgICAkKCcuYW5pbWF0ZScsIHRoaXMpLmFkZENsYXNzKCdhbmltYXRlZCcpO1xuXHRcdFx0Ly8gICAgICAgICAkKCcubWVudS13cmFwcGVyJykucmVtb3ZlQ2xhc3MoJ3N0aWNreScpXG5cdFx0XHQvLyAgICAgfSBlbHNlIHtcblx0XHRcdC8vICAgICAgICAgJCgnLm1lbnUtd3JhcHBlcicpLmFkZENsYXNzKCdzdGlja3knKVxuXHRcdFx0Ly8gICAgIH1cblx0XHRcdC8vXG5cdFx0XHQvL1xuXHRcdFx0Ly8gfSxcblx0XHRcdG9uTGVhdmU6IGZ1bmN0aW9uIG9uTGVhdmUoaW5kZXgsIG5leHRJbmRleCwgZGlyZWN0aW9uKSB7XG5cblx0XHRcdFx0Y29uc29sZS5sb2coaW5kZXgsIG5leHRJbmRleCwgZGlyZWN0aW9uKTtcblxuXHRcdFx0XHQvLyBpbmRleCA9IGN1cnJlbnQgZGl2XG5cdFx0XHRcdHZhciByZXZlcnNlID0gZmFsc2U7XG5cdFx0XHRcdHZhciAkX2N1cnJlbnREaXYgPSAkKCcuc2VjdGlvbjplcSgnICsgKGluZGV4IC0gMSkgKyAnKScsICR3aWRnZXRfZnVsbHBhZ2UpO1xuXHRcdFx0XHRpZiAoJF9jdXJyZW50RGl2Lmhhc0NsYXNzKCdyZXYnKSkge1xuXHRcdFx0XHRcdHJldmVyc2UgPSB0cnVlO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0dmFyIG4gPSBuZXh0SW5kZXggLSAxO1xuXHRcdFx0XHQvLyB2YXIgJF90aGlzID0gJCh0aGlzKS5wYXJlbnQoKS5maW5kKCcuc2VjdGlvbjplcSgnICsgaW5kZXggKyAnKScpO1xuXG5cdFx0XHRcdC8vIGNoZWNrXG5cdFx0XHRcdHZhciBfdGFyZ2V0X2l0ZW0gPSAkKCcuc2VjdGlvbjplcSgnICsgbiArICcpJywgJHdpZGdldF9mdWxscGFnZSk7XG5cblx0XHRcdFx0LyoqXHJcbiAgICAgKlxyXG4gICAgICogSWYgY3VycmVudCBzZWN0aW9uIGlzIGxhc3Qgc2VjdGlvbi4gYW5kIGRpcmVjdGlvbiBpcyBkb3duIHRoZW4sXHJcbiAgICAgKiB0dXJuIG9mZiBhdXRvIHNjcm9sbGluZy5cclxuICAgICAqXHJcbiAgICAgKiovXG5cdFx0XHRcdC8vIGlmICggc2Nyb2xsTW9kZSApIHtcblx0XHRcdFx0Ly8gXHRpZiAoICQoJy5mcC1zZWN0aW9uLmxhc3Qtc2VjdGlvbicpLmhhc0NsYXNzKCdjdXJyZW50JykgJiYgZGlyZWN0aW9uID09ICd1cCcgKSB7XG5cdFx0XHRcdC8vIFx0XHQgJC5mbi5mdWxscGFnZS5zZXRBdXRvU2Nyb2xsaW5nKCB0cnVlICk7XG5cdFx0XHRcdC8vIFx0fSBlbHNlIHtcblx0XHRcdFx0Ly8gXHRcdC8vIHV0aWxzLnNjcm9sbF90b19lbGVtZW50KCBfdGFyZ2V0X2l0ZW0sIDE1MDAsIGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0Ly8gXHRcdCQuZm4uZnVsbHBhZ2Uuc2V0QXV0b1Njcm9sbGluZyhmYWxzZSk7XG5cdFx0XHRcdC8vIFx0XHQvLyB9ICk7XG5cdFx0XHRcdC8vXG5cdFx0XHRcdC8vIFx0fVxuXHRcdFx0XHQvLyB9XG5cblxuXHRcdFx0XHQvLyBjb25zb2xlLmxvZyggX3RhcmdldF9pdGVtICk7XG5cblx0XHRcdFx0Ly9AdG9kbyBOZWVkIHRvIHdvcmsgb24gcmV2ZXJzZVxuXHRcdFx0XHQvLyBpZiggcmV2ZXJzZSApIHtcblx0XHRcdFx0Ly8gXHR1dGlscy5hbmltYXRlKCAkX2N1cnJlbnREaXYgLCAnLmRvLWFuaW1hdGUnLCByZXZlcnNlKTtcblx0XHRcdFx0Ly8gfVxuXHRcdFx0XHRfdXRpbGl0aWVzMlsnZGVmYXVsdCddLmFuaW1hdGUoX3RhcmdldF9pdGVtLCAnLmRvLWFuaW1hdGUnKTtcblx0XHRcdH1cblx0XHR9KTtcblx0fVxuXG5cdGNvbnNvbGUubG9nKCcuLjo6IFdlbGNvbWUgOjouLicpO1xuXG5cdC8vIEFuaW1hdGlvbiBmb3IgQXJjaGl2ZSBQYWdlc1xuXHRpZiAoJCgnYm9keScpLmhhc0NsYXNzKCdhcmNoaXZlJykpIHtcblxuXHRcdCQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24gKCkge1xuXG5cdFx0XHRfdXRpbGl0aWVzMlsnZGVmYXVsdCddLnNjcm9sbF9ldmVudF90cmlnZ2VyKGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0XHQkKCdsaS5wcm9kdWN0JykuZWFjaChmdW5jdGlvbiAoaSwgdikge1xuXG5cdFx0XHRcdFx0aWYgKCQodGhpcykuaXNJblZpZXdncG9ydCgxMDApKSB7XG5cdFx0XHRcdFx0XHQkKHRoaXMpLndhaXQoMTUwMCAqIE1hdGguZmxvb3IoaSAvIDIpLCBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdFx0XHRcdCQodGhpcykuYWRkQ2xhc3MoJ3Rlc3RDbGFzcycpO1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblx0XHRcdH0oKSwgMjUwKTtcblx0XHR9KTtcblx0fVxuXG5cdC8vIENoZWNrIG9iamVjdCB0byBiZSBhbmltYXRlZCBmaXJzdCxcblx0Ly8gc28gY3VzdG9tIHNwZWVkIGNhbiBiZSBhcHBsaWVkXG5cdC8vIGluc3RlYWQgb2YgZGVmYXVsdFxuXHRpZiAoJCgnLmRvLWFuaW1hdGVbZGF0YS1zcGVlZF0nKS5sZW5ndGgpIHtcblx0XHQkKCcuZG8tYW5pbWF0ZVtkYXRhLXNwZWVkXScpLmVhY2goZnVuY3Rpb24gKCkge1xuXHRcdFx0dmFyICRfdGhpcyA9ICQodGhpcyk7XG5cdFx0XHQkX3RoaXMuY3NzKHtcblx0XHRcdFx0Jy13ZWJraXQtdHJhbnNpdGlvbi1kdXJhdGlvbic6IF91dGlsaXRpZXMyWydkZWZhdWx0J10uZ2V0U3BlZWQoJF90aGlzKSArIFwicyFJbXBvcnRhbnQ7XCIsXG5cdFx0XHRcdCctbW96LXRyYW5zaXRpb24tZHVyYXRpb24nOiBfdXRpbGl0aWVzMlsnZGVmYXVsdCddLmdldFNwZWVkKCRfdGhpcykgKyBcInMhSW1wb3J0YW50O1wiLFxuXHRcdFx0XHQndHJhbnNpdGlvbi1kdXJhdGlvbic6IF91dGlsaXRpZXMyWydkZWZhdWx0J10uZ2V0U3BlZWQoJF90aGlzKSArIFwicyFJbXBvcnRhbnQ7XCIsXG5cdFx0XHRcdCctd2Via2l0LWFuaW1hdGlvbi1kdXJhdGlvbic6IF91dGlsaXRpZXMyWydkZWZhdWx0J10uZ2V0U3BlZWQoJF90aGlzKSArIFwicyFJbXBvcnRhbnQ7XCIsXG5cdFx0XHRcdCctbW96LWFuaW1hdGlvbi1kdXJhdGlvbic6IF91dGlsaXRpZXMyWydkZWZhdWx0J10uZ2V0U3BlZWQoJF90aGlzKSArIFwicyFJbXBvcnRhbnQ7XCIsXG5cdFx0XHRcdCdhbmltYXRpb24tZHVyYXRpb24nOiBfdXRpbGl0aWVzMlsnZGVmYXVsdCddLmdldFNwZWVkKCRfdGhpcykgKyBcInMhSW1wb3J0YW50O1wiXG5cdFx0XHR9KTtcblx0XHR9KTtcblx0fVxuXG5cdC8vIFNsaWNrIFNsaWRlci0tLVxuXHQvLyBBcHBseSBDbGFzcyBvbiB3aWRnZXQgY3VycmVudGx5LlxuXHRpZiAoJCgnLnNsaWNrLWNvbnRhaW5lcicpLmxlbmd0aCkge1xuXG5cdFx0JCgnLnNsaWNrLWNvbnRhaW5lciAuc3MtYmFubmVyLWNvbnQgPiB1bCcpLmVhY2goZnVuY3Rpb24gKGksIHYpIHtcblxuXHRcdFx0Ly8gSWYgbW9yZSB0aGFuIDEgTGkgYXJlIGhlcmUuXG5cdFx0XHQvLyBNb3JlIHRoYW4gMSBtZWFucyBuZWVkIHRvIHNsaWRlXG5cdFx0XHRpZiAoJCgnbGknLCAkKHRoaXMpKS5sZW5ndGggPiAxKSB7XG5cdFx0XHRcdCQodGhpcykuc2xpY2soe1xuXHRcdFx0XHRcdHNwZWVkOiA2MDBcblx0XHRcdFx0XHQvLyBhZGFwdGl2ZUhlaWdodDogdHJ1ZVxuXHRcdFx0XHR9KTtcblx0XHRcdH1cblx0XHR9KTtcblx0fTtcbn0pOyAvLyBqUXVlcnkgc2NvcGVcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWFwcC5qcy5tYXBcbiIsIid1c2Ugc3RyaWN0JztcblxuLyoqXHJcbiAqIENyZWF0ZWQgYnkgeGFmYXJcclxuICovXG4kID0gd2luZG93LmpRdWVyeTtcbi8vIENvb2tpZXMgPSB3aW5kb3cuQ29va2llcygpO1xuaWYgKHhhc2hfdXRpbGl0eS54YXNoRGVidWcpIGNvbnNvbGUubG9nKCdqcXVlcnkuanMgTG9hZGVkJyk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1qcXVlcnkuanMubWFwXG4iLCIndXNlIHN0cmljdCc7XG5cbi8qKlxyXG4gKiBDcmVhdGVkIGJ5IHhhZmFSLlxyXG4gKiBDb3B5cmlnaHQgWEFTSExhYnNcclxuICogU2V0IG9mIEN1c3RvbSBQbHVnaW5zIG9yIG1heWJlIGNvcHkvcGFzdGUgdmVuZG9yIHBsdWdpbnMgaGVyZSBhcyB3ZWxsLi4uXHJcbiAqL1xuXG4vLyBhZGQgd2FpdCBhcyAkLndhaXQoKSBzdGFuZGFsb25lIGFuZCAkKGVsZW0pLndhaXQoKSBmb3IgYW5pbWF0aW9uIGNoYWluaW5nXG4oZnVuY3Rpb24gKCQpIHtcblx0aWYgKHhhc2hfdXRpbGl0eS54YXNoRGVidWcpIGNvbnNvbGUubG9nKCdqUXVlcnkucGx1Z2lucyBMb2FkZWQnKTtcblxuXHQvLyBKcXVlcnkgV2FpdCBwbHVnaW4sIGJldHRlciB0aGFuIHNldFRpbWVvdXQgbmF0aXZlLlxuXHQkLndhaXQgPSBmdW5jdGlvbiAoZHVyYXRpb24sIGNvbXBsZXRlQ2FsbGJhY2ssIHRhcmdldCkge1xuXHRcdHZhciAkdGFyZ2V0ID0gJCh0YXJnZXQgfHwgJzxxdWV1ZSAvPicpO1xuXHRcdHJldHVybiAkdGFyZ2V0LmRlbGF5KGR1cmF0aW9uKS5xdWV1ZShmdW5jdGlvbiAobmV4dCkge1xuXHRcdFx0Y29tcGxldGVDYWxsYmFjay5jYWxsKCR0YXJnZXQpO1xuXHRcdFx0bmV4dCgpO1xuXHRcdH0pO1xuXHR9O1xuXG5cdCQuZm4ud2FpdCA9IGZ1bmN0aW9uIChkdXJhdGlvbiwgY29tcGxldGVDYWxsYmFjaykge1xuXHRcdHJldHVybiAkLndhaXQuY2FsbCh0aGlzLCBkdXJhdGlvbiwgY29tcGxldGVDYWxsYmFjaywgdGhpcyk7XG5cdH07XG5cblx0Ly8gQ2hlY2tzICYgcmV0dXJucyBpdGVtIGluIHZpZXdwb3J0LlxuXHQkLmZuLmlzSW5WaWV3cG9ydCA9IGZ1bmN0aW9uICgpIHtcblx0XHR2YXIgb2Zmc2V0ID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiAwO1xuXG5cdFx0dmFyIGVsZW1lbnRUb3AgPSAkKHRoaXMpLm9mZnNldCgpLnRvcDtcblx0XHR2YXIgZWxlbWVudEJvdHRvbSA9IGVsZW1lbnRUb3AgKyAkKHRoaXMpLm91dGVySGVpZ2h0KCk7XG5cblx0XHR2YXIgdmlld3BvcnRUb3AgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgKyBvZmZzZXQ7XG5cdFx0dmFyIHZpZXdwb3J0Qm90dG9tID0gdmlld3BvcnRUb3AgKyAkKHdpbmRvdykuaGVpZ2h0KCk7XG5cblx0XHRyZXR1cm4gZWxlbWVudEJvdHRvbSA+IHZpZXdwb3J0VG9wICYmIGVsZW1lbnRUb3AgPCB2aWV3cG9ydEJvdHRvbTtcblx0fTtcbn0pKGpRdWVyeSk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1qcXVlcnkucGx1Z2lucy5qcy5tYXBcbiIsIid1c2Ugc3RyaWN0JztcblxucmVxdWlyZSgnLi9qcXVlcnknKTtcblxudmFyIF91dGlsaXRpZXMgPSByZXF1aXJlKCcuL3V0aWxpdGllcycpO1xuXG52YXIgX3V0aWxpdGllczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF91dGlsaXRpZXMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyAnZGVmYXVsdCc6IG9iaiB9OyB9XG5cbi8qKlxyXG4gKiBDcmVhdGVkIGJ5IHhhZmFSLlxyXG4gKiBDb3B5cmlnaHQgWEFTSExhYnNcclxuICogQWxsIE1lbnUgcmVsYXRlZCBmdW5jdGlvbnNcclxuICovXG5cbnZhciBfdGFyZ2V0X21lbnUgPSAnLm5hdmJhci10b2dnbGUnLFxuICAgIF9tZW51X2JveCA9ICcubWVudS1jdXJ0YWluJztcblxualF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoJCkge1xuXG5cdGlmICh4YXNoX3V0aWxpdHkueGFzaERlYnVnKSBjb25zb2xlLmxvZygnTWVudS5qcyBMb2FkZWQnKTtcblxuXHR2YXIgaXNfaG9tZSA9ICQoJ2JvZHknKS5oYXNDbGFzcygnaG9tZScpO1xuXG5cdC8qICBTZWFyY2ggQnV0dG9uXHJcbiAgLy8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSovXG5cblx0JCgnLnN1cGVyLW1lbnUgbGkuc2VhcmNoLWJ0bicpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG5cdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdFx0Ly8gYWxlcnQoJ01pbmkgQ2FydCBjbGlja2VkLicpXG5cdFx0JCgnI2hlYWRlci1zZWFyY2gnKS5tb2RhbCgnc2hvdycpO1xuXHR9KTtcblxuXHQvLyBHZXQgZWxlbWVudCBoZWlnaHQgd2l0aCBpdHMgb2Zmc2V0IHBvc2l0aW9uXG5cdC8vIHNvIGFjdHVhbGwgbG9jYXRpb24gaXMgZGlzY292ZXJlZFxuXHRmdW5jdGlvbiBnZXRfbWVudV9idG5fcG9zKGVsZW0pIHtcblx0XHRyZXR1cm4gJChlbGVtKS5vZmZzZXQoKS50b3AgKyAkKGVsZW0pLmhlaWdodCgpO1xuXHR9XG5cblx0Ly8gRHJvcCBEb3ducyBmb3IgQm9vdHN0cmFwXG5cdC8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5cblx0LypcclxuICQoJy5fX19kcm9wZG93bicpLm9uKCdzaG93LmJzLmRyb3Bkb3duJywgZnVuY3Rpb24oKSB7XHJcbiBcdCQodGhpcykuZmluZCgnLmRyb3Bkb3duLW1lbnUnKS5maXJzdCgpLnN0b3AodHJ1ZSwgdHJ1ZSkuc2xpZGVEb3duKCk7XHJcbiB9KTtcclxuIFxuIC8vIEFkZCBzbGlkZVVwIGFuaW1hdGlvbiB0byBCb290c3RyYXAgZHJvcGRvd24gd2hlbiBjb2xsYXBzaW5nLlxyXG4gJCgnLl9fX2Ryb3Bkb3duJykub24oJ2hpZGUuYnMuZHJvcGRvd24nLCBmdW5jdGlvbigpIHtcclxuIFx0JCh0aGlzKS5maW5kKCcuZHJvcGRvd24tbWVudScpLmZpcnN0KCkuc3RvcCh0cnVlLCB0cnVlKS5zbGlkZVVwKCk7XHJcbiB9KTtcclxuICovXG5cblx0JCgnLm1haW4tbmF2aWdhdGlvbiAuZHJvcGRvd24nKS5ob3ZlcihmdW5jdGlvbiAoKSB7XG5cdFx0JCh0aGlzKS5maW5kKCcuZHJvcGRvd24tbWVudScpLmZpcnN0KCkuc3RvcCh0cnVlLCB0cnVlKS5hZGRDbGFzcygnc2hvdy1tZW51Jyk7XG5cdH0sIGZ1bmN0aW9uICgpIHtcblx0XHQkKHRoaXMpLmZpbmQoJy5kcm9wZG93bi1tZW51JykuZmlyc3QoKS5zdG9wKHRydWUsIHRydWUpLnJlbW92ZUNsYXNzKCdzaG93LW1lbnUnKTtcblx0fSk7XG5cblx0Ly8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblx0Ly8gIERST1BET1dOIFNUVUZGICAgICAgICAgICAgIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblx0Ly8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblxuXHQkKCcuZHJvcGRvd24tdG9nZ2xlJykuZWFjaChmdW5jdGlvbiAoaSwgdikge1xuXHRcdCQodGhpcykuYXR0cignZGF0YS10b2dnbGUnLCAnZHJvcGRvd24nKTtcblx0fSk7XG59KTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPW1lbnUuanMubWFwXG4iLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfdXRpbHM7IC8qKlxyXG4gICAgICAgICAgICAgKiBDcmVhdGVkIGJ5IHhhZmFSXHJcbiAgICAgICAgICAgICAqIENvcHlyaWdodCBYQVNITGFic1xyXG4gICAgICAgICAgICAgKiBVdGlsaXR5IE1vZHVsZSBpcyBiYXNlZCBvbiBzdGF0aWMgZnVuY3Rpb25zXHJcbiAgICAgICAgICAgICAqL1xuXG5yZXF1aXJlKCcuL2pxdWVyeScpO1xuXG5pZiAoeGFzaF91dGlsaXR5Lnhhc2hEZWJ1ZykgY29uc29sZS5sb2coJ1V0aXRsaWVzLmpzIExvYWRlZCcpO1xudmFyIHV0aWxzID0gKF91dGlscyA9IHtcblxuXHQvLyBVc2luZyBvYmplY3QgZGVmaW5lIGdsb2JhbGx5LCB4YXNoX3V0aWxpdHkgdmlhIGxvY2FsaXplZF9zY3JpcHRcblx0dGhlbWVVUkw6IHhhc2hfdXRpbGl0eS50aGVtZVVyaSxcblxuXHRzZWxmOiB1bmRlZmluZWQsXG5cblx0Ly8gTG9hZCBDU1MgRWxlbWVudCwgaW5zdGVhZCBvZiBxdWV1ZVxuXHRsb2FkQ3NzOiBmdW5jdGlvbiBsb2FkQ3NzKHBhdGgsIGxvY2F0aW9uKSB7XG5cdFx0dmFyIGxpbmsgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGlua1wiKTtcblx0XHRsb2NhdGlvbiA9IGxvY2F0aW9uIHx8IFwiaGVhZFwiO1xuXHRcdGxpbmsudHlwZSA9IFwidGV4dC9jc3NcIjtcblx0XHRsaW5rLnJlbCA9IFwic3R5bGVzaGVldFwiO1xuXHRcdGxpbmsuaHJlZiA9IHBhdGg7XG5cdFx0JChsaW5rKS5hcHBlbmRUbygkKGxvY2F0aW9uKSk7XG5cdH0sXG5cblx0Ly8gQ29uc29sZSBMb2dzIGluIGRlYnVnIG1vZGVcblx0ZGVidWdQcmludDogZnVuY3Rpb24gZGVidWdQcmludCgpIHtcblx0XHR2YXIgJHN0cmluZyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogbnVsbDtcblxuXHRcdGNvbnNvbGUubG9nKCRzdHJpbmcpO1xuXHR9LFxuXG5cdG1lbnVJc1Zpc2libGU6IGZ1bmN0aW9uIG1lbnVJc1Zpc2libGUoKSB7XG5cdFx0dmFyICRtZW51ID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiBudWxsO1xuXG5cdFx0cmV0dXJuICQoJG1lbnUpLmlzKFwiOnZpc2libGVcIik7XG5cdH0sXG5cblx0Z2V0U3BlZWQ6IGZ1bmN0aW9uIGdldFNwZWVkKCRlbGUpIHtcblx0XHRyZXR1cm4gcGFyc2VJbnQodGhpcy5nZXRBdHRyKCRlbGUsICdzcGVlZCcpKTtcblx0fSxcblxuXHRnZXREZWxheTogZnVuY3Rpb24gZ2V0RGVsYXkoJGVsZSkge1xuXHRcdHJldHVybiBwYXJzZUludCh0aGlzLmdldEF0dHIoJGVsZSwgJ2RlbGF5JykpO1xuXHR9LFxuXG5cdGdldEFuaW1hdGlvbjogZnVuY3Rpb24gZ2V0QW5pbWF0aW9uKCRlbGUpIHtcblx0XHRyZXR1cm4gJC50cmltKHRoaXMuZ2V0QXR0cigkZWxlLCAnYW5pbScpKTtcblx0fVxuXG59LCBfdXRpbHNbJ2dldFNwZWVkJ10gPSBmdW5jdGlvbiBnZXRTcGVlZCgkZWxlKSB7XG5cdHJldHVybiAkLnRyaW0odGhpcy5nZXRBdHRyKCRlbGUsICdzcGVlZCcpKTtcbn0sIF91dGlscy5nZXRBdHRyID0gZnVuY3Rpb24gZ2V0QXR0cigkZWxlLCBhdHRyKSB7XG5cdHJldHVybiAkZWxlLmRhdGEoYXR0cik7XG59LCBfdXRpbHMuYW5pbWF0ZSA9IGZ1bmN0aW9uIGFuaW1hdGUoJHBhcmVudF9lbGVtZW50KSB7XG5cdHZhciAkdGFyZ2V0X2VsZW1lbnQgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6ICcuZG8tYW5pbWF0ZSc7XG5cdHZhciAkcmV2ZXJzZSA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDogZmFsc2U7XG5cblxuXHQvL1x0TmVlZCB0byBhZGQgY2xhc3Mgd2hvIGdvdCBkZWxheSBwYXJhbWVyIGxhdGUuXG5cdC8vXHRDaGVjayBEZWxheSBmaXJzdC5cblx0Ly8gdmFyIGN1cl9hY3Rpb24gPSBhY3Rpb247XG5cblx0Ly8gcmVwZWF0aW9uIG9mIGFuaW1hdGlvblxuXHQvLyAkKCcuY3VycmVudCcpLmZpbmQoJy5yZXBlYXQnKS5lYWNoKGZ1bmN0aW9uICgpIHtcblx0Ly8gXHQkKHRoaXMpLmFkZENsYXNzKCdhbmltYXRlJyk7XG5cdC8vIH0pO1xuXG5cdCQoJy5jdXJyZW50JykucmVtb3ZlQ2xhc3MoJ2N1cnJlbnQnKTtcblx0JHBhcmVudF9lbGVtZW50LmFkZENsYXNzKCdjdXJyZW50Jyk7XG5cblx0Y29uc29sZS5sb2codGhpcyk7XG5cblx0JCgkcGFyZW50X2VsZW1lbnQpLmZpbmQoJHRhcmdldF9lbGVtZW50KS5lYWNoKGZ1bmN0aW9uIChrLCB2KSB7XG5cblx0XHR2YXIgX3RoaXMgPSAkKHRoaXMpO1xuXG5cdFx0dmFyIF9kZWxheSA9IHV0aWxzLmdldERlbGF5KF90aGlzKTsgLy8gdGhpcy4gc2NvcGUgaXNzdWUsIHNvIHJlZmVyaW5nIHBhcmVudFxuXHRcdHZhciBfYW5pbSA9IHV0aWxzLmdldEFuaW1hdGlvbihfdGhpcyk7XG5cblx0XHQvLyBQdWxsIEJhY2sgYWxsIGFuaW1hdGlvbiBpZiByZXZlcnNlIG1vZGUgaXMgb24uXG5cdFx0Ly8gQG5vdGUgbmVlZCB0byByZXZlcnNlIGRlbGF5IGFzIHdlbGwsIGZvciBiZXR0ZXIgZWZmZWN0XG5cdFx0aWYgKCRyZXZlcnNlKSB7XG5cblx0XHRcdGlmIChfZGVsYXkpIHtcblxuXHRcdFx0XHQvLyB0aW1lclxuXHRcdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRfdGhpcy5yZW1vdmVDbGFzcyhfYW5pbSkuZGVsYXkoMjAwKTtcblx0XHRcdFx0XHRpZiAoX3RoaXMuaGFzQ2xhc3MoJ19pbnZpcycpKSB7XG5cdFx0XHRcdFx0XHRfdGhpcy5hZGRDbGFzcygnaW52aXNpYmxlJykucmVtb3ZlQ2xhc3MoJ19pbnZpcycpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSwgX2RlbGF5KTtcblxuXHRcdFx0XHQvLyBleGl0LlxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0Ly8gTm9ybWFsIEFuaW1hdGlvbiBQcm9jZWR1cmVcblx0XHRpZiAoX2RlbGF5KSB7XG5cblx0XHRcdC8vIHRpbWVyXG5cdFx0XHRzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0X3RoaXMuYWRkQ2xhc3MoX2FuaW0pLmRlbGF5KDIwMCk7XG5cdFx0XHRcdGlmIChfdGhpcy5oYXNDbGFzcygnaW52aXNpYmxlJykpIHtcblx0XHRcdFx0XHRfdGhpcy5hZGRDbGFzcygnX2ludmlzJykucmVtb3ZlQ2xhc3MoJ2ludmlzaWJsZScpO1xuXHRcdFx0XHR9XG5cdFx0XHR9LCBfZGVsYXkpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRfdGhpcy5yZW1vdmVDbGFzcyhfYW5pbSk7XG5cdFx0fVxuXHR9KTtcbn0sIF91dGlscy5zY3JvbGxfZXZlbnRfdHJpZ2dlciA9IGZ1bmN0aW9uIHNjcm9sbF9ldmVudF90cmlnZ2VyKGZuLCB0aW1lb3V0KSB7XG5cdHZhciB3YWl0aW5nID0gYXJndW1lbnRzLmxlbmd0aCA+IDIgJiYgYXJndW1lbnRzWzJdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMl0gOiBmYWxzZTtcblxuXG5cdC8vIG1ha2luZyBpdCBsb2NhbFxuXHR2YXIgX3dhaXRpbmcgPSB3YWl0aW5nO1xuXG5cdGlmIChfd2FpdGluZykge1xuXHRcdHJldHVybjtcblx0fSBlbHNlIHtcblxuXHRcdHZhciBfdGltZXIgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0Zm47XG5cdFx0XHRfd2FpdGluZyA9IHRydWU7XG5cdFx0fSwgdGltZW91dCk7XG5cdH1cbn0sIF91dGlscy5zY3JvbGxfdG9fZWxlbWVudCA9IGZ1bmN0aW9uIHNjcm9sbF90b19lbGVtZW50KGVsZW0pIHtcblx0dmFyIHNwZWVkID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiAxMDAwO1xuXHR2YXIgY2FsbGJhY2sgPSBhcmd1bWVudHMubGVuZ3RoID4gMiAmJiBhcmd1bWVudHNbMl0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1syXSA6IG51bGw7XG5cdHZhciBlYXNpbmcgPSBhcmd1bWVudHNbM107XG5cblx0JCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xuXHRcdHNjcm9sbFRvcDogJChlbGVtKS5vZmZzZXQoKS50b3Bcblx0fSwge1xuXHRcdGR1cmF0aW9uOiBzcGVlZCxcblx0XHRjb21wbGV0ZTogZnVuY3Rpb24gY29tcGxldGUoKSB7XG5cdFx0XHRpZiAoJC5pc0Z1bmN0aW9uKGNhbGxiYWNrKSkge1xuXHRcdFx0XHRjYWxsYmFjaygpO1xuXHRcdFx0fVxuXHRcdH1cblx0fSk7XG59LCBfdXRpbHMuc3RpY2t5X3Rlc3RfYWRkID0gZnVuY3Rpb24gc3RpY2t5X3Rlc3RfYWRkKGNsYXNzTmFtZSwgbGltaXQpIHtcblx0dmFyICRfYm9keSA9ICQoJ2JvZHknKTtcblxuXHRpZiAoJCh3aW5kb3cpLnNjcm9sbFRvcCgpID4gbGltaXQpIHtcblx0XHQkX2JvZHkuYWRkQ2xhc3MoY2xhc3NOYW1lKTtcblx0fSBlbHNlIHtcblx0XHQkX2JvZHkucmVtb3ZlQ2xhc3MoY2xhc3NOYW1lKTtcblx0fVxufSwgX3V0aWxzLmdldF9jYXJ0X2l0ZW1fbnVtYmVyID0gZnVuY3Rpb24gZ2V0X2NhcnRfaXRlbV9udW1iZXIoZWxlbSkge1xuXHRyZXR1cm4gcGFyc2VJbnQoJChlbGVtKS50ZXh0KCkuc3BsaXQoJyAnKVswXSk7XG59LCBfdXRpbHMuZ2VuZXJhdGVNb2RhbCA9IGZ1bmN0aW9uIGdlbmVyYXRlTW9kYWwocGxhY2VtZW50SWQsIGhlYWRpbmcsIGZvcm1Db250ZW50KSB7XG5cdHZhciB0aW1lciA9IGFyZ3VtZW50cy5sZW5ndGggPiAzICYmIGFyZ3VtZW50c1szXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzNdIDogMDtcblx0dmFyIHN0clN1Ym1pdEZ1bmMgPSBhcmd1bWVudHNbNF07XG5cdHZhciBidG5UZXh0ID0gYXJndW1lbnRzWzVdO1xuXG5cblx0c3RyU3VibWl0RnVuYyA9IHN0clN1Ym1pdEZ1bmMgfHwgbnVsbDtcblx0aGVhZGluZyA9IGhlYWRpbmcgfHwgbnVsbDtcblxuXHR2YXIgaHRtbCA9ICc8ZGl2IGlkPVwibW9kYWxXaW5kb3dcIiBjbGFzcz1cIm1vZGFsIGZhZGUgXCIgc3R5bGU9XCJkaXNwbGF5Om5vbmU7XCIgdGFiaW5kZXg9XCItMVwiIHJvbGU9XCJkaWFsb2dcIj4nO1xuXHRodG1sICs9IFwiPGRpdiBjbGFzcz0nbW9kYWwtZGlhbG9nJz48ZGl2IGNsYXNzPSdtb2RhbC1jb250ZW50Jz5cIjtcblx0aHRtbCArPSAnPGRpdiBjbGFzcz1cIm1vZGFsLWhlYWRlclwiPic7XG5cdGh0bWwgKz0gJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPsOXPC9idXR0b24+JztcblxuXHRpZiAoaGVhZGluZyAhPSBudWxsKSB7XG5cdFx0aHRtbCArPSAnPGgzPicgKyBoZWFkaW5nICsgJzwvaDM+Jztcblx0fVxuXHRodG1sICs9ICc8L2Rpdj4nO1xuXG5cdC8vaHRtbCArPSAnPHA+Jztcblx0aHRtbCArPSAnPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj4nO1xuXHRodG1sICs9IGZvcm1Db250ZW50O1xuXHRodG1sICs9ICc8L2Rpdj4nO1xuXG5cdGlmIChidG5UZXh0ICE9ICcnICYmIHN0clN1Ym1pdEZ1bmMgIT0gbnVsbCkge1xuXHRcdGh0bWwgKz0gJzxkaXYgY2xhc3M9XCJtb2RhbC1mb290ZXJcIj4nO1xuXHRcdGh0bWwgKz0gJzxzcGFuIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCInO1xuXHRcdGh0bWwgKz0gJyBvbkNsaWNrPVwiJyArIHN0clN1Ym1pdEZ1bmMgKyAnXCI+JyArIGJ0blRleHQ7XG5cdFx0aHRtbCArPSAnPC9zcGFuPic7XG5cdFx0aHRtbCArPSAnPHNwYW4gY2xhc3M9XCJidG5cIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiPic7XG5cdFx0aHRtbCArPSAnQ2xvc2UnO1xuXHRcdGh0bWwgKz0gJzwvc3Bhbj4nOyAvLyBjbG9zZSBidXR0b25cblx0XHRodG1sICs9ICc8L2Rpdj4nOyAvLyBmb290ZXJcblx0fVxuXHRodG1sICs9ICc8L2Rpdj48L2Rpdj4nOyAvLyBtb2RhbENvbnRlbnRcblx0aHRtbCArPSAnPC9kaXY+JzsgLy8gbW9kYWxXaW5kb3dcblxuXHQkKGh0bWwpLmFwcGVuZFRvKHBsYWNlbWVudElkKTtcblx0Ly8gJCggcGxhY2VtZW50SWQgKS5odG1sKGh0bWwpO1xuXHQkKFwiI21vZGFsV2luZG93XCIpLm1vZGFsKCdzaG93Jyk7XG5cblx0dmFyIF90aW1lcl9kaWFsb2cgPSBudWxsO1xuXG5cdC8vIGlmIFRpbWVyIGlzIE9uXG5cdGlmICh0aW1lcikge1xuXHRcdF90aW1lcl9kaWFsb2cgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0JCgnI21vZGFsV2luZG93JykubW9kYWwoJ2hpZGUnKTtcblx0XHR9LCB0aW1lcik7XG5cdH1cblxuXHQkKCcjbW9kYWxXaW5kb3cnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgZnVuY3Rpb24gKCkge1xuXHRcdCQodGhpcykucmVtb3ZlKCk7XG5cdFx0Y2xlYXJUaW1lb3V0KF90aW1lcl9kaWFsb2cpO1xuXHR9KTtcbn0sIF91dGlscyk7IC8vIHV0aWxzXG5cblxuLy8gRXhwb3J0cyBNb2R1bGVcbmV4cG9ydHNbJ2RlZmF1bHQnXSA9IHV0aWxzO1xuLy8gfSk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD11dGlsaXRpZXMuanMubWFwXG4iXX0=
