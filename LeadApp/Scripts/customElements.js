// JavaScript Document
/*
 * Custom Script Functions for Layout Interactive Elements
 * Developed : SepiaSolutions
 * Updated: Oct 2014
 * Client : Alpine Vessel and Client Management
 * Version : 1.5v


*/

jQuery(document).ready(function ($) {




    //----------------------------------------
    //    Select Elements -singular 
    //----------------------------------------
    // console.clear()
    // console.time("select");
    $(window).load(function () {
        rawElement = 'select';
        excludeElement = '.no-select';

        targetElement = $(rawElement).not(excludeElement);

        prettyInput($(targetElement));
        // console.timeEnd('select');
    })

    $('.mmdd').click(function () {
        redraw($('#mm'))
        return false;
    })



    function prettyInput(element) {


        // fancy select style application 
        $(element).each(function (i) {

            elem = $(this);
            elem.addClass('select-customized');

            nChild = $('option', elem).length > 5 ? filterShow = "" : filterShow = "no-filter";

            if (elem.is("[multiple]")) {

                _contClass = "select-box multiple"
                multiSelect = true;

            } else {

                _contClass = "select-box single";
                multiSelect = false;

            }



            // main interface which is visible by default
            var mainInterface = "";

            mainInterface = '<div class="bg-overlay"></div>';
            mainInterface += '<div class="' + _contClass + '">';
            mainInterface += '<div class="select-item">';
            // mainInterface += '	<label class="selected-option">Please Select</label>';
            mainInterface += '	<span class="select-arrow"></span>';

            mainInterface += '<div class="select-cont">';
            mainInterface += '  <div class="filter"><input class="finder" type="text" /><a href="#" class="btn-find">Search</a></div>';
            mainInterface += '</div>';

            mainInterface += '	</div>';
            mainInterface += '</div>';



            $(mainInterface).insertAfter(elem);
            elem.appendTo(elem.parent().find($('.select-cont')));
            var elemParent = $(this).parent($('div'));


            redrawOption(this, elemParent, true);

        }) // main select loop

    }

    //----------------------------------
    // ADD CLICK TO SINGLE SELECT
    //------------------------------------
    /*
        $('.select-box.single').on( 'click','li.act-option',  function (e) {
            
            $('.select-box.single li.selected').removeClass('selected').addClass('act-option');
            
            $(this).addClass('selected').removeClass('act-option');
    
            $(this).parents('.select-box').find('label.selected-option').html( $(this).html() );
    
            var selVal = $(this).attr('data-value')
            $(this).parents('.select-box').find('select:first').val( selVal ).trigger('change');
    
    //		 console.log(selVal);
    
            // hide container
            hideBox('.select-box .option-box');
    
    
        })
    
    
        //----------------------------------------
        // Multile select with click or Drag!
        //----------------------------------------
            
            // CLICK------------------------------
            var dragMode = false;
            $('.select-box.multiple').on( 'click', 'li.act-option', function(e){
    
                //$(this).addClass('selected');
    
            })
    
    
            //drag   // MOUSE DOWN ------------------------------
            $('.select-box.multiple').on( 'mousedown', 'li' , function(e){
                e.preventDefault();
                dragMode = true;
                
                //$(this).toggleClass('.act-option selected');
                $(this).addClass('selected').removeClass('act-option')
    
    
            });
    
            // MOUSE MOVE ------------------------------
            $('.select-box.multiple').on( 'mousemove', 'li.act-option', function(e){
                if( dragMode ) {
                    
                    $(this).addClass('selected').removeClass('act-option');
    
                    e.preventDefault();
                    ////  console.log('mouse move');
                }
            });
    
    
            // MOUSE UP ------------------------------
            $('.select-box.multiple').on('mouseup','li',function(e) {
                
    //			 console.log("mouse up");
                dragMode = false;
                var parent  =$(this).parents('.select-box');
                var select = parent.find('select:first');
                
                // reset
                select.val("").removeAttr('selected');
                var selectedItemTag="";			
                valset=[];
    
                $( 'li.selected', parent ).each(function(i){
                    
                    multiInd = $(this).attr('data-index');
                    valset[i] = $(this).attr('data-value');
    
                    var _dataIndex = $(this).attr('data-index');
                    var _text = $(this).text();
                    selectedItemTag += "<div class='sel-item-tag' data-index="+ _dataIndex +" title='" + _text +"'><span>" + _text +"</span><span class='del-item'></span></div>";
    
                    
                    
                });
                
                // console.log(valset);
                select.val(valset);
                
                defaultTextHolder = parent.find($('.selected-option'))
                //// console.log(defaultTextHolder, " " , selectedItemTag)
                
                // append tags to selection 
                $(defaultTextHolder).html(selectedItemTag);
    
    
                
                // hide container
                if(! e.ctrlKey ) {
                    hideBox('.select-box .option-box');
                }
    
            });
    
    
            // MOUSE LEAVE ------------------------------
            $('.select-box.multiple ul').on('mouseleave', function(e) {
                dragMode = false;
            });
    
        */

    /* ----------------------------- */






    // show hide menu.
    $('.contentDiv,#mydialog').on('click', '.selected-option', function () {
        $(this).next().trigger('click');
    })


    // ---------------------------------------------------------------------------
    // EVENTS CLICK---------------------------------------------------------------
    // ---------------------------------------------------------------------------

    $('.contentDiv, #mydialog').on('click', '.select-arrow', function () {

        //target cached!
        var parent = $(this).parents('.select-box')
        var target = parent.find('.select-cont');


        // background overlay layer
        var bg = $(this).parents('.select-box').prev('.bg-overlay');


        if (target.is(":hidden")) {

            $('.bg-overlay').hide();
            $('.select-cont').hide();
            $('.select-box').removeClass('active');

            bg.show()

            target.show();
            $('.select-arrow').removeClass('close')
            parent.addClass('active');
            $(this).addClass('close');

            parent.find('.finder').focus();


        } else {
            parent.removeClass('active')
            target.hide();
            target.find('option').removeClass('not-found');
            $(this).removeClass('close');
            $('.bg-overlay').hide();
        }


    })



    $('.contentDiv, #mydialog').on('click', '.filter .btn-find', function (key) {

        var type;
        clearTimeout(type);

        textbox = $(this).prev('input')
        val = textbox.val()

        type = setTimeout(
            function () {
                searchList(val, textbox);
                // console.log(val);
            }, 1000);


        return false

    })





    //---------------------------------
    // Single Selector
    //---------------------------------
    $('.contentDiv,#mydialog').on('click', '.select-box.single .select-customized option', function () {
        var parent = $(this).parents('.select-box')
        // console.log('options clicked');
        parent.find('label.selected-option').html($(this).html());
        $('.select-cont', parent).hide();
        parent.removeClass('active');

        // class work
        $(this).siblings().removeClass('selected');
        $(this).addClass("selected");

    })


    //-------------------------------------------------------------
    // Multi Selector
    //-------------------------------------------------------------

    /* Mouse down Event */

    var dragMode = false

    $('.contentDiv, #mydialog').on('mousedown', '.select-box.multiple .select-customized option', function () {
        // turn on drag mode
        dragMode = true

        $(this).addClass('selected');
        $(this).parents('.select-item').find('.selected-option').remove()

        // adding tags
        var allLabels = $(this).parents('.select-item').children('.sel-item-tag ');



    })


    // mouse drag 
    $('.contentDiv, #mydialog').on('mouseover', '.select-box.multiple .select-customized option', function () {
        if (dragMode) {
            $(this).addClass('selected');
            console.log('draging')
        }
    })


    /* Mouse Up Event */

    $('.contentDiv,#mydialog').on('mouseup', '.select-box.multiple .select-customized option', function () {
        dragMode = false;

        // adding tags
        var allLabels = $(this).parents('.select-item').children('.sel-item-tag ');




        var _tmpValue = [];

        var newTag = "";
        $(this).parents('.select-item').find(".selected-option").remove();
        $(this).parents('.select-item').find(".sel-item-tag").remove();
        $(this).parents('.select-box').find('.selected').each(function (i) {
            console.log(i);
            _text = $(this).text();
            _value = $(this).val();

            newTag += "<label class='sel-item-tag' data-value=" + _value + " title='" + _text + "'><span>" + _text + "</span><span class='del-item'></span></label>";
            _tmpValue[i] = $(this).val()
        })

        $(newTag).appendTo($(this).parents('.select-item'));
        $(this).parent().val(_tmpValue);


        return false

    })


    $('.contentDiv,#mydialog').on('click', '.select-box .selected', function (e) {
        return false
    })


    // tag cleaner
    $('.contentDiv,#mydialog').on('click', '.select-box .del-item', function (e) {

        var parent = $(this).parents('.select-item');
        selector = parent.find('select')
        delArray = $(this).parent().attr('data-value');

        // set temp array from selector menu
        selectVal = selector.val()

        toDel = selectVal.indexOf(delArray);

        // delete value from Multi selector Array value
        if (toDel != -1 || toDel != null) {

            // remove selected item from Array
            selectVal.splice(toDel, 1);

            // apply new array to selector value
            selector.val(selectVal);
            $(this).parent().remove();


            // remove class of selected
            selector.children('option[value="' + delArray + '"]').removeClass('selected');

            //  triger change for option
            selector.children('option[value="' + delArray + '"]').trigger('change');

        }

        //console.log(selectVal)

    })




    //-----------------------------------------------------
    // AJAX Request Complete , Redraw All selects
    //-----------------------------------------------------
    $(document).ajaxComplete(function () {





        $(targetElement).each(function () {

            var elemParent = $(this).parent($('div'));
            var masterParent = $(this).parents('.select-item')
            $('label.selected-option', masterParent).remove();


            redrawOption($(this), elemParent, false);



        })

        $('label.selected-option').each(function () {
            if ($(this).parent().find('label').length > 1) {
                $(this).remove()
            }
        })

    })




    //------------------------------------------------------
    // search filter 
    //-----------------------------------------------------

    function searchList(inputVal, textbox) {
        /*
        UL list container where search gonna be performed
        */

        // jQuery Method, slower for large list
        // var item = $(textbox).parent().next()
        // var itemList = item.find('option');

        // var result = item.find('.note-list');

        // $('option', item).not(':Contains('+ inputVal +')').hide().addClass('not-found');
        var selectItem = $(textbox).parent().next().attr('id');

        listItem = document.getElementById(selectItem).children;
        listLength = listItem.length;
        reg = new RegExp(inputVal, 'i');

        // for ( item in listItem ) {
        // 	mm = reg.test(listItem[item].innerHTML)
        // 	if( mm != true ) {
        // 		listItem[item].setAttribute('class','not-found');
        // 	}
        // }


        //console.time("for")
        for (i = 0; i < listLength; i++) {

            txtVal = reg.test(listItem[i].innerHTML)

            if (txtVal != true) {
                //listItem[i].setAttribute('class','not-found');
                listItem[i].className = listItem[i].className + " not-found";
            }

        }

        //console.timeEnd('for')





    } // searchList(inputVal, textbox )



    //------------------------
    // Bind Keys
    //------------------------
    $('.contentDiv, #mydialog').on('keyup', '.filter input', function (key) {

        var textbox = $(this);
        var itemList = $(this).parent().next('select');

        if ($(textbox).val() == "") {
            $(textbox).val("");
            // $('label', result).html("");
            $('option', itemList).removeClass('not-found');
        }

        else if (key.keyCode == 27) {
            hideBox(textbox.parents('.select-cont'));
        }


        if (key.keyCode == 13) {
            textbox.next('.btn-find').trigger('click');
            //console.log("enter pressed")
        }


    });







    // Redraw , added for ... when ajax is referenshed etc
    function redrawOption(element, elemParent, mode) {

        // mode to check its frist time or redrawing eleemnts = true : redraw

        var mode = mode || false;

        var masterParent = elemParent.parents('.select-box');
        // console.log(masterParent)
        listSize = $('option', element).length;

        // set the size of option List
        if (listSize < 8) {

            $(element).attr('size', listSize);

        } else {

            $(element).attr('size', 12);
        }

        var selectedItem = $('option:selected', element);


        // ADD SELECT OPTION title if nothing exist
        if ((selectedItem.length < 1 /* && mode == true */) || $(element).val() == null) {

            // Remove tags of selected item first / multi select.
            $('label.sel-item-tag', masterParent).remove();

            $('<label class="selected-option">Please Select</label>').insertBefore($('.select-arrow', masterParent));

        } else {

            // IF SEelct is multiple then apply tag style 
            if (multiSelect) {
                $('label.sel-item-tag', masterParent).remove();
                var selectedItemTag = "";
                $(selectedItem).each(function (i) {

                    //var _dataIndex = $(this).attr('data-index')
                    var _text = $(this).text();
                    var _dataValue = $(this).attr('value');

                    selectedItemTag += "<label class='sel-item-tag' data-value=" + _dataValue + " title='" + _text + "'><span>" + _text + "</span><span class='del-item'></span></label>";

                })

                $(selectedItemTag).insertBefore($('.select-arrow', masterParent));


            } else {

                $('<label class="selected-option">Please Select</label>').insertBefore($('.select-arrow', masterParent));

                $(element).parents('.select-box').find('label.selected-option').html(selectedItem.html());

            }

        } // master if/else


        // Add Class to Selected Item
        selectedItem.addClass('selected');
        //selectedItem.add(":visible:even").addClass('even')



    } // redraw ends -----------------





    //-------------------------------------------
    // Redraw selector 
    // ------------------------------------------


    function redraw(elementSelect) {
        var elementTarget = $(elementSelect).parents().find('.select-box');
        var elemParent = $(elementSelect).parent($('div'));
        var childElements = $(elemParent).find('.opt-list').empty();

        redrawOption(elementSelect, elemParent);

    }

    // ------------------------------------------


    // repeated hider
    function hideBox(element) {

        $(element).fadeOut(50, function () {

            $('option', element).removeClass('not-found')
            //$(this).find('ul li').removeClass('found').removeAttr('style');
            $('.note-list label').empty();
            $('input', element).val("");

            $(element).parent().prev('.bg-overlay').hide()

        })

        $('.select-arrow').removeClass('close');
    }



    // $(document).click(function(e) {
    //     var target = e.target;

    //     if (!$(target).is('.select-cont') && !$(target).parents().is('.select-cont')) {
    //         hideBox(  $('.select-cont') )
    //         $('.select-cont').removeClass('not-found');
    //     }

    // });


    $('.contentDiv,#mydialog').on('click', '.bg-overlay', function () {
        $(this).hide();
        hideBox($(this).next().find('.select-cont'));

    })







    // custom animted show 
    jQuery.fn.showMe = function (speed, callback) {
        var durSpeed = speed || 250;

        $(this).css({
            'margin-top': -50,
            'opacity': 0
        }).show();

        $(this).animate({

            'margin-top': 0,
            'opacity': 1

        }, {
            duration: durSpeed,
            queue: false
        })

    }


    //    $( document ).ajaxSuccess(function( event, request, settings ) {
    // 	prettyInput($('select'))
    // });






    jQuery.expr[':'].Contains = function (a, i, m) {
        return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };



}); //Jquery