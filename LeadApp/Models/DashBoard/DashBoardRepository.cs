﻿using LeadApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Models
{
    public class DashBoardRepository
    {
        private LeadAppEntities _context = new LeadAppEntities();
        public DashBoardVM DashBoardReport(int GroupId)
        {
            DashBoardVM dashboardvm = new DashBoardVM();
            if (GroupId == 5)
            {

                DateTime startDate = DateTime.Today.Date.AddDays(-(int)DateTime.Today.DayOfWeek).AddDays(1);
                DateTime endDate = startDate.AddDays(5);
                // var leadphones = _context.LeadPhones.Where(q => q.IsDeleted == false).ToList();
                var leadphones = _context.LeadPhones.Where(q => q.LeadPhoneFollowUps.Any(c => c.FollowedUpDate >= startDate && c.FollowedUpDate <= endDate && q.IsDeleted == false)).ToList();
              
                dashboardvm.TotalLeads = leadphones.Count();
                dashboardvm.TotalWeeklyLeadsApproved = (from c in leadphones
                                                        where c.LeadPhoneFollowUps.Any(cc => cc.StatusId == 10 && cc.FollowedUpDate >= startDate && cc.FollowedUpDate <= endDate)
                                                        select c.LeadPhoneId).Count();
                dashboardvm.TotalweeklyLeadsRejected = (from c in leadphones
                                                        where c.LeadPhoneFollowUps.Any(cc => cc.StatusId == 7 && cc.FollowedUpDate >= startDate && cc.FollowedUpDate <= endDate)
                                                        select c.LeadPhoneId).Count();
                dashboardvm.Totalweeklyearning = (from c in leadphones
                                                  where c.LeadPhoneFollowUps.Any(cc => cc.StatusId == 10)
                                                  select c.AmountAccepted).Sum();

                dashboardvm.TotalPercentageLeadsApproved = dashboardvm.TotalWeeklyLeadsApproved > 0 ? Math.Round(((decimal)dashboardvm.TotalWeeklyLeadsApproved / dashboardvm.TotalLeads) * 100, 2) : 0;
                dashboardvm.TotalPercentageLeadsRejected = dashboardvm.TotalweeklyLeadsRejected > 0 ? Math.Round(((decimal)dashboardvm.TotalweeklyLeadsRejected / dashboardvm.TotalLeads) * 100, 2) : 0;

                var agentinfo = (from cc in leadphones
                                 where cc.LeadPhoneFollowUps.Any(c => c.FollowedUpDate >= startDate && c.FollowedUpDate <= endDate)
                                 where cc.AmountAccepted != null
                                 group cc by cc.CreatedBy into g
                                 select new AgentCommsion
                                 {
                                     AgentId = g.Key,
                                     AgentName = g.FirstOrDefault().AppUser.UserName,
                                     AgentAcceptedOffers = g.Count(),
                                     AgentCallsmade = leadphones.Where(q => q.CreatedBy == g.Key && q.LeadPhoneFollowUps.Any(c => c.FollowedUpDate >= startDate && c.FollowedUpDate <= endDate)).Count(),
                                     AgentSales = g.Sum(q => q.AmountAccepted)

                                 }).ToList().Select(query => new AgentCommsion()
                                 {
                                     AgentId = query.AgentId,
                                     AgentName = query.AgentName,
                                     AgentAcceptedOffers = query.AgentAcceptedOffers,
                                     AgentSales = query.AgentSales,
                                     AgentCallsmade = query.AgentCallsmade,
                                     KPI = query.AgentAcceptedOffers > 0 ? Math.Round((((decimal)query.AgentAcceptedOffers / query.AgentCallsmade) * 100), 2) : 0


                                 }).ToList();

                dashboardvm.AgentInfo = agentinfo;
            }
            else if(GroupId==6)
            {
                DateTime startDate = DateTime.Today.Date.AddDays(-(int)DateTime.Today.DayOfWeek).AddDays(1);
                DateTime endDate = startDate.AddDays(5);
                // var leadphones = _context.LeadPhones.Where(q => q.IsDeleted == false).ToList();
                var leadphones = _context.LeadPhones.Where(q => q.LeadPhoneFollowUps.Any(c => c.FollowedUpDate >= startDate && c.FollowedUpDate <= endDate && q.IsDeleted == false && q.CreatedBy==Auth.UserID)).ToList();
              
                dashboardvm.TotalLeads = leadphones.Count();
                dashboardvm.TotalWeeklyLeadsApproved = (from c in leadphones
                                                        where c.LeadPhoneFollowUps.Any(cc => cc.StatusId == 10)
                                                        select c.LeadPhoneId).Count();
                dashboardvm.TotalweeklyLeadsRejected = (from c in leadphones
                                                        where c.LeadPhoneFollowUps.Any(cc => cc.StatusId == 7)
                                                        select c.LeadPhoneId).Count();
                dashboardvm.Totalweeklyearning = (from c in leadphones
                                                  where c.LeadPhoneFollowUps.Any(cc => cc.StatusId == 10)
                                                  select c.AmountAccepted).Sum();

                dashboardvm.TotalPercentageLeadsApproved = dashboardvm.TotalWeeklyLeadsApproved > 0 ? Math.Round(((decimal)dashboardvm.TotalWeeklyLeadsApproved / dashboardvm.TotalLeads) * 100, 2) : 0;
                dashboardvm.TotalPercentageLeadsRejected = dashboardvm.TotalweeklyLeadsRejected > 0 ? Math.Round(((decimal)dashboardvm.TotalweeklyLeadsRejected / dashboardvm.TotalLeads) * 100, 2) : 0;

                var agentinfo = (from cc in leadphones
                                 where cc.LeadPhoneFollowUps.Any(c => c.FollowedUpDate >= startDate && c.FollowedUpDate <= endDate)
                                 where cc.AmountAccepted != null
                                 group cc by cc.CreatedBy into g
                                 select new AgentCommsion
                                 {
                                     AgentId = g.Key,
                                     AgentName = g.FirstOrDefault().AppUser.UserName,
                                     AgentAcceptedOffers = g.Count(),
                                     AgentCallsmade = leadphones.Where(q => q.CreatedBy == g.Key && q.LeadPhoneFollowUps.Any(c => c.FollowedUpDate >= startDate && c.FollowedUpDate <= endDate)).Count(),
                                     AgentSales = g.Sum(q => q.AmountAccepted)

                                 }).ToList().Select(query => new AgentCommsion()
                                 {
                                     AgentId = query.AgentId,
                                     AgentName = query.AgentName,
                                     AgentAcceptedOffers = query.AgentAcceptedOffers,
                                     AgentSales = query.AgentSales,
                                     AgentCallsmade = query.AgentCallsmade,
                                     KPI = query.AgentAcceptedOffers > 0 ? Math.Round((((decimal)query.AgentAcceptedOffers / query.AgentCallsmade) * 100), 2) : 0


                                 }).ToList();

                dashboardvm.AgentInfo = agentinfo;
            }

            return dashboardvm;
   
        }

    

        public ReportVM ReportVM(DateTime? StartDate, DateTime? EndDate)
        {
            ReportVM reportvm = new ReportVM();
            if (StartDate != null && EndDate != null)
            {
                DateTime startDate = Convert.ToDateTime(StartDate);
                DateTime endDate = Convert.ToDateTime(EndDate);

                var leadphones = _context.LeadPhones.Where(q => q.LeadPhoneFollowUps.Any(c => c.FollowedUpDate >= startDate && c.FollowedUpDate <= EndDate && q.IsDeleted == false)).ToList();              
                reportvm.CompletedOffers = (from cc in leadphones
                                            where cc.IsCompleted == true
                                            select cc.LeadPhoneId).Count();
                reportvm.CurrentGrossSales = (from cc in leadphones
                                              where cc.IsCompleted == true && cc.AmountAccepted!=null
                                              select cc.AmountAccepted).Sum();
                reportvm.CurrentAcceptedOffers = (from cc in leadphones
                                                  where cc.AmountAccepted != null
                                                  select cc.LeadPhoneId).Count();
                reportvm.CurrentMadeOffers = (from cc in leadphones
                                              where cc.StatusId==4
                                              select cc.LeadPhoneId).Count();
                reportvm.CurrentCalls = (from cc in leadphones
                                         select cc.LeadPhoneId).Count();

                var commisiontable = _context.LeadPhoneCommisionIDs.ToList();
                /////// accepted offers information
                var agentcommision = (from cc in leadphones
                                      where cc.AmountAccepted != null
                                      group cc by cc.CreatedBy into g
                                      select new AgentCommsion
                                      {
                                          AgentId = g.Key,
                                          AgentName = g.FirstOrDefault().AppUser.UserName,
                                          AgentAcceptedOffers = g.Count(),
                                          AgentCallsmade = leadphones.Where(q=>q.CreatedBy==g.Key).Count(),
                                          AgentSales = g.Sum(q => q.AmountAccepted)                                          
                                      }).ToList().Select(query => new AgentCommsion()
                                      {
                                          Tiers = query.AgentAcceptedOffers > 0 ? commisiontable.Where(x => query.AgentAcceptedOffers >= x.StartNo && query.AgentAcceptedOffers <= x.EndNo).FirstOrDefault().LeadTiers : null,
                                          AgentId = query.AgentId,
                                          AgentName = query.AgentName,
                                          AgentAcceptedOffers = query.AgentAcceptedOffers,
                                          AgentSales = query.AgentSales,
                                          AgentCallsmade = query.AgentCallsmade,
                                          AgentComm = query.AgentAcceptedOffers > 0 ? ((commisiontable.Where(x => query.AgentAcceptedOffers >= x.StartNo && query.AgentAcceptedOffers <= x.EndNo).FirstOrDefault().Percentage / 100) * query.AgentSales) : 0,
                                          AgentBonus = query.AgentAcceptedOffers >0 ? commisiontable.Where(x => query.AgentAcceptedOffers >= x.StartNo && query.AgentAcceptedOffers <= x.EndNo).FirstOrDefault().Bonus : 0

                                      }).ToList();

                reportvm.AgenctCommision = agentcommision;
                // on every tier commision,accepted tiers and calls made
                reportvm.TierCommission = (from ccc in agentcommision
                                           group ccc by ccc.Tiers into g
                                           select new TiersCommision
                                           {
                                               Tier = g.Key,
                                               PerTierCommision = g.Sum(q => q.AgentComm),
                                               PerTierAcceptedOffers = g.Sum(q => q.AgentAcceptedOffers),
                                               PerTierCallsMade=g.Sum(q=>q.AgentCallsmade),
                                               PerTierBonusAmount= g.Sum(q=>q.AgentBonus)

                                           }).ToList();

                reportvm.PerAgentRecord = (from c in leadphones
                                          select new AgentCommsion
                                          {                                             
                                              AgentName = c.AppUser.UserName
                                          

                              }).ToList();
            }

            return reportvm;
        }
    }
}