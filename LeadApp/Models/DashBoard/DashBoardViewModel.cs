﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeadApp.Models
{

  public class ReportVM
  {
       public int CompletedOffers { get; set; }
       public decimal? CurrentGrossSales { get; set; }       
       public int CurrentCalls { get; set; }       
       public int CurrentMadeOffers { get; set; }       
       public int CurrentAcceptedOffers { get; set; }           
       public List<AgentCommsion> AgenctCommision { get; set; }
       public List<TiersCommision> TierCommission { get; set; }       
       public List<AgentCommsion> PerAgentRecord { get; set; }
    }

    public class AgentCommsion
    {
        public int? AgentId { get; set; }
        public string AgentName { get; set; }

        public decimal KPI { get; set; }

        public DateTime FollowUpDate { get; set; }

        public int AgentCallsmade { get; set; }

     

        public decimal? AgentComm { get; set; }

        public decimal? AgentSales { get; set; }

        public string Tiers { get; set; }

        public int AgentAcceptedOffers { get; set; }
        
        public decimal? CommissionPercentage { get; set; }    
        public decimal? AgentBonus { get; set; }
    }

    public class TiersCommision
    {
        public string Tier { get; set; }
        public decimal? PerTierCommision { get; set; }

        public int PerTierCallsMade { get; set; }

        public int PerTierAcceptedOffers { get; set; }

        public decimal? PerTierBonusAmount { get; set; }

       
    }

   


    public class DashBoardVM
    {
      public int TotalLeads { get; set; }
      public int TotalWeeklyLeadsApproved { get; set; }
      public int TotalweeklyLeadsRejected { get; set; }
      public decimal? Totalweeklyearning { get; set; }

      public decimal TotalPercentageLeadsApproved { get; set; }

      public decimal TotalPercentageLeadsRejected { get; set; }
      public List<AgentCommsion> AgentInfo { get; set; }
    }
  

  public class AgentPerformance
  {
      public int AgentId { get; set; }

      public string AgentName { get; set; }
      public int TotalLeads { get; set; }
      public int LeadsApproved { get; set; }

      public decimal PercentageApproved { get; set; }

   


  } 
}
