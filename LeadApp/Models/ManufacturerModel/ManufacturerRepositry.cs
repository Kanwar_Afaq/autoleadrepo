﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using LeadApp.Models;
using System.Web.Mvc;
using System.Xml;
using System.Linq.Expressions;
using LeadApp.Helpers;
using System.Data.Entity.Validation;

namespace LeadApp.Models {
	public class ManufacturerRepositry
	{
		private LeadAppEntities _context = new LeadAppEntities();

		public IEnumerable<Manufacturer> Get()
		{
			return _context.Manufacturers;
		}

		public bool Add(Manufacturer _data, out string error)
		{
			error = "";

			try {

				_data.CreatedBy = Auth.UserID;
				_data.CreatedDate = System.DateTime.Now;
				_context.Manufacturers.Add(_data);

				_context.SaveChanges();

				return true;
			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}

		public bool Edit(Manufacturer _data, out string error)
		{
			error = "";

			try
			{
				Manufacturer _setData = _context.Manufacturers.Where(e => e.ManufactureId == _data.ManufactureId).SingleOrDefault();
				if (_setData != null)
				{
					_setData.Name = _data.Name;

					_setData.ModifiedBy = Auth.UserID;
					_setData.ModifiedDate = System.DateTime.Now;


					//_context.Entry(_setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}

			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}


		public bool Delete(int id, out string error) {
			error = "";
			try {
				Manufacturer DTask = _context.Manufacturers.Where(s => s.ManufactureId == id).SingleOrDefault();
				_context.Manufacturers.Remove(DTask);
				_context.SaveChanges();

					return true;

			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}

		}


		public bool isExist(string Name, int? Id)
		{
			bool isAvail = false;
			int dbID = Id.ConvertToInt();
			try
			{
				isAvail = _context.Manufacturers.Any(m => m.Name == Name);
				if (Id.IsNumeric())
					isAvail = _context.Manufacturers.Any(m => m.Name == Name && m.ManufactureId != dbID);
			}
			catch { }

			return isAvail;
		}



	}
}