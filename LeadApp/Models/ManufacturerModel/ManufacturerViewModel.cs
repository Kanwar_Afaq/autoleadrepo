﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Models
{
	[MetadataType(typeof(ManufacturerMetaData))]
	public partial class Manufacturer
	{

	}

	public class ManufacturerMetaData
	{
		//[Display(Name = "Vehicle Make")]
		[Required]
		[Remote("isExist", "Manufacturer", AdditionalFields = "ManufactureId", ErrorMessage = "Manufacturer already exist")]
		[StringLength(100, ErrorMessage = "Must be between 3 and 100 characters", MinimumLength = 3)]
		public string Name { get; set; }
	}
}