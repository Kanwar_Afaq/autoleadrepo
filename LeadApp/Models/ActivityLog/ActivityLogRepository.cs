﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using LeadApp.Models;
using System.Xml;
using LeadApp.Services;
using LeadApp.Helpers;
using System.Data.Entity.Validation;


namespace LeadApp.Models
{
    public class ActivityLogRepository
    {
        LeadAppEntities _context = new LeadAppEntities();

        public IEnumerable<ActivityLog> GetActivityLog()
        {
            return _context.ActivityLogs;
        }
    }
}