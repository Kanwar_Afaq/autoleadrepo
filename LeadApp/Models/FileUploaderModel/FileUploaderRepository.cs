﻿using LeadApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Models
{
    public class FileUploaderRepository
    {
        LeadAppEntities _context = new LeadAppEntities();

        public List<FileViewModel> GetFileAttachments(Guid Referenceguid)
        {
            var query = (from f in _context.FileAttachments
                         where f.ReferenceGUID == Referenceguid
                         select new FileViewModel
                         {
                             FileAttachementId = f.FileAttachmentGUID,
                             FileSource = f.FileSource,
                             FilePermission = f.FilePermission,
                             FilePermissionIds=f.FilePermissionIds,
                             FileTitle = f.FileTitle,
                             IsDisable = f.isDisable,
							 FileLink = (f.FilePermissionIds.Contains(Auth.GroupID.ToString())) ? WebConstants.LogicalPath + WebConstants.FileFinalUploadFolder + f.FileSource : "#"
                         }).ToList();
            return query;
        }

        public List<FileViewModel> GetFileAttachments()
        {
            var query = (from f in _context.FileAttachments
                         select new FileViewModel
                         {
                             FileAttachementId = f.FileAttachmentGUID,
                             FileSource = f.FileSource,
                             FilePermissionIds=f.FilePermissionIds,
                             FilePermission = f.FilePermission,
                             FileTitle = f.FileTitle,
                             IsDisable = f.isDisable
                         }).ToList();
            return query;
        }

        public void AddFileAttacments(FileViewModel fileuploader)
        {
            FileAttachment fileattachment = new FileAttachment();
            fileattachment.FileAttachmentGUID = Guid.NewGuid();
            fileattachment.ReferenceGUID = fileuploader.ReferenceGUID;
            fileattachment.FilePermissionIds = fileuploader.FilePermissionIds;
            fileattachment.FileTitle = fileuploader.FileTitle;
            fileattachment.FileSource = fileuploader.FileSource;
			fileattachment.CreatedBy = Auth.UserID;
            fileattachment.CreatedDate = DateTime.Now;
            fileattachment.FilePermission = fileuploader.FilePermission;
            fileattachment.isDisable = false;

            _context.FileAttachments.Add(fileattachment);
            _context.SaveChanges();

        }


        public void UpdateAttachment(Guid Id, FileViewModel fileuploader)
        {
            var fileattachment = _context.FileAttachments.FirstOrDefault(m => m.isDisable == false);
            fileattachment.FileTitle = fileuploader.FileTitle;
            fileattachment.FileSource = fileuploader.FileSource;
            fileattachment.FilePermissionIds = fileuploader.FilePermissionIds;
            fileattachment.FilePermission = fileuploader.FilePermission;
            fileattachment.isDisable = false;
			fileattachment.ModifiedBy = Auth.UserID;
            fileattachment.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
        }

        public void DeleteAttachment(Guid Id)
        {
            var fileattachment = _context.FileAttachments.Where(m => m.ReferenceGUID == Id && m.isDisable == false);
            _context.FileAttachments.RemoveRange(fileattachment);
            _context.SaveChanges();
        }




    }
}