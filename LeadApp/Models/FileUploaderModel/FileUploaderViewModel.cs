﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Models
{
    public class FileViewModel
    {
        public Guid FileAttachementId { get; set; }
        public Guid ReferenceGUID { get; set; }
        public Guid? FKGUID { get; set; }
        public bool inclEmail { get; set; }  
        public string FileTitle { get; set; }
        public string RecordId { get; set; }
        public string FileSource { get; set; }
        public string FileLink { get; set; }
        public string FilePermission { get; set;}
        public string FilePermissionIds { get; set; }
        public bool IsDisable { get; set; }       
    }
}