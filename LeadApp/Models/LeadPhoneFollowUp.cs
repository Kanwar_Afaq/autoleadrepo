//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeadApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LeadPhoneFollowUp
    {
        public int LeadPhoneFollowUpId { get; set; }
        public Nullable<decimal> LatestOfferedAmount { get; set; }
        public Nullable<decimal> LatestAskingPrice { get; set; }
        public string LatestComment { get; set; }
        public string StatusText { get; set; }
        public int LeadPhoneId { get; set; }
        public int StatusId { get; set; }
        public bool IsFirstStatus { get; set; }
        public System.DateTime FollowedUpDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual AppUser AppUser { get; set; }
        public virtual LeadPhone LeadPhone { get; set; }
        public virtual Status Status { get; set; }
    }
}
