﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LeadApp.Helpers;

namespace LeadApp.Models
{
	[MetadataType(typeof(AppUserPwdRequestMetaData))]
	public partial class AppUserPwdRequest
	{
		[Display(Name = "User Name")]
		public string UserName { get; set; }

		[Display(Name = "Email")]
		public string Email { get; set; }

		[Display(Name = "Word Verification")]
		public string Captcha { get; set; }
		public bool ShowCaptcha { get; set; }


		[Display(Name = "New Password")]
		[DataType(DataType.Password)]
		public string Password { get; set; }
		[Display(Name = "Confirm New Password")]
		[DataType(DataType.Password)]
		public string Confirmpwd { get; set; }
	}
	public class AppUserPwdRequestMetaData
	{

	}




	[MetadataType(typeof(AppUserMetaData))]
	public partial class AppUser {
		
	}


	public class AppUserMetaData {
	//	[Remote("isUserExist", "Employee", AdditionalFields = "UserId", ErrorMessage = "User Name already exist")] 
        [Remote("isUserExist", "User", AdditionalFields = "UserId", ErrorMessage = "User Name already exist")] 
       	[Required]
		[StringLength(16, ErrorMessage = "Must be between 3 and 16 characters", MinimumLength = 3)]
		[Display(Name = "User Name")]
		public string UserName { get; set; }

		//[Required]
		//[StringLength(16, ErrorMessage = "Must be between 6 and 16 characters", MinimumLength = 6)]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[Display(Name = "Disable Account")]
		public bool isDisable { get; set; }

		[Display(Name = "Send Lead Notification")]
		public bool isNotify { get; set; }
	}


	public class LoginViewModel {
		[Required]
		[Display(Name = "User Name")]
		public string UserName { get; set; }

		[Required]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[Display(Name = "Remember Me")]
		public bool isRemember { get; set; }


		[Display(Name = "Word Verification")]
		public string Captcha { get; set; }
		public bool ShowCaptcha { get; set; }

		public string returnURL { get; set; }

		public int UserId { get; set; }
		public System.Guid UserGUID { get; set; }
		public bool isEmployee { get; set; }
		public int GroupID { get; set; }
		public string Email { get; set; }
	}



	[MetadataType(typeof(AppEmployeeMetaData))]
	public partial class AppEmployee {

		[Display(Name = "Email Account Information")]
		public bool isEmailSend { get; set; }

		//[Required]
		[Display(Name = "Client Department Access")]
		public List<int> PartyDepartments { get; set; }
	}

	public class AppEmployeeMetaData {
		[Required]
		[Display(Name = "Name")]
		public string ContactName { get; set; }

		[Remote("isUserEmailExist", "User", AdditionalFields = "UserId", ErrorMessage = "Email already exist")]
		[Required]
		[DataType(DataType.EmailAddress)]
		[EmailAddress]
		public string Email { get; set; }
		
		[DataType(DataType.EmailAddress)]
		[EmailAddress]
		[Display(Name = "Personal Email")]
		public string PersonalEmail { get; set; }
		
		[Display(Name = "Mobile No.")]
		public string MobileNo { get; set; }
		[Display(Name = "Tel No.")]
		public string TelNo { get; set; }
		[Display(Name = "Tel No. Home")]
		public string TelNoHome { get; set; }
		[Display(Name = "Fax No.")]
		public string FaxNo { get; set; }
		[Display(Name = "Fax No. Home")]
		public string FaxNoHome { get; set; }
		public string Designation { get; set; }
		public string Department { get; set; }
		[Display(Name = "Yahoo ID")]
		public string YahooID { get; set; }
		[Display(Name = "Skype ID")]
		public string SkypeID { get; set; }
		[Display(Name = "BBM Code")]
		public string BBMCode { get; set; }


		[AllowHtml]
		public string Signature { get; set; }
		[AllowHtml]
		public string Notes { get; set; }
	}

}