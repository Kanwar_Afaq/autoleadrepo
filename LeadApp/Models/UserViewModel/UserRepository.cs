﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using LeadApp.Helpers;
using LeadApp.Services;

namespace LeadApp.Models
{
    public class UserRepository
    {
        LeadAppEntities _context = new LeadAppEntities();




		public IEnumerable<AppEmployee> GetEmployee() {
			return _context.AppEmployees;
		}




		public bool isUserExist(string UserName, int UserId) {
			bool UserAuthentication = false;
			try {
				if (UserId > 0)
					UserAuthentication = _context.AppUsers.Any(m => m.UserName == UserName && m.UserId != UserId);
				else
					UserAuthentication = _context.AppUsers.Any(m => m.UserName == UserName);
			} catch { }

			return UserAuthentication;
		}
		public bool isUserEmailExist(string Email, int UserId)
		{
			bool UserAuthentication = false;
			try
			{
				if (UserId > 0)
					UserAuthentication = _context.AppEmployees.Any(m => m.Email == Email && m.UserId != UserId);
				else
					UserAuthentication = _context.AppEmployees.Any(m => m.Email == Email);
			}
			catch { }

			return UserAuthentication;
		}

		public bool Create(AppEmployee appemployee, out string error) {
			error = "";
			try {
				appemployee.CreatedBy = Auth.UserID;
				appemployee.CreatedDate = DateTime.Now;

				_context.AppEmployees.Add(appemployee);
				_context.SaveChanges();

				return true;

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}

        public bool CreateAppUser(AppEmployee appemployee, out string error)
        {
            error = "";
            try
            {
                appemployee.CreatedBy = Auth.UserID;
                appemployee.CreatedDate = DateTime.Now;

                _context.AppUsers.Add(appemployee.AppUser);
                _context.SaveChanges();

                return true;

            }
            catch (DbEntityValidationException e)
            {
                error = CommonHelpers.dbErrorDetail(e);
                return false;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }
        }

        public AppUser Get_UserDetail(string UserName)
        {

            return _context.AppUsers.Where(s => s.UserName == UserName).SingleOrDefault();

        }
		public bool EditProfile(AppEmployee getData, out string error) {
			error = "";
			try {
				var setData = _context.AppEmployees.FirstOrDefault(e => e.UserId == Auth.UserID);

				if (setData != null) {

					if (!string.IsNullOrEmpty(getData.AppUser.Password))
						setData.AppUser.Password = getData.AppUser.Password;


					setData.AppUser.isNotify = getData.AppUser.isNotify; 
					
					setData.ContactName = getData.ContactName;
					setData.MobileNo = getData.MobileNo;
					setData.Email = getData.Email;
					setData.PersonalEmail = getData.PersonalEmail;
					setData.TelNo = getData.TelNo;
					setData.TelNoHome = getData.TelNoHome;
					setData.FaxNo = getData.FaxNo;
					setData.FaxNoHome = getData.FaxNoHome;
					setData.Department = getData.Department;
					setData.Designation = getData.Designation;
					setData.YahooID = getData.YahooID;
					setData.SkypeID = getData.SkypeID;
					setData.BBMCode = getData.BBMCode;
					setData.Signature = getData.Signature;
					//setData.Notes = getData.Notes;
					setData.ModifiedDate = DateTime.Now;
					setData.ModifiedBy = Auth.UserID;


					_context.Entry(setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;

				} else {
					error = "ID Not Valid";
					return false;
				}

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}
		public bool Edit(Guid id, AppEmployee getData, out string error) {
			error = "";
			try {
				var setData = _context.AppEmployees.FirstOrDefault(e => e.EmployeeID == id);

				if (setData != null) {
					//_context.AppEmployeeDepartments.RemoveRange(setData.AppEmployeeDepartments);

					if (!string.IsNullOrEmpty(getData.AppUser.Password))
						setData.AppUser.Password = getData.AppUser.Password;

					//setData.AppEmployeeDepartments = getData.AppEmployeeDepartments;

					setData.AppUser.GroupID = getData.AppUser.GroupID;
					setData.AppUser.isNotify = getData.AppUser.isNotify;

					setData.ContactName = getData.ContactName;
					setData.MobileNo = getData.MobileNo;
					setData.Email = getData.Email;
					setData.PersonalEmail = getData.PersonalEmail;
					setData.TelNo = getData.TelNo;
					setData.TelNoHome = getData.TelNoHome;
					setData.FaxNo = getData.FaxNo;
					setData.FaxNoHome = getData.FaxNoHome;
					setData.Department = getData.Department;
					setData.Designation = getData.Designation;
					setData.YahooID = getData.YahooID;
					setData.SkypeID = getData.SkypeID;
					setData.BBMCode = getData.BBMCode;
					setData.Signature = getData.Signature;
					setData.Notes = getData.Notes;
					setData.ModifiedDate = DateTime.Now;
					setData.ModifiedBy = Auth.UserID;


					_context.Entry(setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				
				} else {
					error = "ID Not Valid";
					return false;
				}

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}
		public bool Delete(Guid id, out string error) {
			error = "";
			try {
				AppEmployee emp = _context.AppEmployees.FirstOrDefault(e => e.EmployeeID == id);

				if (emp != null) {
					_context.AppUserLogs.RemoveRange(emp.AppUser.AppUserLogs);
					_context.AppUserMetas.RemoveRange(emp.AppUser.AppUserMetas);
					_context.AppUsers.Remove(emp.AppUser);

					//_context.AppEmployeeDepartments.RemoveRange(emp.AppEmployeeDepartments);
					_context.AppEmployees.Remove(emp);	

					_context.SaveChanges();

					return true;
				
				} else {
					error = "ID Not Valid";
					return false;
				}
			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}



        public bool UserAuthentication(string UserName, string Password)
        {
            var UserAuthentication = _context.AppUsers.Any(m => m.UserName == UserName && m.Password == Password && m.isActive == true);
            if (UserAuthentication ==true)
                return true;
            else
                return false;
        }


		public long UserLoginLog(int UserID) {
			//Update user login count and last logon date
			AppUser objuse = _context.AppUsers.SingleOrDefault(e => e.UserId == UserID);
			int newVal = (objuse == null) ? 0 : Convert.ToInt32(objuse.LoginCount) + 1;
			objuse.LoginCount = newVal;
			objuse.LastLogn = DateTime.Now;

			//Insert user information in userlog
			AppUserLog objulog = new AppUserLog();
			objulog.LoginDateTime = DateTime.Now;
			objulog.UserId = UserID;
			objulog.Browser = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version;
			objulog.IPAddress = HttpContext.Current.Request.UserHostAddress;
			//objulog.SessionID = HttpContext.Current.Session.SessionID;
			_context.AppUserLogs.Add(objulog);
			_context.SaveChanges();

			return objulog.UserLogID;
		}
		public void InsertUserLogoutLog() {
			AppUserLog obju = _context.AppUserLogs.SingleOrDefault(e => e.UserLogID == Auth.UserLogID);
			if (obju != null) {
				obju.LogOutDateTime = DateTime.Now;
				_context.SaveChanges();
			}
		}		
		
		public IEnumerable<AppUser> GetUserDetail() {
			var qry = from U in _context.AppUsers
					  join UG in _context.AppGroups on U.GroupID equals UG.GroupId
					  select U;	
			return qry;
		}
		public IEnumerable<AppUserMeta> GetUserMeta(int UserID = 0) {
			if (UserID == 0)
				UserID = Auth.UserID;

			var qry = from U in _context.AppUsers
					  join M in _context.AppUserMetas on U.UserId equals M.UserId
					  where U.UserId == UserID
					  select M;
			return qry.ToList();
		}



		public bool AddMetaItem(AppUserMeta MetaItems, out string error) {
			error = "";
			try {
				//Delete Old Record of Same Meta Keys
				_context.AppUserMetas.RemoveRange(_context.AppUserMetas.Where(e=> MetaItems.MetaKey.Contains(e.MetaKey)));

				_context.AppUserMetas.Add(MetaItems);
				_context.SaveChanges();
				return true;

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}


		public int Forgot(int UserId, string RandomNo)
		{
			try
			{
				AppUserPwdRequest respwd = new AppUserPwdRequest();
				respwd.UserId = UserId;				
				respwd.RandomNo = RandomNo;
				respwd.Browser = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version;
				respwd.IPAddress = HttpContext.Current.Request.UserHostAddress;
				respwd.isGenerated = false;
				respwd.CreatedDate = DateTime.Now;

				_context.AppUserPwdRequests.Add(respwd);
				_context.SaveChanges();

				return respwd.RequestId;
			}
			catch
			{
				return 0;
			}
		}

		public bool ResetPassword(int id, string Password)
		{
			AppUserPwdRequest obj = _context.AppUserPwdRequests.SingleOrDefault(e => e.RequestId == id && e.isGenerated == false);
			if (obj != null)
			{
				AppUser usrobj = _context.AppUsers.SingleOrDefault(c => c.UserId == obj.UserId);
				usrobj.Password = EncryptDecryptHelper.Encrypt(Password);
				usrobj.ModifiedDate = DateTime.Now;
				usrobj.ModifiedBy = null;

				obj.isGenerated = true;

				_context.SaveChanges();

				return true;
			}

			return false;
		}


		public List<AppEmployee> GetUserEmails(List<int> UserIds)
		{
			return (from u in _context.AppEmployees where UserIds.Contains(u.UserId) select u).ToList();
		}



		public List<string> GetUserEmails()
		{
			var qry = from u in _context.AppEmployees
					  where u.AppUser.isNotify == true
					  select u.Email;
			return qry.ToList();
		}
    }
}