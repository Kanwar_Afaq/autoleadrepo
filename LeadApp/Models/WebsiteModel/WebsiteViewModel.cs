﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Models
{
	[MetadataType(typeof(WebsiteMetaData))]
	public partial class Website
	{

	}

	public class WebsiteMetaData
	{
		[Required]
		[Remote("isExist", "Website", AdditionalFields = "WebId", ErrorMessage = "Website already exist")]
		[StringLength(100, ErrorMessage = "Must be between 3 and 100 characters", MinimumLength = 3)]
		public string Name { get; set; }
	}
}