﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using LeadApp.Models;
using System.Web.Mvc;
using System.Xml;
using System.Linq.Expressions;
using LeadApp.Helpers;
using System.Data.Entity.Validation;

namespace LeadApp.Models {
	public class WebsiteRepositry
	{
		private LeadAppEntities _context = new LeadAppEntities();

		public IEnumerable<Website> Get()
		{
			return _context.Websites;
		}

		public bool Add(Website _data, out string error)
		{
			error = "";

			try {
				_data.WebGUID = Guid.NewGuid();
				_data.CreatedBy = Auth.UserID;
				_data.CreatedDate = System.DateTime.Now;
				_context.Websites.Add(_data);

				_context.SaveChanges();

				return true;
			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}

		public bool Edit(Website _data, out string error)
		{
			error = "";

			try
			{
				Website _setData = _context.Websites.Where(e => e.WebId == _data.WebId).SingleOrDefault();
				if (_setData != null)
				{
					_setData.Name = _data.Name;
					_setData.Domain = _data.Domain;
					_setData.Email = _data.Email;

					_setData.ModifiedBy = Auth.UserID;
					_setData.ModifiedDate = System.DateTime.Now;


					//_context.Entry(_setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}

			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}


		public bool Delete(int id, out string error) {
			error = "";
			try {
				Website DTask = _context.Websites.Where(s => s.WebId == id).SingleOrDefault();
				_context.Websites.Remove(DTask);
				_context.SaveChanges();

					return true;

			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}

		}


		public bool isExist(string Name, int? Id)
		{
			bool isAvail = false;
			int dbID = Id.ConvertToInt();
			try
			{
				isAvail = _context.Websites.Any(m => m.Name == Name);
				if (Id.IsNumeric())
					isAvail = _context.Websites.Any(m => m.Name == Name && m.WebId != dbID);
			}
			catch { }

			return isAvail;
		}



	}
}