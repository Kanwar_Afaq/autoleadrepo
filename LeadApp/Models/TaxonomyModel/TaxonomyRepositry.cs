﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using LeadApp.Models;
using System.Web.Mvc;
using System.Xml;
using System.Linq.Expressions;
using LeadApp.Helpers;
using System.Data.Entity.Validation;

namespace LeadApp.Models {
	public class TaxonomyRepositry {
		private LeadAppEntities _context = new LeadAppEntities();

		public IEnumerable<Taxonomy> Get()
		{
			return _context.Taxonomies;
		}

		public IEnumerable<TaxonomyCategory> GetCategory()
		{
			return _context.TaxonomyCategories;
		}

		public bool Add(Taxonomy _data,  out string error)
		{
			error = "";

			try {

				_data.CreatedBy = Auth.UserID;
				_data.CreatedDate = System.DateTime.Now;
				_context.Taxonomies.Add(_data);

				_context.SaveChanges();

				return true;
			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}

		public bool Edit(Taxonomy _data, out string error)
		{
			error = "";

			try
			{
				Taxonomy _setData = _context.Taxonomies.Where(e => e.TaxonomyId == _data.TaxonomyId).SingleOrDefault();
				if (_setData != null)
				{

					_setData.ParentId = _data.ParentId;
					_setData.TaxonomyCatId = _data.TaxonomyCatId;
					_setData.Name = _data.Name;

					_setData.ModifiedBy = Auth.UserID;
					_setData.ModifiedDate = System.DateTime.Now;


					//_context.Entry(_setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}

			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}


		public bool Delete(int id, out string error) {
			error = "";
			try {
				Taxonomy DTask = _context.Taxonomies.Where(s => s.TaxonomyId == id).SingleOrDefault();
				_context.Taxonomies.Remove(DTask);
				_context.SaveChanges();

					return true;

			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}

		}


		public bool isExist(string Name, int CatId, int? Id)
		{
			bool isAvail = false;
			int dbID = Id.ConvertToInt();
			try
			{
				isAvail = _context.Taxonomies.Any(m => m.Name == Name && m.TaxonomyCatId == CatId);
				if (Id.IsNumeric())
					isAvail = _context.Taxonomies.Any(m => m.Name == Name && m.TaxonomyCatId == CatId && m.TaxonomyId != dbID);
			}
			catch { }

			return isAvail;
		}



	}
}