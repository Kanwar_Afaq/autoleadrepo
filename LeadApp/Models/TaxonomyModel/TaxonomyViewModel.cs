﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Models
{
	[MetadataType(typeof(TaxonomyMetaData))]
	public partial class Taxonomy
	{

	}

	public class TaxonomyMetaData
	{
		[Required]
		[Remote("isExist", "Taxonomy", AdditionalFields = "TaxonomyId,TaxonomyCatId", ErrorMessage = "Taxonomy already exist")]
		[StringLength(100, ErrorMessage = "Must be between 3 and 100 characters", MinimumLength = 3)]
		public string Name { get; set; }
	}
}