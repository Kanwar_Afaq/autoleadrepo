//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeadApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Taxonomy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Taxonomy()
        {
            this.Leads = new HashSet<Lead>();
            this.Leads1 = new HashSet<Lead>();
            this.Taxonomies1 = new HashSet<Taxonomy>();
        }
    
        public int TaxonomyId { get; set; }
        public Nullable<int> ParentId { get; set; }
        public int TaxonomyCatId { get; set; }
        public string Name { get; set; }
        public bool isDisabled { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lead> Leads { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lead> Leads1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Taxonomy> Taxonomies1 { get; set; }
        public virtual Taxonomy Taxonomy1 { get; set; }
        public virtual TaxonomyCategory TaxonomyCategory { get; set; }
    }
}
