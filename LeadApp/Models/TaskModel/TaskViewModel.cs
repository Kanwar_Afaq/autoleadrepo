﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Models
{
	[MetadataType(typeof(TaskMetaData))]
	public partial class Task
	{

		[DataType(DataType.DateTime), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy hh:mm tt}")]
		public System.DateTime LastReplyDate { get; set; }

	}

	public class TaskMetaData
	{

		[DataType(DataType.DateTime), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy hh:mm tt}")]
		public System.DateTime CreatedDate { get; set; }

		[AllowHtml]
		public string Description { get; set; }
	}

	[MetadataType(typeof(TaskResponseMetaData))]
	public partial class TaskRespons
	{
	}

	public class TaskResponseMetaData
	{
		[DataType(DataType.DateTime), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMM yyyy hh:mm tt}")]
		public System.DateTime ResponseDate { get; set; }

		[AllowHtml]
		public string Response { get; set; }
	}


}