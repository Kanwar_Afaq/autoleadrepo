﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using LeadApp.Models;
using System.Web.Mvc;
using System.Xml;
using System.Linq.Expressions;
using LeadApp.Helpers;
using System.Data.Entity.Validation;

namespace LeadApp.Models {
	public class TaskRepositry {
		private LeadAppEntities _context = new LeadAppEntities();

		public IEnumerable<Task> GetTasks()
		{
			return _context.Tasks;
		}


		public IEnumerable<Task> GetAllTasks() {
			var qry = _context.Tasks.Where(e => e.TaskAssignees.Any(x=>x.AssigneeUserID == Auth.UserID) || e.CreatedBy == Auth.UserID);

			return qry;

			//return (from u in _context.TaskAssignees
			//		join t in _context.Tasks on u.TaskID equals t.TaskID
			//		join pr in _context.AppEmployees on t.CreatedBy equals pr.UserId
			//		join c in _context.AppEmployees on u.AssigneeUserID equals c.UserId
			//		join ts in _context.ActivityStatuses on t.StatusID equals ts.StatusID
			//		join s in _context.ActivityStatuses on u.StatusID equals s.StatusID
			//		where t.CreatedBy == Auth.UserID || u.AssigneeUserID == Auth.UserID
			//		orderby t.CreatedDate descending
			//		select new TaskViewModel {
			//			TaskID = u.TaskID,
			//			DateDue = t.DateDue,
			//			Title = t.Title,
			//			Description = t.Description,
			//			StatusID = ts.StatusID,
			//			StatusName = ts.StatusName,
			//			isClosed = t.isClosed,
			//			CreatedBy = t.CreatedBy,
			//			AssigneeName = pr.ContactName,

			//			AssigneeUserID = u.AssigneeUserID,
			//			AssignToUsername = c.ContactName,
			//			AssigneeStatusID = u.StatusID,
			//			AssigneeStatusName = s.StatusName,

			//			TaskAssignees = _context.TaskAssignees.Where(e=>e.TaskID == u.TaskID).ToList()

			//			//,LastReplyDate = _context.TaskResponses.Where(e => e.TaskID == u.TaskID).LastOrDefault().ResponseDate					
			//		});

		}


		public bool CloseTask(Guid id, out string error) {
			error = "";
			Task data = _context.Tasks.Where(s => s.TaskID == id).SingleOrDefault();
			if (data != null) {
				if (data.CreatedBy != Auth.UserID) {
					error = "You can close your own task only";
					return false;

				} else {

					data.isClosed = true;
					data.StatusID = 5;
					_context.Entry(data).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();

					return true;

				}
			} else {
				error = "ID Not Valid";
				return false;
			}
		}
		public bool ChangeStatus(Guid id, string StatusName, out string error) {
			error = "";
			Task data = _context.Tasks.Where(s => s.TaskID == id).SingleOrDefault();
			if (data != null) {
				if (data.CreatedBy != Auth.UserID) {
					error = "You can change your own task only";
					return false;

				} else {
					var getStaus = _context.ActivityStatuses.Where(e => e.StatusName == StatusName).FirstOrDefault();

					if (getStaus == null) {
						error = "Status Not Valid";
						return false;

					} else {

						data.StatusID = getStaus.StatusID;
						_context.Entry(data).State = System.Data.Entity.EntityState.Modified;
						_context.SaveChanges();

						return true;
					}

				}
			} else {
				error = "ID Not Valid";
				return false;
			}
		}

		public bool AddTask(Task _data, List<int> AssigneeUserID, out string error) {
			error = "";

			try {
				_data.TaskID = Guid.NewGuid();


				List<TaskAssignee> _assigne = new List<TaskAssignee>();
				foreach (var item in AssigneeUserID)
				{
					TaskAssignee _Asigneedata = new TaskAssignee();
					_Asigneedata.TaskID = _data.TaskID;
					_Asigneedata.StatusID = _data.StatusID;
					_Asigneedata.AssigneeUserID = item;
					_assigne.Add(_Asigneedata);
				}
				_data.TaskAssignees = _assigne;


				_data.CreatedBy = Auth.UserID;
				_data.CreatedDate = System.DateTime.Now;
				_context.Tasks.Add(_data);

				_context.SaveChanges();

				return true;
			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}

		public bool EditTask(Task _data, List<int> AssigneeUserID, out string error)
		{
			error = "";

			try
			{
				Task _setData = _context.Tasks.Where(e => e.TaskID == _data.TaskID).SingleOrDefault();
				if (_setData != null)
				{
					_context.TaskAssignees.RemoveRange(_setData.TaskAssignees);

					List<TaskAssignee> _assigne = new List<TaskAssignee>();
					foreach (var item in AssigneeUserID)
					{
						TaskAssignee _Asigneedata = new TaskAssignee();
						_Asigneedata.TaskID = _data.TaskID;
						_Asigneedata.StatusID = _data.StatusID;
						_Asigneedata.AssigneeUserID = item;
						_assigne.Add(_Asigneedata);
					}
					//_data.TaskAssignees = _assigne;
					_setData.TaskAssignees = _assigne;
					

					_setData.Title = _data.Title;
					_setData.StatusID = _data.StatusID;
					_setData.DateDue = _data.DateDue;
					_setData.Description = _data.Description;

					_setData.ModifiedBy = Auth.UserID;
					_setData.ModifiedDate = System.DateTime.Now;


					//_context.Entry(_setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}

			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}



		public IEnumerable<ActivityStatus> GetStatus() {
			return _context.ActivityStatuses;
		}


		public bool AddReply(TaskRespons TaskData, out string error) {
			error = "";
			TaskRespons DatabaseTask = new TaskRespons();


			try {
				DatabaseTask.TaskID = TaskData.TaskID;
				DatabaseTask.StatusID = TaskData.StatusID;
				DatabaseTask.SubmitUserID = TaskData.SubmitUserID;
				DatabaseTask.Response = TaskData.Response;
                DatabaseTask.ResponseDate = System.DateTime.Now;

				_context.TaskResponses.Add(DatabaseTask);
				_context.SaveChanges();

				return true;

			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}

		public IEnumerable<TaskRespons> GetAllTaskReply(Guid id) {
			return _context.TaskResponses.Where(e => e.TaskID == id);

			//var Detail = (from u in _context.TaskResponses
			//			  join pr in _context.AppEmployees on u.SubmitUserID equals pr.UserId
			//			  join s in _context.ActivityStatuses on u.StatusID equals s.StatusID
			//			  where u.TaskID == id
			//			  select new TaskViewModel {
			//				  TaskID = u.TaskID,
			//				  AssigneeStatusID = u.StatusID,
			//				  AssigneeStatusName = s.StatusName,
			//				  AssigneeUserID = u.SubmitUserID,
			//				  AssigneeName = pr.ContactName,
			//				  Description = u.Response,
			//				  ResponseDate=(DateTime)u.ResponseDate,


			//			  }).ToList();

			//return Detail;

		}



		public List<int> TaskStackHolders(Guid id) {
			List<int> data = _context.TaskAssignees.Where(e => e.TaskID == id).Select(e => e.AssigneeUserID).ToList();
			data.Add(_context.Tasks.Where(e => e.TaskID == id).Select(e => e.CreatedBy).First());

			return data;
		}

		public bool DeleteTask(Guid id, out string error) {
			error = "";


			try {
				Task DTask = new Task();
				DTask = _context.Tasks.Where(s => s.TaskID == id).SingleOrDefault();

				if (DTask.CreatedBy != Auth.UserID) {
					error = "You can delete your own task only";
					return false;

				} else {


					_context.TaskResponses.RemoveRange(_context.TaskResponses.Where(s => s.TaskID == id));
					_context.TaskAssignees.RemoveRange(_context.TaskAssignees.Where(s => s.TaskID == id));
					//_context.SaveChanges();  

					_context.Tasks.Remove(DTask);
					_context.SaveChanges();

					return true;
				}

			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}

		}

	}
}