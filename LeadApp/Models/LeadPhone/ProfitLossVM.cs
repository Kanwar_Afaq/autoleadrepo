﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Models
{
    public class ProfitLossVM
    {
        public int LeadPhoneId { get; set; }
        public string Manufacturer { get; set; }
   
        public string Name { get; set; }
        public string Email { get; set; }
   
        public decimal? AmountAccepted { get; set; }
        public decimal? SalesPrice { get; set; }
    
        public string Status { get; set; }
        public DateTime? createdDate { get; set; }

     
    }
}