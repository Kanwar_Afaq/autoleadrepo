﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Models
{
    public class VarifyVM
    {
        public int LeadPhoneId { get; set; }
        public string Manufacturer { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }

        public decimal? AmountAccepted { get; set; }
        public decimal? SalesPrice { get; set; }
        public string PlaceOfPickups { get; set; }
        public string VehicleModel { get; set; }
        public int? VehicleYear { get; set; }
        public string city { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string VinNo { get; set; }
    }

}