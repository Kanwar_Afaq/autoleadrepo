﻿using LeadApp.Helpers;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace LeadApp.Models
{
    public class LeadPhoneRepository
    {
        private LeadAppEntities _context = new LeadAppEntities();
        public List<LeadPhoneVM> getByStatusId(int? StatusId)
        {
            var result = (from q in _context.LeadPhones
                          join p in _context.Status on q.StatusId equals p.StatusId
                          where q.IsDeleted == false && q.StatusId == StatusId

                          orderby q.LeadPhoneId descending
                          select new LeadPhoneVM
                          {
                              LeadPhoneId = q.LeadPhoneId,
                              Email = q.Email,
                              Name = q.FullName,
                              AskingPrice = q.InitialAmount,
                              Manufacturer = q.Manufacturer.Name,
                              VinNo = q.VinNo,
                              VehicleYear = q.VehicleYear,
                              Status = p.StatusName
                          }).ToList();
            return result;
        }
        public List<ProfitLossVM> getProfitLossStats()
        {
            var result = (from q in _context.LeadPhones

                          where q.IsCompleted == true && q.AmountAccepted != null

                          orderby q.LeadPhoneId descending
                          select new ProfitLossVM
                          {
                              LeadPhoneId = q.LeadPhoneId,
                              AmountAccepted = q.AmountAccepted,
                              SalesPrice = q.SalesPrice,
                              Email = q.Email,
                              Name = q.FullName,
                              createdDate = q.CreatedDate,

                              Manufacturer = q.Manufacturer.Name,


                              Status = q.SalesPrice - q.AmountAccepted > 0 ? "Profit" : q.SalesPrice == null ? "N/A" : "Loss"
                          }).ToList();
            return result;
        }
        public IEnumerable<VarifyVM>getVarifyLeads(int? statusId)
        {
            if(Auth.UserID>0 && Auth.GroupID==7)
            {
                var _data = (from q in _context.LeadPhones
                            
                             where q.IsDeleted == false && q.isVarified==true && q.StatusId==statusId
                             select new VarifyVM
                             {
                                 LeadPhoneId = q.LeadPhoneId,
                                 Email = q.Email,
                                 Name = q.FullName,
                                 AmountAccepted = q.AmountAccepted,
                                 Manufacturer = q.Manufacturer.Name,
                                 VinNo = q.VinNo,
                                 VehicleYear = q.VehicleYear,
                                
                             }).ToList();
                return _data;
            }
            return null;
        }
        public IEnumerable<LeadPhoneVM> GetByUserId(int? StatusId)
        {

            DateTime startDate = DateTime.Today.Date.AddDays(-(int)DateTime.Today.DayOfWeek).AddDays(1);
            DateTime endDate = startDate.AddDays(5);
            if (StatusId == 20)
            {
                if (Auth.GroupID == 5)
                {
                    return (from q in _context.LeadPhones
                         
                            where q.IsDeleted == false && q.LeadPhoneFollowUps.Any(p=>p.FollowedUpDate >= startDate && p.FollowedUpDate<= endDate)
                            select new LeadPhoneVM
                            {
                                LeadPhoneId = q.LeadPhoneId,
                                Email = q.Email,
                                Name = q.FullName,
                                AskingPrice = q.InitialAmount,
                                Manufacturer = q.Manufacturer.Name,
                                VinNo = q.VinNo,
                                VehicleYear = q.VehicleYear,
                              
                            }).ToList();
                }
                else if(Auth.GroupID==6)
                {
                    return (from q in _context.LeadPhones
                       
                            where q.IsDeleted == false  && q.CreatedBy==Auth.UserID && q.LeadPhoneFollowUps.Any(p => p.FollowedUpDate >= startDate && p.FollowedUpDate <= endDate)
                            select new LeadPhoneVM
                            {
                                LeadPhoneId = q.LeadPhoneId,
                                Email = q.Email,
                                Name = q.FullName,
                                AskingPrice = q.InitialAmount,
                                Manufacturer = q.Manufacturer.Name,
                                VinNo = q.VinNo,
                                VehicleYear = q.VehicleYear,
                              
                            }).ToList();
                }
            }
            else if (StatusId == 21)
            {
                if (Auth.GroupID == 5)
                {
                    return (from q in _context.LeadPhones
                         
                            where q.IsDeleted == false && q.LeadPhoneFollowUps.Any(p => p.FollowedUpDate >= startDate && p.FollowedUpDate <= endDate && p.StatusId==10)
                            select new LeadPhoneVM
                            {
                                LeadPhoneId = q.LeadPhoneId,
                                Email = q.Email,
                                Name = q.FullName,
                                AskingPrice = q.InitialAmount,
                                Manufacturer = q.Manufacturer.Name,
                                VinNo = q.VinNo,
                                VehicleYear = q.VehicleYear,
                             
                              
                            }).ToList();
                }
                else if (Auth.GroupID == 6)
                {
                    return (from q in _context.LeadPhones
                         
                            where q.IsDeleted == false && q.CreatedBy == Auth.UserID && q.LeadPhoneFollowUps.Any(p => p.FollowedUpDate >= startDate && p.FollowedUpDate <= endDate && q.StatusId==10)
                            select new LeadPhoneVM
                            {
                                LeadPhoneId = q.LeadPhoneId,
                                Email = q.Email,
                                Name = q.FullName,
                                AskingPrice = q.InitialAmount,
                                Manufacturer = q.Manufacturer.Name,
                                VinNo = q.VinNo,
                                VehicleYear = q.VehicleYear,
                              
                            }).ToList();
                }
            }
            else if (StatusId == 22)
            {
                if (Auth.GroupID == 5)
                {
                    return (from q in _context.LeadPhones
                           
                            where q.IsDeleted == false && q.LeadPhoneFollowUps.Any(p => p.FollowedUpDate >= startDate && p.FollowedUpDate <= endDate && q.StatusId == 7)
                            select new LeadPhoneVM
                            {
                                LeadPhoneId = q.LeadPhoneId,
                                Email = q.Email,
                                Name = q.FullName,
                                AskingPrice = q.InitialAmount,
                                Manufacturer = q.Manufacturer.Name,
                                VinNo = q.VinNo,
                                VehicleYear = q.VehicleYear,
                              
                            }).ToList();
                }
                else if (Auth.GroupID == 6)
                {
                    return (from q in _context.LeadPhones
                         
                            where q.IsDeleted == false && q.CreatedBy == Auth.UserID && q.LeadPhoneFollowUps.Any(p => p.FollowedUpDate >= startDate && p.FollowedUpDate <= endDate && q.StatusId == 7)
                            select new LeadPhoneVM
                            {
                                LeadPhoneId = q.LeadPhoneId,
                                Email = q.Email,
                                Name = q.FullName,
                                AskingPrice = q.InitialAmount,
                                Manufacturer = q.Manufacturer.Name,
                                VinNo = q.VinNo,
                                VehicleYear = q.VehicleYear,
                              
                            }).ToList();
                }
            }
            else
            {



                var _data = (from q in _context.LeadPhones
                             join p in _context.Status on q.StatusId equals p.StatusId
                             where q.IsDeleted == false
                             select new LeadPhoneVM
                             {
                                 LeadPhoneId = q.LeadPhoneId,
                                 Email = q.Email,
                                 Name = q.FullName,
                                 AskingPrice = q.InitialAmount,
                                 Manufacturer = q.Manufacturer.Name,
                                 VinNo = q.VinNo,
                                 VehicleYear = q.VehicleYear,
                                 StatusId = q.StatusId,
                                 Status = p.StatusName
                             }).ToList();/* _context.LeadPhones.Where(q => q.IsDeleted == false).OrderByDescending(q => q.LeadPhoneId)*/


                if (Auth.GroupID == 5)
                {
                    if (StatusId != null)
                    {
                        var result = (from q in _context.LeadPhones
                                      join p in _context.Status on q.StatusId equals p.StatusId
                                      where q.IsDeleted == false && q.StatusId == StatusId
                                      select new LeadPhoneVM
                                      {
                                          LeadPhoneId = q.LeadPhoneId,
                                          Email = q.Email,
                                          Name = q.FullName,
                                          AskingPrice = q.InitialAmount,
                                          Manufacturer = q.Manufacturer.Name,
                                          VinNo = q.VinNo,
                                          VehicleYear = q.VehicleYear,
                                          StatusId = q.StatusId,
                                          Status = p.StatusName
                                      }).ToList().OrderByDescending(q => q.LeadPhoneId);
                        return result;
                    }
                }
                else if (Auth.GroupID == 7)
                {
                    if (StatusId != null)
                    {
                        if (StatusId == 11)
                        {
                            var result = (from q in _context.LeadPhones

                                          where q.IsDeleted == false && q.isVarified == false && q.IsCompleted == true && q.AmountAccepted != null
                                          select new LeadPhoneVM
                                          {
                                              LeadPhoneId = q.LeadPhoneId,
                                              Email = q.Email,
                                              Name = q.FullName,
                                              AskingPrice = q.InitialAmount,
                                              Manufacturer = q.Manufacturer.Name,
                                              VinNo = q.VinNo,
                                              VehicleYear = q.VehicleYear,


                                          }).ToList().OrderByDescending(q => q.LeadPhoneId);
                            return result;
                        }

                        if (StatusId == 18)
                        {
                            var result2 = (from q in _context.LeadPhones

                                           where q.IsDeleted == false && q.isVarified == true
                                           select new LeadPhoneVM
                                           {
                                               LeadPhoneId = q.LeadPhoneId,
                                               Email = q.Email,
                                               Name = q.FullName,
                                               //AmountAccepted = q.AmountAccepted,
                                               Manufacturer = q.Manufacturer.Name,
                                               VinNo = q.VinNo,

                                               VehicleYear = q.VehicleYear,

                                           }).ToList();
                            return result2;
                        }

                    }
                }
                else
                {
                    if (StatusId == null)
                    {
                        var result = (from q in _context.LeadPhones

                                      where q.IsDeleted == false && q.CreatedBy == Auth.UserID
                                      select new LeadPhoneVM
                                      {
                                          LeadPhoneId = q.LeadPhoneId,
                                          Email = q.Email,
                                          Name = q.FullName,
                                          AskingPrice = q.InitialAmount,
                                          Manufacturer = q.Manufacturer.Name,
                                          VinNo = q.VinNo,
                                          VehicleYear = q.VehicleYear,
                                          StatusId = q.StatusId,


                                      }).ToList().OrderByDescending(q => q.LeadPhoneId);
                        return result;
                    }
                    else
                    {
                        var result = (from q in _context.LeadPhones
                                      join p in _context.Status on q.StatusId equals p.StatusId
                                      where q.IsDeleted == false && q.StatusId == StatusId && q.CreatedBy == Auth.UserID
                                      select new LeadPhoneVM
                                      {
                                          LeadPhoneId = q.LeadPhoneId,
                                          Email = q.Email,
                                          Name = q.FullName,
                                          AskingPrice = q.InitialAmount,
                                          Manufacturer = q.Manufacturer.Name,
                                          VinNo = q.VinNo,
                                          VehicleYear = q.VehicleYear,
                                          StatusId = q.StatusId,
                                          Status = p.StatusName
                                      }).ToList().OrderByDescending(q => q.LeadPhoneId);
                        return result;
                    }
                }
            }

            return null;

        }


        public IEnumerable<LeadPhone> Get()
        {
            return _context.LeadPhones.Where(q => q.IsDeleted == false);
        }

        public IEnumerable<LeadPhone> Get(int id)
        {
            return _context.LeadPhones.Where(q => q.LeadPhoneId == id && q.IsDeleted == false);
        }

        public bool RejectLead(int LeadPhoneId, string Note)
        {
            if (Auth.UserID > 0)
            {
                var _datalastaskingprice = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == LeadPhoneId && q.LatestAskingPrice != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
                var _datalastofferedamount = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == LeadPhoneId && q.LatestOfferedAmount != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
                var LeadPhoneobj = _context.LeadPhones.FirstOrDefault(q => q.LeadPhoneId == LeadPhoneId);
                LeadPhoneobj.IsCompleted = true;
                LeadPhoneobj.StatusId = 7;

                LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                leadphonefollowup.StatusId = 7;
                leadphonefollowup.LeadPhoneId = LeadPhoneId;
                leadphonefollowup.LatestComment = Note;
                leadphonefollowup.LatestOfferedAmount = _datalastofferedamount.Any() ? _datalastofferedamount.FirstOrDefault().LatestOfferedAmount : 0;
                leadphonefollowup.LatestAskingPrice = _datalastaskingprice.Any() ? _datalastaskingprice.FirstOrDefault().LatestAskingPrice : 0;
                leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                leadphonefollowup.StatusText = "Rejected";
                leadphonefollowup.CreatedBy = Auth.UserID;
                leadphonefollowup.CreatedDate = System.DateTime.Now;
                _context.LeadPhoneFollowUps.Add(leadphonefollowup);
                _context.SaveChanges();

            }
            return true;
        }
        public bool EditSalePrice(int LeadPhoneId, double SalePrice)
        {
            if (Auth.UserID > 0)
            {


                var query = (from q in _context.LeadPhones
                             where q.LeadPhoneId == LeadPhoneId
                             select q).FirstOrDefault();
                query.SalesPrice = Convert.ToDecimal(SalePrice);
                query.StatusId = 14;

                LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                leadphonefollowup.StatusId = 14;
                leadphonefollowup.LeadPhoneId = LeadPhoneId;
                leadphonefollowup.LatestComment = "";             
                leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                leadphonefollowup.StatusText = "Vehicle Sold";
                leadphonefollowup.CreatedBy = Auth.UserID;
                leadphonefollowup.CreatedDate = System.DateTime.Now;
                _context.LeadPhoneFollowUps.Add(leadphonefollowup);
                _context.SaveChanges();
                return true;





            }
            return false;
        }
        public bool VarifiedLead(int LeadPhoneId, string PlaceofPickup, string City, string Phone,string Email, int VehicleYear, string VinNo, string ZipCode)
        {
            if (Auth.UserID > 0)
            {


                var query = (from q in _context.LeadPhones
                             where q.LeadPhoneId == LeadPhoneId
                             select q).First();
              
            
                query.City = City;
                query.Email = Email;
                query.Phone = Phone;
                query.ZipCode = ZipCode;
              
                query.VehicleYear = VehicleYear;
                query.VinNo = VinNo;
             
                query.PlaceOfPickup = PlaceofPickup;
                query.isVarified = true;
                LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                leadphonefollowup.StatusId = 13;
                leadphonefollowup.LeadPhoneId = LeadPhoneId;
                //leadphonefollowup.LatestComment = Note;
                leadphonefollowup.StatusText = "Verified Lead";
                //leadphonefollowup.LatestOfferedAmount = _datalastofferedamount.Any() ? _datalastofferedamount.FirstOrDefault().LatestOfferedAmount : 0;
                //leadphonefollowup.LatestAskingPrice = _datalastaskingprice.Any() ? _datalastaskingprice.FirstOrDefault().LatestAskingPrice : 0;
                leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                leadphonefollowup.CreatedBy = Auth.UserID;
                leadphonefollowup.CreatedDate = System.DateTime.Now;
                _context.LeadPhoneFollowUps.Add(leadphonefollowup);
                _context.SaveChanges();
                return true;

            }
            return false;
        }
        public VarifyVM getVarified(int LeadPhoneId)
        {
            if (Auth.UserID > 0)
            {


                VarifyVM vm = (from q in _context.LeadPhones
                               where q.LeadPhoneId == LeadPhoneId
                               select new VarifyVM
                               {
                                   PlaceOfPickups = q.PlaceOfPickup,
                                   Manufacturer = q.Manufacturer.Name,
                                   Phone = q.Phone,
                                   city = q.City,
                                   AmountAccepted = q.AmountAccepted,
                                   Email = q.Email,
                                   Name = q.FullName,
                                   SalesPrice = q.SalesPrice,
                                   VehicleModel = q.VehicleModel,
                                   VehicleYear = q.VehicleYear,
                                   VinNo = q.VinNo,
                                   ZipCode = q.ZipCode
                               }).FirstOrDefault();

                return vm;
            }
            return null;

        }
        public int? getPendingLeads()
        {
            int? result = _context.LeadPhones.Where(q => q.StatusId == 2).Count();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        public bool DisqualifyLead(int LeadPhoneId, string Note)
        {
            if (Auth.UserID > 0)
            {
                var _datalastaskingprice = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == LeadPhoneId && q.LatestAskingPrice != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
                var _datalastofferedamount = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == LeadPhoneId && q.LatestOfferedAmount != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
                var LeadPhoneobj = _context.LeadPhones.FirstOrDefault(q => q.LeadPhoneId == LeadPhoneId);
                LeadPhoneobj.IsCompleted = true;
                LeadPhoneobj.StatusId = 3;

                LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                leadphonefollowup.StatusId = 3;
                leadphonefollowup.LeadPhoneId = LeadPhoneId;
                leadphonefollowup.LatestComment = Note;
                leadphonefollowup.LatestOfferedAmount = _datalastofferedamount.Any() ? _datalastofferedamount.FirstOrDefault().LatestOfferedAmount : 0;
                leadphonefollowup.LatestAskingPrice = _datalastaskingprice.Any() ? _datalastaskingprice.FirstOrDefault().LatestAskingPrice : 0;
                leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                leadphonefollowup.StatusText = "Disqualified";
                leadphonefollowup.CreatedBy = Auth.UserID;
                leadphonefollowup.CreatedDate = System.DateTime.Now;
                _context.LeadPhoneFollowUps.Add(leadphonefollowup);
                _context.SaveChanges();

            }
            return true;
        }

        public bool ApproveLead(int LeadPhoneId, string Note, decimal AmountAccepted, string PlaceofPickup)
        {
            if (Auth.UserID > 0)
            {


                var _datalastaskingprice = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == LeadPhoneId && q.LatestAskingPrice != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
                var _datalastofferedamount = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == LeadPhoneId && q.LatestOfferedAmount != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
                var LeadPhoneobj = _context.LeadPhones.FirstOrDefault(q => q.LeadPhoneId == LeadPhoneId);
                LeadPhoneobj.AmountAccepted = AmountAccepted;
                LeadPhoneobj.PlaceOfPickup = PlaceofPickup;
                LeadPhoneobj.IsCompleted = true;
                LeadPhoneobj.StatusId = 11;

                LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                leadphonefollowup.StatusId = 10;
                leadphonefollowup.LeadPhoneId = LeadPhoneId;
                leadphonefollowup.LatestComment = Note;
                leadphonefollowup.StatusText = "Offer accepted";
                leadphonefollowup.LatestOfferedAmount = _datalastofferedamount.Any() ? _datalastofferedamount.FirstOrDefault().LatestOfferedAmount : 0;
                leadphonefollowup.LatestAskingPrice = _datalastaskingprice.Any() ? _datalastaskingprice.FirstOrDefault().LatestAskingPrice : 0;
                leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                leadphonefollowup.CreatedBy = Auth.UserID;
                leadphonefollowup.CreatedDate = System.DateTime.Now;
                _context.LeadPhoneFollowUps.Add(leadphonefollowup);

                LeadPhoneFollowUp leadphonefollowup1 = new LeadPhoneFollowUp();
                leadphonefollowup1.StatusId = 11;
                leadphonefollowup1.LeadPhoneId = LeadPhoneId;
                leadphonefollowup1.LatestComment = Note;
                leadphonefollowup1.StatusText = "Lead Closed";
                leadphonefollowup1.LatestOfferedAmount = _datalastofferedamount.Any() ? _datalastofferedamount.FirstOrDefault().LatestOfferedAmount : 0;
                leadphonefollowup1.LatestAskingPrice = _datalastaskingprice.Any() ? _datalastaskingprice.FirstOrDefault().LatestAskingPrice : 0;
                leadphonefollowup1.FollowedUpDate = System.DateTime.Now;
                leadphonefollowup1.CreatedBy = Auth.UserID;
                leadphonefollowup1.CreatedDate = System.DateTime.Now;
                _context.LeadPhoneFollowUps.Add(leadphonefollowup1);
                _context.SaveChanges();

            }
            return true;
        }

        public bool AddNote(int LeadPhoneId, string Note, string Mode, decimal? Amount)
        {
            if (Auth.UserID > 0)
            {
                var _datalastaskingprice = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == LeadPhoneId && q.LatestAskingPrice != null).OrderByDescending(q => q.LeadPhoneFollowUpId);
                var _datalastofferedamount = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneId == LeadPhoneId && q.LatestOfferedAmount != null).OrderByDescending(q => q.LeadPhoneFollowUpId);

                var LeadPhoneobj = _context.LeadPhones.FirstOrDefault(q => q.LeadPhoneId == LeadPhoneId);
                if (Mode == "amountoffered")
                {
                    LeadPhoneobj.StatusId = 4;
                    LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                    leadphonefollowup.StatusId = 4;
                    leadphonefollowup.LatestOfferedAmount = Amount;
                    leadphonefollowup.LatestAskingPrice = _datalastaskingprice.Any() ? _datalastaskingprice.FirstOrDefault().LatestAskingPrice : 0;
                    leadphonefollowup.StatusText = "Manager Approved";
                    leadphonefollowup.LeadPhoneId = LeadPhoneId;
                    leadphonefollowup.LatestComment = Note;
                    leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                    leadphonefollowup.CreatedBy = Auth.UserID;
                    leadphonefollowup.CreatedDate = System.DateTime.Now;
                    _context.LeadPhoneFollowUps.Add(leadphonefollowup);
                    _context.SaveChanges();


                }
                else if (Mode == "amountask")
                {

                    LeadPhoneobj.StatusId = 2;
                    LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                    leadphonefollowup.StatusId = 5;
                    leadphonefollowup.LatestOfferedAmount = _datalastofferedamount.Any() ? _datalastofferedamount.FirstOrDefault().LatestOfferedAmount : 0;
                    leadphonefollowup.LatestAskingPrice = Amount;
                    leadphonefollowup.StatusText = "Client Response";
                    leadphonefollowup.LeadPhoneId = LeadPhoneId;
                    leadphonefollowup.LatestComment = Note;
                    leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                    leadphonefollowup.CreatedBy = Auth.UserID;
                    leadphonefollowup.CreatedDate = System.DateTime.Now;
                    _context.LeadPhoneFollowUps.Add(leadphonefollowup);

                    LeadPhoneFollowUp leadphonefollowup1 = new LeadPhoneFollowUp();
                    leadphonefollowup1.StatusId = 2;
                    leadphonefollowup1.LatestOfferedAmount = _datalastofferedamount.Any() ? _datalastofferedamount.FirstOrDefault().LatestOfferedAmount : 0;
                    leadphonefollowup1.LatestAskingPrice = Amount;

                    leadphonefollowup1.StatusText = "Pending Manager Approval";
                    leadphonefollowup1.LeadPhoneId = LeadPhoneId;
                    leadphonefollowup1.LatestComment = Note;
                    leadphonefollowup1.FollowedUpDate = System.DateTime.Now;
                    leadphonefollowup1.CreatedBy = Auth.UserID;
                    leadphonefollowup1.CreatedDate = System.DateTime.Now;
                    _context.LeadPhoneFollowUps.Add(leadphonefollowup1);
                    _context.SaveChanges();





                }
            }
            return true;
        }

        public bool AddNoteOnly(int LeadPhoneId, string Note)
        {
            if (Auth.UserID > 0)
            {
                LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                leadphonefollowup.StatusId = 12;
                leadphonefollowup.StatusText = "Note";
                leadphonefollowup.LeadPhoneId = LeadPhoneId;
                leadphonefollowup.LatestComment = Note;
                leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                leadphonefollowup.CreatedBy = Auth.UserID;
                leadphonefollowup.CreatedDate = System.DateTime.Now;
                _context.LeadPhoneFollowUps.Add(leadphonefollowup);
                _context.SaveChanges();
            }
            return true;
        }
        public bool Add(LeadPhone _data, out string error, out int LeadPhoneId)
        {

            error = "";
            LeadPhoneId = 0;
            try
            {
                if (Auth.UserID > 0)
                {
                    LeadPhone leadphone = new LeadPhone();
                    leadphone.VehicleModel = _data.VehicleModel;
                    leadphone.VehicleYear = _data.VehicleYear;
                    leadphone.ManufactureId = _data.ManufactureId;
                    leadphone.Mileage = _data.Mileage;
                    leadphone.FullName = _data.FullName;
                    leadphone.Email = _data.Email;
                    leadphone.Phone = _data.Phone;
                    leadphone.City = _data.City;
                    leadphone.Comments = _data.Comments;
                    leadphone.ZipCode = _data.ZipCode;
                    leadphone.InitialAmount = _data.InitialAmount;
                    leadphone.VinNo = _data.VinNo;
                    leadphone.CreatedBy = Auth.UserID;
                    leadphone.CreatedDate = System.DateTime.Now;
                    leadphone.StatusId = 2;  // latest status comes here
                    _context.LeadPhones.Add(leadphone);

                    LeadPhoneFollowUp leadphonefollowup = new LeadPhoneFollowUp();
                    leadphonefollowup.StatusId = 1;
                    leadphonefollowup.StatusText = "Lead Generated";
                    leadphonefollowup.IsFirstStatus = true;
                    leadphonefollowup.FollowedUpDate = System.DateTime.Now;
                    leadphonefollowup.CreatedBy = Auth.UserID;
                    leadphonefollowup.CreatedDate = System.DateTime.Now;
                    leadphone.LeadPhoneFollowUps.Add(leadphonefollowup);

                    LeadPhoneFollowUp leadphonefollowup1 = new LeadPhoneFollowUp();
                    leadphonefollowup1.StatusId = 2;
                    leadphonefollowup1.StatusText = "Pending Manager Approval";
                    leadphonefollowup1.LatestAskingPrice = _data.InitialAmount;
                    leadphonefollowup1.IsFirstStatus = true;
                    leadphonefollowup1.FollowedUpDate = System.DateTime.Now;
                    leadphonefollowup1.CreatedBy = Auth.UserID;
                    leadphonefollowup1.CreatedDate = System.DateTime.Now;
                    leadphone.LeadPhoneFollowUps.Add(leadphonefollowup1);
                    _context.SaveChanges();
                    LeadPhoneId = _data.LeadPhoneId;
                }
                return true;
            }
            catch (DbEntityValidationException e)
            {
                error = CommonHelpers.dbErrorDetail(e);
                return false;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }
        }
        public bool isVisiblePriceButton(int leadPhoneId)
        {
            var result = _context.LeadPhones.Where(q => q.IsCompleted == true && q.LeadPhoneId == leadPhoneId && q.AmountAccepted != null);
            if (result != null)
            {
                foreach (var item in result)
                {
                    if (item.SalesPrice == null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            return false;
        }
        public bool Edit(int? id, LeadPhone _data, out string error)
        {
            error = "";
            try
            {
                LeadPhone Leadphone = _context.LeadPhones.Where(e => e.LeadPhoneId == _data.LeadPhoneId).FirstOrDefault();
                Leadphone.VehicleModel = _data.VehicleModel;
                Leadphone.VehicleYear = _data.VehicleYear;
                Leadphone.ManufactureId = _data.ManufactureId;
                Leadphone.Mileage = _data.Mileage;
                Leadphone.FullName = _data.FullName;
                Leadphone.Email = _data.Email;
                Leadphone.Phone = _data.Phone;
                Leadphone.City = _data.City;
                Leadphone.Comments = _data.Comments;
                Leadphone.ZipCode = _data.ZipCode;
                //  Leadphone _data.EstimatedAmount;
                Leadphone.CreatedBy = Auth.UserID;
                Leadphone.CreatedDate = System.DateTime.Now;

                var Leadphonefollowup = _context.LeadPhoneFollowUps.Where(q => q.LeadPhoneFollowUpId == id);
                _context.LeadPhoneFollowUps.RemoveRange(Leadphonefollowup);

                LeadPhoneFollowUp LeadPhoneFollowUp = new LeadPhoneFollowUp();
                LeadPhoneFollowUp.StatusId = 5;
                LeadPhoneFollowUp.IsFirstStatus = true;
                LeadPhoneFollowUp.FollowedUpDate = System.DateTime.Now;
                LeadPhoneFollowUp.CreatedBy = Auth.UserID;
                LeadPhoneFollowUp.CreatedDate = System.DateTime.Now;                //
                _context.SaveChanges();

                return true;
            }
            catch (DbEntityValidationException e)
            {
                error = CommonHelpers.dbErrorDetail(e);
                return false;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }
        }


        public bool Delete(int id, out string error)
        {
            error = "";
            try
            {
                LeadPhone LeadPhonetext = _context.LeadPhones.Where(s => s.LeadPhoneId == id).FirstOrDefault();
                if (LeadPhonetext != null)
                {
                    LeadPhonetext.IsDeleted = true;
                    _context.SaveChanges();
                }
                return true;

            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }

        }

    }
}