﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Models
{



    [MetadataType(typeof(LeadPhoneMetaData))]
	public partial class LeadPhone
	{
        [UIHint("Label4")]
        public string Labelfor { get; set; }
	}

    public class LeadPhoneMetaData
	{
		[Display(Name = "Lead ID")]
		public string LeadPhoneId { get; set; }

		[Display(Name = "Name")]
		[Required]
		//[Remote("isExist", "Lead", AdditionalFields = "LeadId", ErrorMessage = "Lead already exist")]
		[StringLength(100, ErrorMessage = "Must be between 3 and 100 characters", MinimumLength = 3)]
		public string FullName { get; set; }


		[Display(Name = "Phone Number")]
		public string Phone { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Manufacturer")]
        public Nullable<int> ManufactureId { get; set; }

        [Display(Name = "Asking Price")]
        public decimal InitialAmount { get; set; }

     



		
	}
    public class LeadPhoneVM
    {
        public int LeadPhoneId { get; set; }
        public string Manufacturer { get; set; }
        public int? VehicleYear { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string VinNo { get; set; }
        public decimal AskingPrice { get; set; }
       public int? StatusId { get; set; }
       public string Status { get; set; }
     
    }
}