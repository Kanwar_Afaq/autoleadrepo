﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeadApp.Models
{
	[MetadataType(typeof(CurrencyMetaData))]
	public partial class Currency {
        //public string CurrenyName {
        //    get { return CurrencyName; }  
        //}
	}

    public class CurrencyMetaData
    {       
		[Display(Name = "Currency")]
        public string CurrencyName { get; set; }
    }
}