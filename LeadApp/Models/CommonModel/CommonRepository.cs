﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Models {
	public class CommonRepository {
		public LeadAppEntities _context = new LeadAppEntities();


		public IEnumerable<Country> GetCountry() {
			return _context.Countries;
		}




		public void Dispose(bool disposing) {
			if (disposing) {
				_context.Dispose();
			}

		}

	}



	//public class CurrencyRepository {
	//	LeadAppEntities context = new LeadAppEntities();

	//	public List<Currency> CurrencyByIDName() {
	//		return context.Currencies.ToList();
	//	}

	//	public void Dispose(bool disposing) {
	//		if (disposing) {
	//			context.Dispose();
	//		}
	//	}
	//}
}