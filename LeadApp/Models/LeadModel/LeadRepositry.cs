﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using LeadApp.Models;
using System.Web.Mvc;
using System.Xml;
using System.Linq.Expressions;
using LeadApp.Helpers;
using System.Data.Entity.Validation;

namespace LeadApp.Models
{
	public class LeadRepositry
	{
		private LeadAppEntities _context = new LeadAppEntities();

		public IEnumerable<Lead> Get()
		{
			return _context.Leads;
		}

		public Lead GetSingle(int Id)
		{
			return _context.Leads.Where(e => e.LeadId == Id).FirstOrDefault();
		}


		public IEnumerable<LeadViewModel> GetList()
		{
			var qry = from a in _context.Leads
					  join w in _context.Websites on a.WebId equals w.WebId
					  join m in _context.Manufacturers on a.ManufactureId equals m.ManufactureId
					  orderby a.LeadId descending
					  select new LeadViewModel
					  {
						  LeadId = a.LeadId,
						  EntryDate = a.EntryDate,
						  WebId = w.WebId,
						  WebsiteName = w.Name,
						  Name = a.Name,
						  Phone = a.Phone,
						  Email = a.Email,
						  VehicleYear = a.VehicleYear,
						  ManufactureId = m.ManufactureId,
						  ManufacturerName = m.Name,
						  VehicleModel = a.VehicleModel,
						  Mileage = a.Mileage,
						  ZipCode = a.ZipCode
					  };

			return qry;
		}

		public bool Add(Lead _data, out int Id, out string error)
		{
			error = "";
			Id = 0;

			try
			{
				_data.isPeddlePosted = false;
				_data.EntryDate = DateTime.Now;
				_data.IPAddress = HttpContext.Current.Request.UserHostAddress;
				_data.Browser = HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version;
				_data.CreatedBy = Auth.UserID;
				_data.CreatedDate = System.DateTime.Now;

				_context.Leads.Add(_data);

				_context.SaveChanges();
				Id = _data.LeadId;
				return true;
			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				error = ex.InnerException != null ? ex.InnerException.Message : "";
				return false;
			}

		}

		public bool Ack(int LeadId, string PeddleError, out string error)
		{
			error = "";

			try
			{
				Lead _setData = _context.Leads.Where(e => e.LeadId == LeadId).SingleOrDefault();
				if (_setData != null)
				{
					_setData.isPeddlePosted = false;
					_setData.PeddleError = PeddleError;


					//_context.Entry(_setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}
			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}
		}

		public bool PeddlePosted(int LeadId, out string error)
		{
			error = "";

			try
			{
				Lead _setData = _context.Leads.Where(e => e.LeadId == LeadId).SingleOrDefault();
				if (_setData != null)
				{
					_setData.isPeddlePosted = true;

					_setData.ModifiedBy = Auth.UserID;
					_setData.ModifiedDate = System.DateTime.Now;


					//_context.Entry(_setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}
			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}
		}

		public bool Edit(Lead _data, out string error)
		{
			error = "";

			try
			{
				Lead _setData = _context.Leads.Where(e => e.LeadId == _data.LeadId).SingleOrDefault();
				if (_setData != null)
				{
					_setData.WebId = _data.WebId;
					_setData.ManufactureId = _data.ManufactureId;
					_setData.VehicleYear = _data.VehicleYear;
					_setData.VehicleModel = _data.VehicleModel;
					_setData.Mileage = _data.Mileage;
					_setData.ConditionId = _data.ConditionId;
					_setData.TitleId = _data.TitleId;
					_setData.ZipCode = _data.ZipCode;
					_setData.Name = _data.Name;
					_setData.Phone = _data.Phone;
					_setData.Email = _data.Email;
					_setData.Comments = _data.Comments;
					_setData.EntryDate = _data.EntryDate;


					_setData.ModifiedBy = Auth.UserID;
					_setData.ModifiedDate = System.DateTime.Now;


					//_context.Entry(_setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}

			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}


		public bool Delete(int id, out string error)
		{
			error = "";
			try
			{
				Lead DTask = _context.Leads.Where(s => s.LeadId == id).SingleOrDefault();
				_context.Leads.Remove(DTask);
				_context.SaveChanges();

				return true;

			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}

		}


		public bool isExist(string Name, int? Id)
		{
			bool isAvail = false;
			int dbID = Id.ConvertToInt();
			try
			{
				isAvail = _context.Leads.Any(m => m.Name == Name);
				if (Id.IsNumeric())
					isAvail = _context.Leads.Any(m => m.Name == Name && m.LeadId != dbID);
			}
			catch { }

			return isAvail;
		}



	}
}