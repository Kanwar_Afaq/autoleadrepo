﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Models
{

	public class LeadViewModel : Lead
	{
		public string WebsiteName { get; set; }
		public string ManufacturerName { get; set; }
	}

	[MetadataType(typeof(LeadMetaData))]
	public partial class Lead
	{

	}

	public class LeadMetaData
	{
		[Display(Name = "Name")]
		[Required]
		//[Remote("isExist", "Lead", AdditionalFields = "LeadId", ErrorMessage = "Lead already exist")]
		[StringLength(100, ErrorMessage = "Must be between 3 and 100 characters", MinimumLength = 3)]
		public string Name { get; set; }


		[Display(Name = "Phone Number")]
		public string Phone { get; set; }

		[Display(Name = "Vehicle Comments")]
		public string Comments { get; set; }

		[Display(Name = "Vehicle Condition")]
		public Nullable<int> ConditionId { get; set; }

		[Display(Name = "Vehicle Mileage")]
		public string Mileage { get; set; }

		[Display(Name = "Do you have your vehicle title?")]
		public Nullable<int> TitleId { get; set; }

		[Display(Name = "Vehicle Make")]
		public Nullable<int> ManufactureId { get; set; }

		[Display(Name = "Website")]
		public int WebId { get; set; }
	}
}