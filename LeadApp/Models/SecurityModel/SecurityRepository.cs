﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using LeadApp.Helpers;
using LeadApp.Models;
using LeadApp.Services;

namespace LeadApp.Models
{
    public class SecurityRepository
    {
        public LeadAppEntities _context = new LeadAppEntities();

		public AppGroupModule VerifyUserAccess(string controllerName) {
			var qry = (from G in _context.AppGroupModules
					   join GM in _context.AppModules on G.ModuleId equals GM.ModuleID
					   where G.GroupId == Auth.GroupID
					   && GM.ModuleLink.ToLower() == controllerName.ToLower()
					   && G.IsAllowed == true
					   select G);

			return qry.FirstOrDefault();
		}


		public IEnumerable<AppModule> getUserModules()
		{
			var qry = (from G in _context.AppGroupModules
					   join GM in _context.AppModules on G.ModuleId equals GM.ModuleID
					   where G.GroupId == Auth.GroupID
					   && G.IsAllowed == true
					   select GM);

			return qry;
		}





		#region Module Methods

		public List<AppModule> GetAllModules() {
			var Features = (from M in _context.AppModules
							select M).ToList();

			return Features;


		}
		public AppModule GetModule(int Id) {
			var Features = (from AM in _context.AppModules
							where AM.ModuleID == Id
							select AM).FirstOrDefault();

			return Features;
		}



		public List<AppModuleConfig> GetAppModuleConfig()
		{
			var qry = (from M in _context.AppModuleConfigs
							select M).ToList();

			return qry;
		}

		public AppModuleConfig GetAppModuleConfig(int Id)
		{
			var qry = (from M in _context.AppModuleConfigs
					   where M.ConfigID == Id
					   select M).FirstOrDefault();

			return qry;
		}

		public bool ConfigEdit(int Id, AppModuleConfig getDATA, out string error)
		{
			error = string.Empty;
			try
			{
				var setData = _context.AppModuleConfigs.FirstOrDefault(m => m.ConfigID == Id);

				if (setData != null)
				{
					//setData.ModuleID = getDATA.ModuleID;
					setData.ConfigKey = getDATA.ConfigKey;
					setData.ConfigValue = getDATA.ConfigValue;

					setData.ModifiedBy = Auth.UserID;
					setData.ModifiedDate = DateTime.Now;



					_context.Entry(setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				}
				else
				{
					error = "ID Not Valid";
					return false;
				}

			}
			catch (DbEntityValidationException e)
			{
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			}
			catch (Exception ex)
			{
				error = ex.Message;
				return false;
			}
		}






		public bool isModuleExist(string ModuleName, int ModuleID) {
			bool isExist = false;
			try {
				if (ModuleID == 0)
					isExist = _context.AppModules.Any(m => m.ModuleName == ModuleName);
				else
					isExist = _context.AppModules.Any(m => m.ModuleName == ModuleName && m.ModuleID != ModuleID);

			} catch { }

			return isExist;
		}
		public bool CreateModule(AppModule getDATA, out string error) {
			error = string.Empty;
			try {
				getDATA.CreatedBy = Auth.UserID;
				getDATA.CreatedDate = DateTime.Now;

				_context.AppModules.Add(getDATA);
				_context.SaveChanges();

				return true;

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}		 																			   
		public bool EditModule(int Id, AppModule getDATA, out string error) {
			error = string.Empty;
			try {
				var setData = _context.AppModules.FirstOrDefault(m => m.ModuleID == Id);

				if (setData != null) {
					setData.ModuleID = getDATA.ModuleID;
					setData.ModuleName = getDATA.ModuleName;
					setData.ModuleLink = getDATA.ModuleLink;
					setData.ModuleIndex = getDATA.ModuleIndex;
					setData.ModuleParam = getDATA.ModuleParam;
					setData.ShowInMenu = getDATA.ShowInMenu;
					setData.isPrimary = getDATA.isPrimary;

					setData.Sort = getDATA.Sort;
					setData.CssClass = getDATA.CssClass;

					setData.HasFullAccess = getDATA.HasFullAccess;
					setData.HasCreate = getDATA.HasCreate;
					setData.HasEdit = getDATA.HasEdit;
					setData.HasDelete = getDATA.HasDelete;
					setData.HasView = getDATA.HasView;
					setData.HasDetail = getDATA.HasDetail;

					setData.HasCustom1 = getDATA.HasCustom1;
					setData.HasCustom2 = getDATA.HasCustom2;
					setData.HasCustom3 = getDATA.HasCustom3;
					setData.HasCustom4 = getDATA.HasCustom4;
					setData.HasCustom5 = getDATA.HasCustom5;
					setData.HasCustom6 = getDATA.HasCustom6;
					setData.HasCustom7 = getDATA.HasCustom7;
					setData.HasCustom8 = getDATA.HasCustom8;
					setData.HasCustom9 = getDATA.HasCustom9;
					setData.HasCustom10 = getDATA.HasCustom10;
					setData.HasCustom11 = getDATA.HasCustom11;
					setData.HasCustom12  = getDATA.HasCustom12;
					setData.HasCustom13 = getDATA.HasCustom13;
					setData.HasCustom14 = getDATA.HasCustom14;
					setData.HasCustom15 = getDATA.HasCustom15;
					setData.HasCustom16 = getDATA.HasCustom16;
					setData.HasCustom17 = getDATA.HasCustom17;
					setData.HasCustom18 = getDATA.HasCustom18;
					setData.HasCustom19 = getDATA.HasCustom19;
					setData.HasCustom20 = getDATA.HasCustom20;

					setData.Label1 = getDATA.Label1;
					setData.Label2 = getDATA.Label2;
					setData.Label3 = getDATA.Label3;
					setData.Label4 = getDATA.Label4;
					setData.Label5 = getDATA.Label5;
					setData.Label6 = getDATA.Label6;
					setData.Label7 = getDATA.Label7;
					setData.Label8 = getDATA.Label8;
					setData.Label9 = getDATA.Label9;
					setData.Label10 = getDATA.Label10;

					setData.Label11 = getDATA.Label11;
					setData.Label12 = getDATA.Label12;
					setData.Label13 = getDATA.Label13;
					setData.Label14 = getDATA.Label14;
					setData.Label15 = getDATA.Label15;
					setData.Label16 = getDATA.Label16;
					setData.Label17 = getDATA.Label17;
					setData.Label18 = getDATA.Label18;
					setData.Label19 = getDATA.Label19;
					setData.Label20 = getDATA.Label20;


					setData.ModifiedBy = Auth.UserID;
					setData.ModifiedDate = DateTime.Now;



					_context.Entry(setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;
				} else {
					error = "ID Not Valid";
					return false;
				}

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}
		public bool DeleteModule(int Id, out string error) {
			error = string.Empty;
			try {
				AppModule delData = _context.AppModules.Where(m => m.ModuleID == Id).FirstOrDefault();
				if (delData != null) {
					_context.AppModules.Remove(delData);
					_context.SaveChanges();

					return true;

				} else {
					error = "ID Not Valid";
					return false;
				}

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}


		#endregion





		#region Group Methods

		public IEnumerable<AppGroup> AppGroups() {
			return _context.AppGroups;
		}
		public AppGroup getAppGroup(int Id) {
			return _context.AppGroups.FirstOrDefault(e => e.GroupId == Id);
		} 



		public bool CreateGroup(AppGroup getDATA, out string error) {
			error = string.Empty;
			try {
				getDATA.CreatedBy = Auth.UserID;
				getDATA.CreatedDate = DateTime.Now;
				_context.AppGroups.Add(getDATA);
				_context.SaveChanges();

				return true;

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}			 
		public bool EditGroup(int Id, AppGroup getDATA, out string error) {
			error = string.Empty;
			try {
				var setData = _context.AppGroups.Where(m => m.GroupId == Id).FirstOrDefault();
				
				
				if (setData != null) {
					_context.AppGroupModules.RemoveRange(setData.AppGroupModules);

					setData.GroupName = getDATA.GroupName;
					setData.isDisable = getDATA.isDisable;
					setData.AppGroupModules = getDATA.AppGroupModules;

					setData.ModifiedBy = Auth.UserID;
					setData.Modified = DateTime.Now;

					_context.Entry(setData).State = System.Data.Entity.EntityState.Modified;
					_context.SaveChanges();
					return true;


				} else {
					error = "ID Not Valid";
					return false;
				}

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}
		public bool DeleteGroup(int Id, out string error) {
			error = string.Empty;
			try {
				AppGroup delData = _context.AppGroups.Where(m => m.GroupId == Id).FirstOrDefault();
				_context.AppGroupModules.RemoveRange(delData.AppGroupModules);
				_context.AppGroups.Remove(delData);
				_context.SaveChanges();

				return true;

			} catch (DbEntityValidationException e) {
				error = CommonHelpers.dbErrorDetail(e);
				return false;
			} catch (Exception ex) {
				error = ex.Message;
				return false;
			}
		}

		#endregion




		public void Dispose(bool disposing) {
			if (disposing) {
				_context.Dispose();
			}

		}
	}
}