﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Models {

	[MetadataType(typeof(AppModuleMetaData))]
	public partial class AppModule {

		public bool isGroupModuleAsigned { get; set; }
		public string Roles {
			get {
				return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15} {16} {17} {18} {19} {20} {21} {22} {23} {24} {25}",
							(HasFullAccess ? "FullAccess" : ""),
							(HasCreate ? "Create" : ""),
							(HasEdit ? "Edit" : ""),
							(HasView ? "View" : ""),
                            (HasDetail ? "Details" : ""),
							(HasDelete ? "Delete" : ""),
							(HasCustom1 ? Label1 : ""),
							(HasCustom2 ? Label2 : ""),
							(HasCustom3 ? Label3 : ""),
							(HasCustom4 ? Label4 : ""),
							(HasCustom5 ? Label5 : ""),
							(HasCustom6 ? Label6 : ""),
							(HasCustom7 ? Label7 : ""),
							(HasCustom8 ? Label8 : ""),
							(HasCustom9 ? Label9 : ""),
							(HasCustom10 ? Label10 : ""),
							(HasCustom11 ? Label11 : ""),
							(HasCustom12 ? Label12 : ""),
							(HasCustom13 ? Label13 : ""),
							(HasCustom14 ? Label14 : ""),
							(HasCustom15 ? Label15 : ""),
							(HasCustom16 ? Label16 : ""),
							(HasCustom17 ? Label17 : ""),
							(HasCustom18 ? Label18 : ""),
							(HasCustom19 ? Label19 : ""),
							(HasCustom20 ? Label20 : "")
							);
			}
		}

		public List<ModuleRights> Rights {
			get {
				List<ModuleRights> ExtraFields = new List<ModuleRights>();
				if (HasFullAccess)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = "FullAccess";
					ExtraFields.Add(mr);
				}
				if (HasView) {
					ModuleRights mr = new ModuleRights();
					mr.Name = "Index";
					ExtraFields.Add(mr);
				}
				if (HasCreate) {
					ModuleRights mr = new ModuleRights();
					mr.Name = "Create";
					ExtraFields.Add(mr);
				}
				if (HasEdit) {
					ModuleRights mr = new ModuleRights();
					mr.Name = "Edit";
					ExtraFields.Add(mr);
				}
				if (HasDelete) {
					ModuleRights mr = new ModuleRights();
					mr.Name = "Delete";
					ExtraFields.Add(mr);
				}
				if (HasDetail) {
					ModuleRights mr = new ModuleRights();
					mr.Name = "Details";
					ExtraFields.Add(mr);
				}  
				if (HasCustom1) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label1;
					ExtraFields.Add(mr);
				}
				if (HasCustom2) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label2;
					ExtraFields.Add(mr);
				}
				if (HasCustom3) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label3;
					ExtraFields.Add(mr);
				}
				if (HasCustom4) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label4;
					ExtraFields.Add(mr);
				}
				if (HasCustom5) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label5;
					ExtraFields.Add(mr);
				}
				if (HasCustom6) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label6;
					ExtraFields.Add(mr);
				}	   
				if (HasCustom7) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label7;
					ExtraFields.Add(mr);
				}
				if (HasCustom8) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label8;
					ExtraFields.Add(mr);
				}
				if (HasCustom9) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label9;
					ExtraFields.Add(mr);
				}
				if (HasCustom10) {
					ModuleRights mr = new ModuleRights();
					mr.Name = Label10;
					ExtraFields.Add(mr);
				}




				if (HasCustom11)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label11;
					ExtraFields.Add(mr);
				}
				if (HasCustom12)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label12;
					ExtraFields.Add(mr);
				}
				if (HasCustom13)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label13;
					ExtraFields.Add(mr);
				}
				if (HasCustom14)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label14;
					ExtraFields.Add(mr);
				}
				if (HasCustom15)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label15;
					ExtraFields.Add(mr);
				}
				if (HasCustom16)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label16;
					ExtraFields.Add(mr);
				}
				if (HasCustom17)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label17;
					ExtraFields.Add(mr);
				}
				if (HasCustom18)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label18;
					ExtraFields.Add(mr);
				}
				if (HasCustom19)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label19;
					ExtraFields.Add(mr);
				}
				if (HasCustom20)
				{
					ModuleRights mr = new ModuleRights();
					mr.Name = Label20;
					ExtraFields.Add(mr);
				}

				return ExtraFields;
			}
		}


		private List<ModuleRights> _RightsModified = new List<ModuleRights>();
		public List<ModuleRights> RightsModified { get { return _RightsModified; } set { _RightsModified = value; } }
	}

	public class ModuleRights {
		public ModuleRights() {
			Value = true;
			isChecked = string.Empty;
		}

		public string Name { get; set; }
		public bool Value { get; set; }
		public string isChecked { get; set; }
	}

	public class AppModuleMetaData {
		[Required]
		[Remote("isModuleExist", "Module", AdditionalFields = "ModuleID", ErrorMessage = "Module name already exists. Please enter a different name.")]
		[Display(Name = "Module Name")]
		public string ModuleName { get; set; }

		[Required]
		[Display(Name = "Module Link")]
		public string ModuleLink { get; set; }
		
		[Range(0, 99)]
		public int Sort { get; set; }

		[Display(Name = "FullAccess")]
		public bool HasFullAccess { get; set; }

		[Display(Name="Create")]
		public bool HasCreate { get; set; }
		[Display(Name = "Edit")]
		public bool HasEdit { get; set; }
		[Display(Name = "Delete")]
		public bool HasDelete { get; set; }
		[Display(Name = "Index")]
		public bool HasView { get; set; }
		[Display(Name = "Detail")]
		public bool HasDetail { get; set; }
	}





	[MetadataType(typeof(AppGroupMetaData))]
	public partial class AppGroup {

		//public List<AppModule> AppModuleList { get; set; }
	}	
	public class AppGroupMetaData {
		[Required]
		[Display(Name = "Group Name")]
		public string GroupName { get; set; }

		//[Range(10, 99)]
		public int Priority { get; set; }

		[Display(Name = "Disable")]
		public bool isDisable { get; set; }
	}
  
}