﻿using LeadApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LeadApp.Helpers;


namespace LeadApp.Services {
	public class WebsiteService
	{
		WebsiteRepositry _rep = new WebsiteRepositry();
		UserService _ServUser = new UserService();
		string error = "";

		public IEnumerable<Website> Get()
		{
			return _rep.Get();
		}

		
		public bool Add(Website _data, out string error)
		{

			bool isSuccess = _rep.Add(_data, out error);
			return isSuccess;
		}

		public bool Edit(Website _data, out string error)
		{

			bool isSuccess = _rep.Edit(_data, out error);
			return isSuccess;
		}



		public bool Delete(int id, out string error) {
			return _rep.Delete(id, out error);
		}


		public bool isExist(string name, int? id)
		{
			return _rep.isExist(name, id);
		}
	}
}