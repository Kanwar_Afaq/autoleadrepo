﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LeadApp.Models;

namespace LeadApp.Services
{
    public class FileUploaderServices
    {

        FileUploaderRepository fileuploaderrepository = new FileUploaderRepository();

        public List<FileViewModel> GetFileAttachments(Guid FileAttahementId)
        {
            return fileuploaderrepository.GetFileAttachments(FileAttahementId);
        }

        public List<FileViewModel> GetFileAttachments()
        {
            return fileuploaderrepository.GetFileAttachments();
        }

        public void AddFileAttacments(FileViewModel fileuploader)
        {
            fileuploaderrepository.AddFileAttacments(fileuploader);

        }


        public void UpdateAttachment(Guid Id, FileViewModel fileuploader)
        {
            fileuploaderrepository.UpdateAttachment(Id, fileuploader);
        }

        public void DeleteAttachment(Guid Id)
        {
            fileuploaderrepository.DeleteAttachment(Id);
        }


    }
}