﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LeadApp.Models;
using System.Web.Mvc;

namespace LeadApp.Services
{
    public class ActivityLogServices
    {
        ActivityLogRepository _rep = new ActivityLogRepository();

        public IEnumerable<ActivityLog> GetActivityLog()
        {
            return _rep.GetActivityLog().OrderByDescending(e=>e.ActivityLogID).Take(25);
        }

    }
}