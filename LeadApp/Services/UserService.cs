﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LeadApp.Helpers;
using LeadApp.Models;

namespace LeadApp.Services {
	public class UserService {
		private UserRepository _rep = new UserRepository();


		#region Login Methods

		public IEnumerable<AppUser> GetUserDetail() {
			return _rep.GetUserDetail();
		}
		public IEnumerable<AppUserMeta> GetUserMeta(int UserID = 0) {
			return _rep.GetUserMeta(UserID);
		}
		public void SignOut() {
			if (Auth.isLogin)
				_rep.InsertUserLogoutLog();
		}



		public bool CreateSession(LoginViewModel logindata, out string error, bool VerifyCookieUser = false, bool EncryptPwd = true) {
			error = "";
			try {
				string EncPwd = EncryptPwd ? EncryptDecryptHelper.Encrypt(logindata.Password) : logindata.Password;
				AppUser UserDetail = GetUserDetail().Where(e => e.UserName == logindata.UserName && e.Password == EncPwd).SingleOrDefault();

				if (VerifyCookieUser)
					UserDetail = GetUserDetail().Where(e => e.UserName == logindata.UserName && e.UserGUID == logindata.UserGUID && e.isEmployee == true).SingleOrDefault();


				if (UserDetail != null) {
					if (UserDetail.isDisable) {
						error = "User account is disabled";
						return false;
					}

					var ThumbImg = GetUserMeta(UserDetail.UserId).Where(e => e.MetaKey == "Thumbnail-Original").Select(e => e.MetaValue).FirstOrDefault();
					AppEmployee EmployeeDetail = UserDetail.AppEmployees.FirstOrDefault(e => e.UserId == UserDetail.UserId);
					string UserEmail = (EmployeeDetail != null) ? EmployeeDetail.Email : "";

					HttpContext.Current.Session["UserLogID"] = _rep.UserLoginLog(UserDetail.UserId);

					///put all the properties in session variables
					HttpContext.Current.Session["isLogin"] = "true";
					HttpContext.Current.Session["UserID"] = UserDetail.UserId;
					HttpContext.Current.Session["UserGUID"] = UserDetail.UserGUID;
					HttpContext.Current.Session["UserName"] = UserDetail.UserName;
					HttpContext.Current.Session["GroupID"] = UserDetail.GroupID;
					HttpContext.Current.Session["Name"] = (EmployeeDetail != null) ? EmployeeDetail.ContactName : "";
					HttpContext.Current.Session["Email"] = UserEmail;
					HttpContext.Current.Session["Signature"] = (EmployeeDetail != null) ? EmployeeDetail.Signature : "";
					HttpContext.Current.Session["IsActive"] = UserDetail.isActive ? "true" : "false";
					HttpContext.Current.Session["Thumbnail"] = (ThumbImg != null) ? ThumbImg : "";

					// Add User Login Info in Cookie if Cookie is Enabled in User Browser
					if (logindata.isRemember && WebConfig.isCookieEnable) {
						Random rand = new Random();
						string AuthString = "UserID=" + UserDetail.UserId + "&UserGUID=" + UserDetail.UserGUID + "&UserName=" + UserDetail.UserName + "&Email=" + UserEmail;
						string AuthKey = EncryptDecryptHelper.Encrypt(AuthString + "&RandomNo=" + rand.Next());
						HttpCookie LoginAuthKey = new HttpCookie("AuthKey", AuthKey);
						LoginAuthKey.Expires = DateTime.Now.AddDays(30d);
						HttpContext.Current.Response.Cookies.Add(LoginAuthKey);
					}


					return true;
				} else {
					error = "Invalid user name or password.";
					return false;
				}
			} catch (System.Data.SqlClient.SqlException ex) {
				if (ex.Number == 53)
					error = "Error Occured: Database server is not accessible.";
				else if (ex.Number == 4060)
					error = "Error Occured: Database either does not exist or is inaccessible.";
				else if (ex.Number == 18456)
					error = "Error Occured: Error in database credentials.";
				else
					error = "Error Occured: Error in database connectivity.";

				return false;
			} catch (Exception ex) {
				error = "Error Occured: " + ex.Message;
				return false;
			}
		}
		public void CreateSessionByCookie() {
			if (!Auth.isLogin) {

				var AuthKey = HttpContext.Current.Request.Cookies["AuthKey"];
				if (AuthKey != null) {
					string AuthorizationKey = EncryptDecryptHelper.Decrypt(AuthKey.Value);
					//Verify Auth Key is Valid or not
					if (!string.IsNullOrEmpty(AuthorizationKey)) {
						var UserID = AuthorizationKey.Split('&')[0];
						var UserGUID = AuthorizationKey.Split('&')[1];
						var UserName = AuthorizationKey.Split('&')[2];
						var EmailAddress = AuthorizationKey.Split('&')[3];

						var UserIDData = UserID.Substring(UserID.IndexOf("=") + 1);
                        var UserGUIDData = UserGUID.Substring(UserGUID.IndexOf("=") + 1);
						var UserNameData = UserName.Substring(UserName.IndexOf("=") + 1);
						var EmailAddressData = EmailAddress.Substring(EmailAddress.IndexOf("=") + 1);

						LoginViewModel logindata = new LoginViewModel();
						logindata.UserName = UserNameData;
						logindata.UserId = HelperMethods.ConvertToInt(UserIDData);
						logindata.UserGUID = HelperMethods.ConvertToGUID(UserGUIDData);
						logindata.Email = EmailAddressData;

						string error = "";
						CreateSession(logindata, out error, true);
					}
				}
			}
		}



		public bool Forgot(AppUserPwdRequest _data, out string error)
		{
			error = "";
			try
			{
				IEnumerable<AppUser> VerifyUser;

				if (!string.IsNullOrEmpty(_data.UserName) && !string.IsNullOrEmpty(_data.Email))
					VerifyUser = GetUserDetail().Where(e => e.UserName == _data.UserName && e.AppEmployees.FirstOrDefault().Email == _data.Email);
				else if (!string.IsNullOrEmpty(_data.UserName))
					VerifyUser = GetUserDetail().Where(e => e.UserName == _data.UserName);
				else if (!string.IsNullOrEmpty(_data.Email))
					VerifyUser = GetUserDetail().Where(e => e.AppEmployees.FirstOrDefault().Email == _data.Email);
				else
					VerifyUser = GetUserDetail().Where(e => e.UserName == _data.UserName && e.AppEmployees.FirstOrDefault().Email == _data.Email);

				if (VerifyUser.Count() > 0)
				{
					AppUser UserData = VerifyUser.SingleOrDefault();

					if (UserData.isDisable)
					{
						error = "User account is disabled. Password cannot be reset";
						return false;
					}
					else
					{
						string RandomNo = HelperMethods.GenerateRandomText();
						//save in log

						int PasswordRequestID = _rep.Forgot(UserData.UserId, RandomNo);


						string emailLink = "r=" + RandomNo + "&id=" + PasswordRequestID;
						string LinkURL = CommonHelpers.GetRedirectionLink(emailLink);

						//Send Email to user informing its new password
						bool SendEmail = SystemEmailService.ResetPasswordRequest(UserData.UserName, UserData.AppEmployees.FirstOrDefault().Email, LinkURL);

						return true;
					}

					
				}
				else
				{
					error = "No User exist of given details.";
					return false;
				}

			}
			catch (Exception ex)
			{
				error = "Error Occured: " + ex.Message;
				return false;
			}
		}


		public bool ResetPassword(int id, string Password)
		{
			return _rep.ResetPassword(id, Password);
		}

		#endregion









		#region User Creation Methods

		public IEnumerable<AppEmployee> GetEmployees() {
			return _rep.GetEmployee();
		}

		public AppEmployee GetUser(int UserID) {
			return _rep.GetEmployee().Where(e => e.UserId == UserID).FirstOrDefault();
		}

		public AppEmployee GetEmployee(Guid id) {
			return _rep.GetEmployee().Where(e => e.EmployeeID == id).FirstOrDefault();
		}


		public bool isUserExist(string UserName, int UserId) {
			return (_rep.isUserExist(UserName, UserId) ? false : true);
		}

		public bool isUserEmailExist(string Email, int UserId)
		{
			return (_rep.isUserEmailExist(Email, UserId) ? false : true);
		}


		public bool CreateEmployee(AppEmployee appemployee, List<int> PartyDepartments, int GroupId, bool isEmailSend, out string error, out bool isEmailSent)
		{

			appemployee.EmployeeID = Guid.NewGuid();
			appemployee.AppUser.UserGUID = Guid.NewGuid();
			appemployee.AppUser.GroupID = GroupId;
			appemployee.AppUser.isEmployee = true;
			appemployee.AppUser.isActive = true;
			appemployee.AppUser.isActive = true;

			appemployee.AppUser.CreatedBy = Auth.UserID;
			appemployee.AppUser.CreatedDate = DateTime.Now;
			appemployee.CreatedBy = Auth.UserID;
			appemployee.CreatedDate = DateTime.Now;



			string UserPwd = string.IsNullOrEmpty(appemployee.AppUser.Password) ? HelperMethods.GenerateRandomText(8) : appemployee.AppUser.Password;
			string EncPwd = EncryptDecryptHelper.Encrypt(UserPwd);
			appemployee.AppUser.Password = EncPwd;




			//List<AppEmployeeDepartment> empDepts = new List<AppEmployeeDepartment>();
			//foreach (var item in PartyDepartments) {
			//	AppEmployeeDepartment eDept = new AppEmployeeDepartment();
			//	eDept.EmployeeID = appemployee.EmployeeID;
			//	eDept.PartyDepartmentId = item;

			//	empDepts.Add(eDept);
			//}
			//appemployee.AppEmployeeDepartments = empDepts;


			bool isSuccess = _rep.Create(appemployee, out error);
			isEmailSent = false;
			if (isSuccess & isEmailSend)
				isEmailSent = SystemEmailService.NewAccountCreated(appemployee.ContactName, appemployee.AppUser.UserName, UserPwd, appemployee.Email);

			return isSuccess;
		}



		public AppUser CreateCustomer(AppEmployee appemployee, List<int> PartyDepartments, int GroupId, out string error) {
			bool Check = false;

			appemployee.EmployeeID = Guid.NewGuid();
			appemployee.AppUser.UserGUID = Guid.NewGuid();
			appemployee.AppUser.GroupID = GroupId;
			appemployee.AppUser.isEmployee = false;
			appemployee.AppUser.isActive = true;
			appemployee.AppUser.CreatedBy = Auth.UserID;
			appemployee.AppUser.CreatedDate = DateTime.Now;



			string UserPwd = string.IsNullOrEmpty(appemployee.AppUser.Password) ? HelperMethods.GenerateRandomText(8) : appemployee.AppUser.Password;
			string EncPwd = EncryptDecryptHelper.Encrypt(UserPwd);
			appemployee.AppUser.Password = EncPwd;




			//List<AppEmployeeDepartment> empDepts = new List<AppEmployeeDepartment>();

			//if (PartyDepartments != null) {
			//	foreach (var item in PartyDepartments) {
			//		AppEmployeeDepartment eDept = new AppEmployeeDepartment();
			//		eDept.EmployeeID = appemployee.EmployeeID;
			//		eDept.PartyDepartmentId = item;

			//		empDepts.Add(eDept);
			//	}
			//}
			//appemployee.AppEmployeeDepartments = empDepts;



			Check = _rep.CreateAppUser(appemployee, out error);
			if (Check) {
				AppUser NewUser = new AppUser();
				NewUser = _rep.Get_UserDetail(appemployee.AppUser.UserName);
				return NewUser;
			} else {
				return null;
			}
		}


		public bool EditProfile(AppEmployee appemployee, out string error) {

			appemployee.AppUser.Password = !string.IsNullOrEmpty(appemployee.AppUser.Password) ? EncryptDecryptHelper.Encrypt(appemployee.AppUser.Password) : "";

			return _rep.EditProfile(appemployee, out error);
		}

		public bool EditEmployee(Guid id, AppEmployee appemployee, List<int> PartyDepartments, int GroupId, bool isEmailSend, out string error, out bool isEmailSent)
		{


			appemployee.AppUser.GroupID = GroupId;
			appemployee.AppUser.Password = !string.IsNullOrEmpty(appemployee.AppUser.Password) ? EncryptDecryptHelper.Encrypt(appemployee.AppUser.Password) : "";


			//List<AppEmployeeDepartment> empDepts = new List<AppEmployeeDepartment>();
			//foreach (var item in PartyDepartments) {
			//	AppEmployeeDepartment eDept = new AppEmployeeDepartment();
			//	eDept.EmployeeID = appemployee.EmployeeID;
			//	eDept.PartyDepartmentId = item;

			//	empDepts.Add(eDept);
			//}
			//appemployee.AppEmployeeDepartments = empDepts;





			bool isSuccess = _rep.Edit(id, appemployee, out error);
			isEmailSent = false;
			if (isSuccess & isEmailSend)
				isEmailSent = SystemEmailService.AccountUpdated(appemployee.ContactName, appemployee.AppUser.UserName, appemployee.AppUser.Password, appemployee.Email);

			return isSuccess;

		}
		public bool DeletEmployee(Guid id, out string error) {
			return _rep.Delete(id, out error);
		}




		public bool UserThumbnails(string FilLocation) {
			string error = "";
			try {
				int UserProfileThumbSmall = WebConstants.UserProfileThumbSmall;
				int UserProfileThumbMedium = WebConstants.UserProfileThumbMedium;
				int UserProfileThumbLarge = WebConstants.UserProfileThumbLarge;

				FileSystemHelper FileUpl = new FileSystemHelper();
				var SmallThmb = FileUpl.GenThumb(FilLocation, UserProfileThumbSmall);
				var InsertSmallThumb = AddThumbRec(SmallThmb, "Thumbnail-Small");

				var MediumThmb = FileUpl.GenThumb(FilLocation, UserProfileThumbMedium);
				var InsertMediumThumb = AddThumbRec(MediumThmb, "Thumbnail-Medium");

				var LargeThmb = FileUpl.GenThumb(FilLocation, UserProfileThumbLarge);
				var InsertLargeThumb = AddThumbRec(LargeThmb, "Thumbnail-Large");

				return true;
			} catch {
				return false;
			}

		}
		public bool AddThumbRec(string FileName, string FileKey = "Thumbnail") {
			string error = "";
			AppUserMeta uThumb = new AppUserMeta();
			uThumb.MetaKey = FileKey;
			uThumb.MetaValue = FileName;
			uThumb.isInternal = true;
			uThumb.UserId = Auth.UserID;

			//Add Meta Item
			return _rep.AddMetaItem(uThumb, out error);
		}



		public List<AppEmployee> GetUserEmails(List<int> UserIds)
		{
			return _rep.GetUserEmails(UserIds);
		}

		#endregion


		public string GetUserEmails()
		{
			List<string> _data = _rep.GetUserEmails();
			string AdminEmails = string.Empty;
			string Separator = string.Empty;
			foreach (var item in _data)
			{
				Separator = !string.IsNullOrEmpty(AdminEmails) ? ", " : "";
				AdminEmails += Separator + item;
			}

			return AdminEmails;
		}
	}

}