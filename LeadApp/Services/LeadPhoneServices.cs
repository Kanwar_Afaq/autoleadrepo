﻿using LeadApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Services
{
    public class LeadPhoneServices
    {
         private LeadPhoneRepository _leadrepository = new LeadPhoneRepository();

         public IEnumerable<LeadPhone> Get()
         {
            return _leadrepository.Get();
         }
        public List<ProfitLossVM> getProfitLossStats()
        {
            return _leadrepository.getProfitLossStats();
        }
        public int? getPendingLeads()
        {
            return _leadrepository.getPendingLeads();
        }
        public bool isVisiblePriceButton(int leadPhoneId)
        {
            return _leadrepository.isVisiblePriceButton(leadPhoneId);
        }
        public IEnumerable<VarifyVM> getVarifyLeads(int? statusId)
        {
            return _leadrepository.getVarifyLeads(statusId);
        }
         public IEnumerable<LeadPhoneVM> GetByUserId(int? StatusId)
         {
             return _leadrepository.GetByUserId(StatusId);
         }
        public bool VarifiedLead(int LeadPhoneId, string PlaceofPickup, string City, string Phone, string Email, int VehicleYear, string VinNo, string ZipCode)
        {
            return _leadrepository.VarifiedLead(LeadPhoneId, PlaceofPickup, City, Phone, Email, VehicleYear, VinNo, ZipCode);
        }
        public IEnumerable<LeadPhoneVM>GetByStatusId(int? StatusId)
        {
            return _leadrepository.getByStatusId(StatusId);
        }

         public IEnumerable<LeadPhone> Get(int Id)
         {
             return _leadrepository.Get();
         }

         public bool AddNote(int LeadPhoneId, string Note,string Mode,decimal? Amount)
         {
             return _leadrepository.AddNote(LeadPhoneId,Note,Mode,Amount);
         }

         public bool AddNoteOnly(int LeadPhoneId, string Note)
         {
             return _leadrepository.AddNoteOnly(LeadPhoneId, Note);
         }        

         public bool ApproveLead(int LeadPhoneId,string Note, decimal AmountAccepted,string PlaceofPickup)
         {
             return _leadrepository.ApproveLead(LeadPhoneId, Note, AmountAccepted, PlaceofPickup);
         }       

         public bool RejectLead(int LeadPhoneId,string Note)
         {
             return _leadrepository.RejectLead(LeadPhoneId,Note);
         }

         public bool DisqualifyLead(int LeadPhoneId, string Note)
         {
             return _leadrepository.DisqualifyLead(LeadPhoneId, Note);
         }
        public bool EditSalePrice(int LeadPhoneId, double SalePrice)
        {
            return _leadrepository.EditSalePrice(LeadPhoneId,SalePrice);
        }
        public VarifyVM getVarified(int LeadPhoneId)
        {
            return _leadrepository.getVarified(LeadPhoneId);
        }



         public bool Add(LeadPhone _data, out string error, out int LeadPhoneId)
         {
             return _leadrepository.Add(_data, out error, out LeadPhoneId);
         }

         public bool Edit(int? id,LeadPhone _data, out string error)
         {
            return _leadrepository.Edit(id,_data, out error);
         }

         public bool Delete(int id, out string error)
         {
            return _leadrepository.Delete(id, out error);
         }

     }
}