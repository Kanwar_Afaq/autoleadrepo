﻿using LeadApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LeadApp.Helpers;


namespace LeadApp.Services {
	public class TaxonomyService
	{
		TaxonomyRepositry _rep = new TaxonomyRepositry();
		UserService _ServUser = new UserService();
		string error = "";

		public IEnumerable<Taxonomy> Get()
		{
			return _rep.Get();
		}
		public IEnumerable<Taxonomy> GetVehicleConditions()
		{
			return _rep.Get().Where(e=>e.TaxonomyCatId == 5);
		}

		public IEnumerable<Taxonomy> GetVehicleTitle()
		{
			return _rep.Get().Where(e => e.TaxonomyCatId == 6);
		}


		public IEnumerable<TaxonomyCategory> GetCategory()
		{
			return _rep.GetCategory();
		}


		
		public bool Add(Taxonomy _data, out string error)
		{

			bool isSuccess = _rep.Add(_data, out error);
			return isSuccess;
		}

		public bool Edit(Taxonomy _data, out string error)
		{

			bool isSuccess = _rep.Edit(_data, out error);
			return isSuccess;
		}



		public bool Delete(int id, out string error) {
			return _rep.Delete(id, out error);
		}


		public bool isExist(string name, int CatId, int? id)
		{
			return _rep.isExist(name, CatId, id);
		}
	}
}