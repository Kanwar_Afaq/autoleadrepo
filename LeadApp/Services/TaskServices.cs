﻿using LeadApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LeadApp.Helpers;


namespace LeadApp.Services {
	public class TaskServices {
		TaskRepositry _rep = new TaskRepositry();
		UserService _ServUser = new UserService();
		string error = "";

		public Task GetTask(Guid id)
		{
			return _rep.GetTasks().Where(e => e.TaskID == id).FirstOrDefault();
		}

		//public TaskViewModel GetTaskDetail(Guid id) {
		//	return _Rep.GetAllTasks().Where(e=>e.TaskID == id).FirstOrDefault();
		//}

		public IEnumerable<Task> GetAllTasks() {
			var _data = _rep.GetAllTasks();
			return _data;
			//List<Task> output = new List<Task>();

			//foreach (var item in _data)
			//{
			//	foreach (var u in item.TaskAssignees)					
			//		item.Assignees += !string.IsNullOrEmpty(item.Assignees) ? ", " : "" + u.AppUser.AppEmployees.FirstOrDefault().ContactName;
				
			//	output.Add(item);
			//}

			//return output;
		}

		public IEnumerable<TaskRespons> GetAllTaskReply(Guid id) {
			var data = _rep.GetAllTaskReply(id);
			return data;
		}



		public IEnumerable<ActivityStatus> GetStatus() {
			return _rep.GetStatus();
		}





		public bool AddTask(Task _data, List<int> AssigneeUserID, out string error) {

			bool isSuccess = _rep.AddTask(_data, AssigneeUserID, out error);
			if (isSuccess)
				sendEmailAlert(_data, AssigneeUserID, "Add");

			return isSuccess;

		}

		public bool Edit(Task _data, List<int> AssigneeUserID, out string error)
		{

			bool isSuccess = _rep.EditTask(_data, AssigneeUserID, out error);
			if (isSuccess)
				sendEmailAlert(_data, AssigneeUserID, "Update");

			return isSuccess;
		}

		private bool sendEmailAlert(Task _data, List<int> AssigneeUserID, string Template)
		{
			var UserEmails = _ServUser.GetUserEmails(AssigneeUserID).Select(e => e.Email).ToList();
			string EmailList = string.Empty;
			foreach (var e in UserEmails)
			{
				EmailList += !string.IsNullOrEmpty(EmailList) ? ", " : "";
				EmailList += e;
			}
			bool isEmailSent = SystemEmailService.TaskAlert(Template, _data.Title, Auth.Name, Auth.Email, _data.Description, EmailList, _data.DateDue.FormatDateTime());

			return isEmailSent;
		}


		public bool AddReply(TaskRespons _data, out string error)
		{
			_data.SubmitUserID = Auth.UserID;

			bool isSuccess = _rep.AddReply(_data, out error);
			if (isSuccess)
			{
				List<int> TaskAsignee = _rep.TaskStackHolders(_data.TaskID);
				//TaskAsignee.Add(_data.CreatedBy);

				var UserEmails = _ServUser.GetUserEmails(TaskAsignee).Select(e => e.Email).ToList();
				string EmailList = string.Empty;
				foreach (var e in UserEmails)
				{
					EmailList += !string.IsNullOrEmpty(EmailList) ? ", " : "";
					EmailList += e;
				}
				string StatusName = GetStatus().Where(e => e.StatusID == _data.StatusID).FirstOrDefault().StatusName;
				var TaskDetail = GetAllTasks().Where(e => e.TaskID == _data.TaskID).FirstOrDefault();
				string TaskTitle = TaskDetail != null ? TaskDetail.Title : "";
				string TaskDueDate = TaskDetail != null ? TaskDetail.DateDue.FormatDateTime() : "";
				bool isEmailSent = SystemEmailService.TaskReplyAlert(TaskTitle, Auth.Name, Auth.Email, _data.Response, EmailList, TaskDueDate, StatusName);
			}

			return isSuccess;
		}



		public bool DeleteTask(Guid id, out string error) {
			return _rep.DeleteTask(id, out error);

		}

		public bool CloseTask(Guid id, out string error) {
			return _rep.CloseTask(id, out error);
		}

		public bool ChangeStatus(Guid id, string StatuName, out string error) {
			return _rep.ChangeStatus(id, StatuName, out error);
		}





	}
}