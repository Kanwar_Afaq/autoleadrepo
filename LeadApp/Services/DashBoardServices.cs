﻿using LeadApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeadApp.Services
{
    public class DashBoardServices
    {
        public DashBoardRepository _dashboardrepository = new DashBoardRepository();
        public DashBoardVM DashBoardReport(int GroupID)
        {
            return _dashboardrepository.DashBoardReport(GroupID);
        }
    

        public ReportVM ReportVM(DateTime? starttime,DateTime? endtime)
        {
            return _dashboardrepository.ReportVM(starttime,endtime);
        }
    }
}