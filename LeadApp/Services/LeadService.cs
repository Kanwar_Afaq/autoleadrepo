﻿using LeadApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LeadApp.Helpers;


namespace LeadApp.Services {
	public class LeadService
	{
		LeadRepositry _rep = new LeadRepositry();
		ManufacturerService _servManf = new ManufacturerService();
		UserService _ServUser = new UserService();
		string error = "";

		public IEnumerable<Lead> Get()
		{
			return _rep.Get();
		}

		public Lead GetSingle(int Id)
		{
			return _rep.GetSingle(Id);
		}


		public IEnumerable<LeadViewModel> GetList()
		{
			return _rep.GetList();
		}

		
		public bool Add(Lead _data, out string error, out int Id)
		{
			bool isSuccess = false;
			error = string.Empty;
			Id = 0;
			string Body = string.Empty;

			if (_rep.Get().Where(e => e.WebId == _data.WebId && e.VehicleYear == _data.VehicleYear && e.ManufactureId == _data.ManufactureId && e.Phone == e.Phone && e.EntryDate >= DateTime.Now.AddMinutes(-1)).Count() > 0)
			{
				isSuccess = false;
				error = "Your request is already been Submitted!";
			}
			if (!_data.ManufactureId.IsNumeric() && !_data.VehicleYear.IsNumeric())
			{
				isSuccess = false;
				error = "Please specify Vehicle detail and submit it again!";
			}
			else
			{
				isSuccess = _rep.Add(_data, out Id, out error);
				var getLeadData = GetSingle(Id);
				//string Manf = (getLeadData.Manufacturer != null) ? getLeadData.Manufacturer.Name : "";
				var getManufacturer = _servManf.Get().Where(e => e.ManufactureId == getLeadData.ManufactureId).FirstOrDefault();
				string Manf = getManufacturer != null ? getManufacturer.Name : "-";

				Body += "<br><b>Lead ID:</b> " + getLeadData.LeadId;
				Body += "<br><b>Time:</b> " + getLeadData.EntryDate;
				Body += "<br><b>Name:</b> " + getLeadData.Name;
				Body += "<br><b>Tel:</b> " + getLeadData.Phone;
				Body += "<br><b>Year:</b> " + getLeadData.VehicleYear;
				Body += "<br><b>Make:</b> " + Manf;
				Body += "<br><b>Model:</b> " + getLeadData.VehicleModel;
				Body += "<br><b>Mileage:</b> " + getLeadData.Mileage;
				Body += "<br><b>Location:</b> " + getLeadData.ZipCode;
				if (!string.IsNullOrEmpty(getLeadData.Email))
					Body += "<br><b>Email:</b> " + getLeadData.Email;

				string Vehicle = Manf + " " + getLeadData.VehicleModel;
				SystemEmailService.LeadAlert(getLeadData.LeadId, Vehicle, Body, _ServUser.GetUserEmails(), getLeadData);
			}

			
			return isSuccess;
		}


		public bool Ack(int LeadId, string PeddleError, out string error)
		{
			bool isSuccess = false;
			error = string.Empty;

			isSuccess = _rep.Ack(LeadId, PeddleError, out error);

			return isSuccess;
		}

		public bool PeddlePosted(int LeadId, out string error)
		{
			bool isSuccess = _rep.PeddlePosted(LeadId, out error);
			return isSuccess;
		}

		public bool Edit(Lead _data, out string error)
		{

			bool isSuccess = _rep.Edit(_data, out error);
			return isSuccess;
		}



		public bool Delete(int id, out string error) {
			return _rep.Delete(id, out error);
		}


		public bool isExist(string name, int? id)
		{
			return _rep.isExist(name, id);
		}
	}
}