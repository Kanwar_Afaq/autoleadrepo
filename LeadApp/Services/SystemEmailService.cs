﻿using LeadApp.Helpers;
using LeadApp.Models;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LeadApp.Services
{
	public class SystemEmailService
	{
		#region User Module Emails
		public static bool NewAccountCreated(string Name, string Username, string Password, string EmailID, string Subject = "")
		{
			string EmailFilename = "Welcome.htm";

			ListDictionary ReplaceData = ReplaceKeywords(Name, Username, Password, EmailID, "");

			if (string.IsNullOrEmpty(Subject))
				Subject = string.Format("Welcome to " + WebConfig.Title);

			return EmailHelper.SystemEmail(EmailFilename, ReplaceData, EmailID, Subject);
		}

		public static bool AccountUpdated(string Name, string Username, string NewPassword, string EmailID, string Subject = "")
		{
			string EmailFilename = "Account-Update.htm";

			ListDictionary ReplaceData = ReplaceKeywords(Name, Username, NewPassword, EmailID, "");

			if (string.IsNullOrEmpty(Subject))
				Subject = string.Format("Account Updated - " + WebConfig.Title);

			return EmailHelper.SystemEmail(EmailFilename, ReplaceData, EmailID, Subject);
		}

		public static bool ResetPasswordRequest(string Username, string EmailID, string ActivateLink, string Subject = "")
		{
			string EmailFilename = "ResetPassword-Request.htm";

			ListDictionary ReplaceData = ReplaceKeywords(Username, "", "", EmailID, "", "", "", "", "", ActivateLink);

			if (string.IsNullOrEmpty(Subject))
				Subject = string.Format("Reset Password Request");

			return EmailHelper.SystemEmail(EmailFilename, ReplaceData, EmailID, Subject);
		}
		public static bool ResetPasswordChange(string Username, string NewPassword, string EmailID, string Subject = "")
		{
			string EmailFilename = "ResetPassword.htm";

			ListDictionary ReplaceData = ReplaceKeywords(Username, NewPassword, "", EmailID);

			if (string.IsNullOrEmpty(Subject))
				Subject = string.Format("Reset Password Successful");

			return EmailHelper.SystemEmail(EmailFilename, ReplaceData, EmailID, Subject);
		}

		//public static bool ProblemSignIn(string Username, string UserEmailID, string AdminEmails, string EmailContent)
		//{
		//	string EmailFilename = "Admin-ProblemSingin.htm";

		//	ListDictionary ReplaceData = ReplaceKeywords(Username, "", UserEmailID, "", EmailContent);

		//	string Subject = string.Format("User Signin Complain");

		//	return EmailHelper.SystemEmail(EmailFilename, ReplaceData, AdminEmails, Subject);

		//}
		#endregion



		#region Tasks Emails
		public static bool TaskAlert(string Template, string TaskTitle, string Author, string AuthorEmail, string Body, string TO, string DueDate)
		{
			string EmailFilename = (Template == "Add") ? "TaskAlert.htm" : "TaskAlert.htm";

			ListDictionary ReplaceData = ReplaceKeywords("", "", "", AuthorEmail, Body, TaskTitle, Author, DueDate);

			string Subject = string.Format("Task Alert - " + TaskTitle);

			return EmailHelper.SystemEmail(EmailFilename, ReplaceData, TO, Subject, "", AuthorEmail);
		}
		public static bool TaskReplyAlert(string TaskTitle, string Author, string AuthorEmail, string Body, string TO, string DueDate, string StatusName)
		{
			string EmailFilename = "TaskAlert-Reply.htm";

			ListDictionary ReplaceData = ReplaceKeywords("", "", "", AuthorEmail, Body, TaskTitle, Author, DueDate, StatusName);

			string Subject = string.Format("New Task Reply Alert - " + TaskTitle);

			return EmailHelper.SystemEmail(EmailFilename, ReplaceData, TO, Subject, "", AuthorEmail);
		}
		#endregion



		#region New Lead Alert
		public static bool LeadAlert(int LeadId, string Vehicle, string Body, string Recipients, Lead _data)
		{
			string EmailFilename = "LeadAlert.htm";

			ListDictionary ReplaceData = ReplaceKeywords("", "", "", "", Body, "", "", "");

			//string Subject = string.Format("Lead Alert - " + Vehicle);
			string Subject = string.Format(_data.VehicleYear + " " + Vehicle + "" + _data.VehicleModel + " " + _data.ZipCode + " - Lead Alert");

			return EmailHelper.SystemEmail(EmailFilename, ReplaceData, WebConfig.FromEmailID, Subject, "", "", "", Recipients);
		}

		#endregion












		private static ListDictionary ReplaceKeywords(string Name, string Username, string Password, string EmailID, string EmailMessage = "", string TaskTitle = "", string Author = "", string dDate = "", string StatusName = "", string ActivationLink = "")
		{
			ListDictionary ReplaceData = new ListDictionary();
			ReplaceData.Add("<%NAME%>", Name);
			ReplaceData.Add("<%USERNAME%>", Username);
			ReplaceData.Add("<%PASSWORD%>", Password);
			ReplaceData.Add("<%EMAILID%>", EmailID);
			ReplaceData.Add("<%EMAIL-MESSAGE%>", EmailMessage);

			ReplaceData.Add("<%TASK-TITLE%>", TaskTitle);
			ReplaceData.Add("<%AUTHOR%>", Author);
			ReplaceData.Add("<%STATUS%>", StatusName);
			ReplaceData.Add("<%DATE%>", dDate);

			ReplaceData.Add("<%ACTIVATELINK%>", ActivationLink);
			ReplaceData.Add("<%URL%>", WebConfig.AppURL);
			ReplaceData.Add("<%SIGNATURE%>", WebConfig.EmailSignature);

			return ReplaceData;
		}







	}
}