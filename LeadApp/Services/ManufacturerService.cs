﻿using LeadApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LeadApp.Helpers;


namespace LeadApp.Services {
	public class ManufacturerService
	{
		ManufacturerRepositry _rep = new ManufacturerRepositry();
		UserService _ServUser = new UserService();
		string error = "";

		public IEnumerable<Manufacturer> Get()
		{
			return _rep.Get().OrderBy(e=>e.Name);
		}

		
		public bool Add(Manufacturer _data, out string error)
		{

			bool isSuccess = _rep.Add(_data, out error);
			return isSuccess;
		}

		public bool Edit(Manufacturer _data, out string error)
		{

			bool isSuccess = _rep.Edit(_data, out error);
			return isSuccess;
		}



		public bool Delete(int id, out string error) {
			return _rep.Delete(id, out error);
		}


		public bool isExist(string name, int? id)
		{
			return _rep.isExist(name, id);
		}
	}
}