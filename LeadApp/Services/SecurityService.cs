﻿using LeadApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LeadApp.Services
{
    public class SecurityService
    {
        public SecurityRepository _rep = new SecurityRepository();



		public AppGroupModule VerifyUserAccess(string controllerName) {
			return _rep.VerifyUserAccess(controllerName);
		}

		public IEnumerable<AppModule> getUserModules()
		{
			return _rep.getUserModules();
		}



		#region Module Methods

		public List<AppModule> GetAllModules() {
			return _rep.GetAllModules();
		}
		public AppModule GetModule(int Id) {
			return _rep.GetModule(Id);
		}

		public List<AppModuleConfig> getAppModuleConfig()
		{
			return _rep.GetAppModuleConfig();
		}

		public AppModuleConfig getAppModuleConfig(int Id)
		{
			return _rep.GetAppModuleConfig(Id);
		}

		public bool ConfigEdit(int Id, AppModuleConfig getDATA, out string error)
		{
			return _rep.ConfigEdit(Id, getDATA, out error);
		}


	
		public bool isModuleExist(string ModuleName, int ModuleID) {
			return (_rep.isModuleExist(ModuleName, ModuleID)) ? false : true;
		}
		public bool CreateModule(AppModule getDATA, out string error)
        {

			//getDATA.Custom1Label = (getDATA.HasCustom1) ? getDATA.Custom1Label : "";
			//getDATA.Custom2Label = (getDATA.HasCustom2) ? getDATA.Custom2Label : "";
			//getDATA.Custom3Label = (getDATA.HasCustom3) ? getDATA.Custom3Label : "";
			//getDATA.Custom4Label = (getDATA.HasCustom4) ? getDATA.Custom4Label : "";
			//getDATA.Custom5Label = (getDATA.HasCustom5) ? getDATA.Custom5Label : "";
			//getDATA.Custom6Label = (getDATA.HasCustom6) ? getDATA.Custom6Label : "";

			return _rep.CreateModule(getDATA, out error);
        }
		public bool EditModule(int Id, AppModule getDATA, out string error)
        {
			//getDATA.Custom1Label = (getDATA.HasCustom1) ? getDATA.Custom1Label : "";
			//getDATA.Custom2Label = (getDATA.HasCustom2) ? getDATA.Custom2Label : "";
			//getDATA.Custom3Label = (getDATA.HasCustom3) ? getDATA.Custom3Label : "";
			//getDATA.Custom4Label = (getDATA.HasCustom4) ? getDATA.Custom4Label : "";
			//getDATA.Custom5Label = (getDATA.HasCustom5) ? getDATA.Custom5Label : "";
			//getDATA.Custom6Label = (getDATA.HasCustom6) ? getDATA.Custom6Label : "";


			return _rep.EditModule(Id, getDATA, out error);
        }
		public bool DeleteModule(int Id, out string error) {
			return _rep.DeleteModule(Id, out error);
		}


		#endregion


		#region Group Methods


		public IEnumerable<AppGroup> GetAllGroups() {
			return _rep.AppGroups();
		}
		public AppGroup getAppGroup(int id) {
			return _rep.getAppGroup(id);
		}




		public bool CreateGroup(AppGroup getDATA, out string error) {
			return _rep.CreateGroup(getDATA, out error);
		}			 
		public bool EditGroup(int Id, AppGroup getDATA, out string error) {
			return _rep.EditGroup(Id, getDATA, out error);
		}
		public bool DeleteGroup(int Id, out string error) {
			return _rep.DeleteGroup(Id, out error);
		}



		public AppGroup ProcessGroupData(AppGroup appgroup, FormCollection fm, out List<AppModule> outPutAllmodules) {
			List<AppModule> getAllmodules = GetAllModules();
			List<AppModule> setAllmodules = new List<AppModule>();

			appgroup.Priority = 20;

			//ICollection<AppGroupModule> selModules = appgroup.AppGroupModules;
			List<AppGroupModule> selModules = new List<AppGroupModule>();
			appgroup.AppGroupModules.Clear();

			int i = 0;
			foreach (var item in getAllmodules) {
				List<ModuleRights> AllRights = new List<ModuleRights>();

				string permission = string.Empty;
				int j = 0;
				foreach (var subitem in getAllmodules[i].Rights) {
					string separator = !string.IsNullOrEmpty(permission) ? ", " : "";
					string RightsValue = (fm["SelectedModules[" + i + "].Rights[" + j + "]." + subitem.Name] != null) ? "true" : "false";
					permission += separator + subitem.Name + ":" + RightsValue;



					ModuleRights Rights = subitem;
					Rights.isChecked = (RightsValue == "true") ? " checked=\"checked\"" : "";

					AllRights.Add(Rights);
					j++;
				}

				bool isAssignedtoGroup = (fm["SelectedModules[" + i + "].ModuleID"] != null) ? true : false;


				AppModule Module = new AppModule();
				Module = item;
				Module.isGroupModuleAsigned = isAssignedtoGroup;
				Module.RightsModified.AddRange(AllRights);
				setAllmodules.Add(Module);



				AppGroupModule groupModule = new AppGroupModule();
				groupModule.GroupId = appgroup.GroupId;
				groupModule.ModuleId = item.ModuleID;
				groupModule.IsAllowed = isAssignedtoGroup;
				groupModule.Permissions = permission;

				selModules.Add(groupModule);

				i++;
			}
			appgroup.AppGroupModules = selModules;


			outPutAllmodules = setAllmodules;
			return appgroup;
		}
		public List<AppModule> EditGroupData(AppGroup getData) {
			List<AppModule> getAllmodules = GetAllModules();
			List<AppModule> setAllmodules = new List<AppModule>();


			int i = 0;
			foreach (var item in getAllmodules) {
				List<ModuleRights> AllRights = new List<ModuleRights>();

				var getPermission = getData.AppGroupModules.FirstOrDefault(e => e.ModuleId == item.ModuleID);

				var AccessRights = VerifyPermission(getPermission);



				foreach (var subitem in getAllmodules[i].Rights) {
					ModuleRights Rights = subitem;
					Rights.isChecked = AccessRights.Where(e => e.Item1 == subitem.Name && e.Item2 == "true").Any() ? " checked=\"checked\"" : "";
					AllRights.Add(Rights);
				}

				var GrpAllowd = getData.AppGroupModules.FirstOrDefault(e => e.ModuleId == item.ModuleID);
				AppModule Module = new AppModule();
				Module = item;
				Module.isGroupModuleAsigned = (GrpAllowd != null) ? GrpAllowd.IsAllowed : false;
				Module.RightsModified.AddRange(AllRights);
				setAllmodules.Add(Module);
				

				i++;
			}

			return setAllmodules;
		}


		public List<Tuple<string, string>> VerifyPermission(AppGroupModule getPermission) {

			var AccessRights = new List<Tuple<string, string>>();

			if (getPermission != null) {
				string permissionData = getPermission.Permissions;
				string[] permissionlist = permissionData.Split(", ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

				foreach (var t in permissionlist) {
					var name = t.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
					if (name.Length > 1) {
						var tx = Tuple.Create(name[0], name[1]);
						AccessRights.Add(tx);
					}
				}
			}

			return AccessRights;
	
		}

		#endregion

	}
}